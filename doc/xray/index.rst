=============
X-ray spectra
=============

There are different tools for the prediction of x-ray spectra
as x-ray photoelectron spectra (XPS) or
x-ray photoabsorption spectra (XAS).

.. toctree::
    :maxdepth: 1

    dks
    xps_on_absolute_scale
    xas_on_absolute_scale
    fitting_xas_spectra
    fitting_xas_to_experiment
