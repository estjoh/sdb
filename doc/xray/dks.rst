===================
First peak energies
===================

The x-ray signal of XPS or the first peak in XAS
are calculated with a full core-hole.
The values are stored with the
database in the sub-directory ``1ch``
of each molecule.

These values can be calculated automatically
using the script::

  tools/calc_molecules.py

The script has several options that can be obtained
by calling::

  python3 calc_molecules.py -h

These energies are used to correct XPS and XAS energies.