=================================
Fitting XAS spectra to experiment
=================================

When comparing the theoretical spectra to experiments
we may want to optimalize the fit. In the following script
we have "experimental spectrum" and fit the theoretcal spectra
to it.

.. literalinclude:: compare_fit.py

The fitt reproduce the parameters we used to offset the
experiment and produse an good fit.

.. image:: compare_spectra.png

The ``compare_spectra`` function basic function fitt the
following parameters:

* For the experiment:
    * ``scale``
    * ``shift``
* For the calculation:
    * ``width``

The limits of these parameters can be adjusted by giving
the function, the parameter with ``_min`` or ``_max`` at 
the end do define the boarders. 
The energy range of the experiment can also be defined by 
the ``e_min`` and ``e_max`` parameters.

There are diffrent metods to calculate the goodnes of fit.
(By defult the methode is R²) The difftent methods are 
choosen buy spesifying the ``methode`` parameter and can
be chosen as:

* ``'R2'``
* ``'Adj. R2'``
* ``'NRMSE'``

There are also two options on how to obtain the fitt:

#. minnimizing residual
#. maximizing R²

These two should give the same results, but the function finds 
a local minima and not the global minima so they my give ifrent
results. By defult we minimiz residual, if we want to maximize 
R² we nned to set: 

.. code-block:: python3

    compare_spectra(..., maximize_r_squerd = True, ...)

The ``compare_spectra`` can also take two folded spectra, e.g.
comparing two experiment to each other. 
This can be done by:

.. code-block:: python3

    compare_spectra(..., folded = True, ...)

Varing fold
###########

It can also consider varing fold as i some cases with XAS
this is benifical. However as this is not yet
in the main GPAW we have included a function for it in sdb
you can call it as:

.. code-block:: python3

    from sdb.xray.folder import varing_fold

    X, Y = varing_fold(
        width1, folding, x_s, y_s, linbroad)

Here the ``linbroad`` parameter is a list containing:

.. code-block:: python3

    width2 = linbroad[0]
    x1 = linbroad[1]
    x2 = linbroad[2]

``x1`` and ``x2`` are the bordes of the varing width. 
if ``x`` < ``x1`` the folding uses ``widht1`` if ``x`` > ``x2`` 
the folding usses ``width2`` if ``x1`` < ``x`` < ``x2`` 
an gradient between ``width1`` and ``width2`` is used.

To get the best fit with varing fold you give the function the 
``linbroad`` containg the guessed ``width2``, ``x1``and ``x2``
in the ``compare_spectra`` function:

.. code-block:: python3

    width2 = 3.
    line1 = 50
    line2 = 60
    linbroad = [width2, line1, line2]
    compare_spectra(..., linbroad=linbroad, ...)

Here the values arew just examples. The boudries can be adjusted by
defining ``width2_min`` and ``width2_max``
and the same with ``line1`` and ``line2``.