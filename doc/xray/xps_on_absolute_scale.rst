========================================================
Prediction of XPS spectra on the absolute scale
========================================================

We use GPAW_ for the prediction of x-ray
photoelectron (XPS) spectra.
Core excited states can be calculated, but their energies
need to be corrected.
We use semi-empirical shifts [#WMP16]_ to achieve this.

We first calculate the raw core-hole energies

.. literalinclude:: MeHS/xps.py

The result of the calculation is stored now in the file
:file:`1ch_by_atom_PBE_h0.2_box4.dat`

.. literalinclude:: MeHS/1ch_by_atom_PBE_h0.2_box4.dat

We can use the raw energies given there to predict the S(2p)
XPS-spectrum on the absolute energy scale

.. literalinclude:: MeHS/predict.py

producing the figure

.. image:: MeHS/Gas-phase_MeSH.png

which shows the two peaks of S(2p3/2) at lower and S(2p1/2)
at higher energy. The corrected energy of 169.9 eV for the
S(2p3/2) peak is in
fair agreement to the experimental value of 169.4 eV [#WMP16]_.

.. _GPAW: https://wiki.fysik.dtu.dk/gpaw

.. [#WMP16] M. Walter, M. Moseler, and L. Pastewka,
           :doi:`Offset-corrected Δ-Kohn-Sham scheme for semiempirical prediction of absolute x-ray photoelectron energies in molecules and solids <10.1103/PhysRevB.94.041112>`,
           *Phys. Rev. B* **94**, 041112(R) (2016).
