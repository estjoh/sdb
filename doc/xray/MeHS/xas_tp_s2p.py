from gpaw import GPAW, FermiDirac
from sdb.molecules import find
from gpaw.xas import XAS


atoms = find(name='CH4S', verbose=0).atoms
atoms.calc = GPAW(mode='fd', xc='PBE',
                  setups={'S': '2p05ch'},
                  occupations=FermiDirac(width=0.1),
                  nbands=30  # add some more bands
                  )
atoms.get_potential_energy()

xas = XAS(atoms.calc, mode='xas')
# write out the matrix elements
xas.write('me_2p05ch.npz')
