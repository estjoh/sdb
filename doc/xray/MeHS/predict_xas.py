# creates: Gas-phase_MeSH.png
import matplotlib.pyplot as plt

from gpaw.xas import XAS
from gpaw.utilities.folder import Folder
from sdb.xray.calculated import read_data
from sdb.xray.predict import intensities

# raw DKS energy from calculation
vstate = 'S(2p)'
alldata = read_data('1ch_q-1_by_atom_PBE_h0.2_box4.dat')
_, dks, _ = alldata[vstate][0]

# raw ionization limit
alldata = read_data('1ch_by_atom_PBE_h0.2_box4.dat')
_, xps, _ = alldata[vstate][0]

# correct energies
xps_energies, _ = intensities(vstate, [xps])

dks_energies, w_xas = intensities(vstate, [dks])

xas = XAS.restart('me_2p05ch.npz')
energy_s, y_cs = xas.get_oscillator_strength(
    dks=dks_energies, w=w_xas)
intensity_s = y_cs.sum(0) / 3

width = 0.2  # eV
x, y = Folder(width).fold(energy_s, intensity_s, xmax=172)
plt.plot(x, y)

# mark ionization potentials
i = 1
for energy in xps_energies:
    plt.axvline(x=energy, ls=':', color='k')
    plt.text(x=energy+0.2, y=0.018, s=f'E$_{{{i}/2}}$')
    i += 2

plt.xlabel('corrected x-ray energy [eV]')
plt.ylabel(r'intensity [eV$^{-1}$]')

plt.tight_layout()
plt.savefig('Gas-phase_MeSH_xas.png')
