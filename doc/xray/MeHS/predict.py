# creates: Gas-phase_MeSH.png
import matplotlib.pyplot as plt

from gpaw.utilities.folder import Folder
from sdb.xray.calculated import read_data
from sdb.xray.predict import intensities

# raw energy from calculation
vstate = 'S(2p)'
alldata = read_data('1ch_by_atom_PBE_h0.2_box4.dat')
energies = [energy for index, energy, _ in alldata[vstate]]

width = 0.2  # eV
x, y = Folder(width).fold(*intensities(vstate, energies),
                          xmin=168, xmax=172)
plt.plot(x, y)
plt.xlabel('corrected x-ray energy [eV]')
plt.ylabel('intensity')
plt.savefig('Gas-phase_MeSH.png')
