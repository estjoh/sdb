from gpaw import GPAW, FermiDirac
from sdb.molecules import find
from sdb.xray.gpaw import calculate_and_write


atoms = find(name='CH4S', verbose=0).atoms
atoms.calc = GPAW(mode='fd', xc='PBE',
                  occupations=FermiDirac(width=0.1))

# calculate automatically all dks energies within atoms
dataf = '1ch_q-1_by_atom_PBE_h0.2_box4.dat'
calculate_and_write(atoms, dataf, charge=-1)
