========================================================
Prediction of XAS spectra on the absolute scale
========================================================

We use GPAW_ for the prediction of x-ray
absorption spectra (XAS).
Core excited states can be calculated, but their energies
need to be correct, where we use semi-empirical shifts [#WMP16]_.

We first calculate the raw core-hole energies for
the first transition that is usually called the
Δ-Kohn-Sham (DKS) energy. The claculation must be performed with
a formal negative charge (``charge=-1``) to compensate the core hole.

.. literalinclude:: MeHS/xas_dks.py

The result of the calculation is stored now in the file
:file:`1ch_q-1_by_atom_PBE_h0.2_box4.dat`

.. literalinclude:: MeHS/1ch_q-1_by_atom_PBE_h0.2_box4.dat

Traditional approach
--------------------

In the next step, a transition potential (TP, half core hole)
calculation is performed

.. literalinclude:: MeHS/xas_tp_s2p_traditional.py

which writes the file :file:`S2p05ch.gpw`.

We can use the raw energies given there to predict the S(2p)
XAS-spectrum on the absolute energy scale

.. literalinclude:: MeHS/predict_xas_traditional.py

Memory save approach
--------------------

One may also evaluate the matrix elements and write them out
immediately

.. literalinclude:: MeHS/xas_tp_s2p.py

We can use the matrix elements to predict the S(2p)
XAS-spectrum on the absolute energy scale

.. literalinclude:: MeHS/predict_xas.py

Both approaches produce the figure

.. image:: MeHS/Gas-phase_MeSH_xas.png


.. _GPAW: https://wiki.fysik.dtu.dk/gpaw

.. [#WMP16] M. Walter, M. Moseler, and L. Pastewka,
           :doi:`Offset-corrected Δ-Kohn-Sham scheme for semiempirical prediction of absolute x-ray photoelectron energies in molecules and solids <10.1103/PhysRevB.94.041112>`,
           *Phys. Rev. B* **94**, 041112(R) (2016).
