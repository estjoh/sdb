====================================
Fitting contributions to XAS spectra
====================================

We may want to get information about the contributions
of different structures to an experimental spectrum.
In the following script an "experimental spectrum" is created
and then fitted to two contributions that are specified by
lists of energies and oscillator strengths.

.. literalinclude:: fitting.py

The fit leads to very good agreement (by construction).
Note, that the folding width is fitted also.

.. image:: fitting.png
