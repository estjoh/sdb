# creates: fitting.png
import numpy as np
import matplotlib.pyplot as plt
from gpaw.utilities.folder import Folder

from sdb.xray.contributions import contributions, values

width = 0.4

# the first contribution
es0 = [286, 287]
fs0 = np.array([1, 0.5])
weight0 = 1

# the second contribution
es1 = [290]
fs1 = np.array([0.3])
weight1 = 6

# create all energies and oscillator strengths
es = np.concatenate((es0, es1)).flat
fs = np.concatenate((weight0 * fs0, weight1 * fs1)).flat
# create the "experimental spectrum"
folding = 'Lorentz'
folding = 'Gauss'
e_exp, i_exp = Folder(width, folding).fold(
    es, fs, dx=0.2, xmin=280, xmax=293)
# add some noise
i_exp += 0.2 * (np.random.rand(i_exp.shape[0]) - 0.5)

# the "theoretical" spectra
energies = (es0, es1)
oscillator_strengths = (fs0, fs1)

R2, params = contributions(
        e_exp, i_exp, energies, oscillator_strengths,
        # use "wrong" initial parameters
        initial_params={'width': 2, 'w0': 4, 'w1': 1},
        folding=folding)

w = np.empty(len(energies))
for i, _ in enumerate(energies):
    w[i] = params[f'w{i}'].value
w = 100 * w / w.sum()

e = np.linspace(e_exp.min(), e_exp.max(), 512)
sumid = e * 0
for i, es in enumerate(energies):
    calc = [es, oscillator_strengths[i]]
    sumid += params[f'w{i}'] * values(e, calc, params, folding)
    plt.plot(e, params[f'w{i}'] * values(
            e, calc, params, folding), label=f'contr. {i} {w[i]:.0f}%')
plt.plot(e, sumid, '--', label=f'sum R$^2$={R2 * 100:.0f}%', lw=5, alpha=0.3)

plt.plot(e_exp, i_exp, 'o', label='experiment')

plt.legend()
plt.xlabel('energy [eV]')
plt.ylabel('intensity')

plt.tight_layout()
plt.savefig('fitting.png')
