# creates compare_spectra.png
import numpy as np
import matplotlib.pyplot as plt
from gpaw.utilities.folder import Folder

from sdb.xray.compare import compare_spectra

# experimetal spectra parameters
width = 0.4
shift_exp = -0.3
scale_exp = 10

# Theoretical spectrum
energies = np.array([285, 288])
intensities = np.array([2, 1])

# experimental sepctrum
e_exp, i_exp = Folder(width, 'Gauss').fold(
    energies + shift_exp, intensities)


# add some noice
i_exp += 0.3 * (np.random.rand(i_exp.shape[0]) - 0.5)

i_exp /= i_exp.max() * scale_exp

# finding the optimal parameters
R2, params = compare_spectra(
    e_exp, i_exp, energies, intensities, folding='Gauss',
    init_params={'width': 0.3, 'scale': 1, 'shift': 0}
    )

e_calc, f_calc = Folder(params['width'], 'Gauss').fold(
    energies, intensities)

plt.plot(e_calc, f_calc, 'r', label=f'calc: R²= {R2*100:.0f}%', linewidth=2.)
plt.plot(e_exp - params['shift'],
         i_exp*params['scale']*f_calc.max(), 'ko', label='exp', linewidth=1)

plt.legend()
plt.xlabel('energy [eV]')
plt.ylabel('FOS [1/eV]')

plt.tight_layout()
plt.savefig('compare_spectra.png')
