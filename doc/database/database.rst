==================================
The file structure in the database
==================================

----------------------------
File structure for molecules
----------------------------

Molecules are expected to be located in the subdirectory ``molecules``
where they reside in a subdirectory according to Hill notation:

.. literalinclude:: hill.py

The sudirectories follow

* Next optional level: isomers (same number of atoms, different bonds)
* Next optional level: conformers (same chemical bonds, different conformations, or weak bonds)
* Next optional level: charge, example ``q-1`` for the anion
* Next level: Calculator, example ``GPAW_PBE``

The structure is exemplified for the TCAQ molecule (``C20H8N4``) having 3 conformers::

  molecules/C20H8N4/conformer_flat/GPAW_PBE
  molecules/C20H8N4/conformer_flat/q2/GPAW_PBE
  molecules/C20H8N4/conformer_flat/q1/GPAW_PBE
  molecules/C20H8N4/conformer_flat/q-1/GPAW_PBE
  molecules/C20H8N4/conformer_flat/q-2/GPAW_PBE
  molecules/C20H8N4/conformer_butterfly/GPAW_PBE
  ...
  molecules/C20H8N4/conformer_tilted/GPAW_PBE
  ...

where the dots denote directories for the charged species similar as in ``conformer_flat``.

-------------------------------
Information about the molecules
-------------------------------

Information about the molecule is contained in the file ``definition.yml``.
This may look for the example of TCNQ (C12H4N4) simply as::

  name: TCNQ

The information may be updated from the PubChem library:

.. literalinclude:: update_TCNQ_pubchem.py

changing the file ``definition.yml`` to::

  name:
  - TCNQ
  - 1518-16-7
  - 7,7,8,8-Tetracyanoquinodimethane
  - TCNQ
  - Tetracyanoquinodimethane
  - 2,2'-(Cyclohexa-2,5-diene-1,4-diylidene)dimalononitrile
  - Tetracyanoquinodimethan
  - Tetracyano-p-quinodimethane
  - Propanedinitrile, 2,2'-(2,5-cyclohexadiene-1,4-diylidene)bis-
  - Tetracyanoquinodimethan(e)
  pubchemcid: '73697'
  smiles: N#CC(C#N)=c1ccc(=C(C#N)C#N)cc1

Experimental data
=================

Experimental data should be added to file ``experimental.yml``.
The XPS section may look like (example of water):

.. literalinclude:: yml_examples/xps.yml

Here, two O(1s) values are listed.

In some cases, there are different atoms of the same
element that require a more detailed description
as in the example of F4S:

.. literalinclude:: yml_examples/F4S_xps.yml
