======================
Accessing the database
======================

Searching for data
==================

You may search a molecule by its name::

  from sdb.molecules import find

  find('TCNQ')

This will rerturn either a instance ``Molecule`` (if unique) or a list (if not unique)
or raise an error (if not found).

Interacting with pubchem
========================

We can update the information about the molecule using pubchem::

  from sdb.molecules.pubchem import update

  mol = find(name='acetonitrile')
  update(mol)