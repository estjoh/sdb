import sys
import sphinx_rtd_theme

sys.path.append('.')
assert sys.version_info >= (2, 7)

extensions = ['sphinx.ext.autodoc',
              'sphinx.ext.doctest',
              'sphinx.ext.extlinks',
              'sphinx.ext.mathjax',
              'sphinx.ext.viewcode',
              'sphinx.ext.intersphinx']
source_suffix = '.rst'
master_doc = 'index'
project = 'Structure database'
author = 'AG Walter' 
copyright = 'AG Walter 2024'
templates_path = ['templates']
exclude_patterns = ['build']
default_role = 'math'
pygments_style = 'sphinx'
autoclass_content = 'both'
modindex_common_prefix = ['sdb.']
extlinks = {
    'doi': ('https://doi.org/%s', 'doi: %s')}

html_theme = 'sphinx_rtd_theme'
html_style = 'sampling.css'
#html_favicon = 'static/cogef.ico'
html_static_path = ['static']
html_last_updated_fmt = '%a, %d %b %Y %H:%M:%S'
html_show_sourcelink = False

# Options for LaTeX output
latex_engine = 'pdflatex'
#latex_documents = ('index' , 'misstoolbox', 'This is my title', 'Alice \\and Bob', 'manual')
latex_show_urls = 'no'
latex_elements = {'papersize': 'a4paper'}
latex_show_urls = 'inline'
latex_show_pagerefs = True
