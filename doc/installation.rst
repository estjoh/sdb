============
Installation
============

Necessary libraries
-------------------

The necessary libraries can be installed via ``pip``::

  pip install ase pyyaml pandas lmfit gpaw
  pip install git+https://gitlab.com/ag_walter/sampling.git --upgrade

User installation
-----------------

The latest version can be installed using ``pip``::

  python3 -m pip install git+https://gitlab.com/ag_walter/sdb.git --upgrade

Developer installation
----------------------

It is recommended to follow the
`ASE developer instructions <https://wiki.fysik.dtu.dk/ase/development/contribute.html>`_.
The installation will then be::

  cd your-path
  git clone git@gitlab.com:your-user-name/sdb.git


Necessary libraries for doc
---------------------------

The install sphinx via ``pip``::

  pip install sphinx

