==================
Structure database
==================

The structure database access library (sdb) consist of
helping functions and methods to access and process data
in the structure database og AG Walter.
The sdb makes heavy use of ASE_.

.. _ASE: https://wiki.fysik.dtu.dk/ase

.. toctree::
    :maxdepth: 1

    installation
    data
    xray/index
