========
Database
========

The database containing the actual data itself is organized by directories and files.
Their location must be specified in
the environment variable ``STRUCTURE_DATABASE_DIR``.

.. toctree::
    :maxdepth: 1

    database/database
    database/access

Communicating to HPC systems
----------------------------

If you want to copy the full database to another computer,
e.g. a HPC system, it is recommended to use ``rsync``.
As example called from the directory ``AG_Walter``::

  # justus2
  rsync -vrzltE --update --progress structure_database justus2:/lustre/work/ws/ws1/fr_mw767-sdb/AG_Walter
  # nemo
  rsync -vrzltE --update --progress structure_database nemo:/work/ws/nemo/fr_mw767-sdb-0

You may then run calculations on the HPC system.
There is a tool ``tools/todo`` which you can use on the HPC system. It will check what is missing.
The results can be fetched in a similar way (called from the directory ``AG_Walter``)::

  # justus2
  rsync -vrzltE --update --progress justus2:/lustre/work/ws/ws1/fr_mw767-sdb/AG_Walter/structure_database .
  # nemo
  rsync -vrzltE --update --progress nemo:/work/ws/nemo/fr_mw767-sdb-0/structure_database .

You do not have to worry about extra files like ``run.justus2`` or similar as
there is the tool ``tools/clean`` which can be used to indentify and remove them.

It may be necessary to update the current state of the database after some
rearrangement of files and/or deletion of garbage.
Then use also ``--delete``::

  # justus2
  rsync -vrzltE --update --progress --delete structure_database justus2:/lustre/work/ws/ws1/fr_mw767-sdb/AG_Walter
  # nemo
  rsync -vrzltE --update --progress --delete structure_database nemo:/work/ws/nemo/fr_mw767-sdb-0