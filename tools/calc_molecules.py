from optparse import OptionParser

from ase.parallel import parprint, paropen, world

from gpaw import GPAW, FermiDirac
from gpaw.xc.hybrid import HybridXC
from gpaw.utilities.adjust_cell import adjust_cell

from sdb.molecules import walk, Molecule
from sdb.gpaw import setups
from sdb.xray.gpaw import calculate_and_write, filenames

parser = OptionParser()
parser.add_option("-H", "--h", dest="h",
                  help="Grid spacing", default=0.2)
parser.add_option("-b", "--box", dest="box",
                  help="Minimal box size", default=4)
parser.add_option("-f", "--xcfunctional", dest="XC", default='PBE',
                  help="Exchange-Correlation functional (default value PBE)")
parser.add_option("-s", "--spinpol", dest="spinpol", default=False,
                  action="store_true",
                  help="Spin polarized calculation (default False)")
parser.add_option("-n", "--nonscf",
                  action="store_true", dest="nsc", default=False,
                  help="Non-selfconsistent calculation (default False)")
parser.add_option("-q", "--charge", dest="q",
                  help="virtual charge in the core-excited state "
                  "(total charge -1, default 0)", default=None)
parser.add_option("-w", "--width", dest="width",
                  help="Fermi width (default 0.1)", default=0.1)
parser.add_option("-z", "--sz", dest="sz",
                  help="Sz in core-excited state (default None). "
                  "The ground state has SZ+q.", default=None)

(options, args) = parser.parse_args()

xc = options.XC
h = float(options.h)
box = float(options.box)
if box.is_integer():
    box = int(box)

names = walk('S')
if len(args):
    names = args

excludeAl = []
for name in names:
    if 'Al' in name:
        pass
    else:
        excludeAl.append(name)

if options.nsc:
    xcop = 'PBE'
    dxc = xc
    xc += '_nsc'
elif xc == 'PBE0' or xc == 'B3LYP':
    xcop = HybridXC(xc, finegrid=False)
else:
    xcop = xc
spinpol = options.spinpol
width = float(options.width)

occupations = FermiDirac(width=width)
if options.sz is not None:
    assert spinpol
    occupations = FermiDirac(width=width, fixmagmom=True)

charge = 0
if options.q is not None:
    charge = float(options.q)

for name in names:
    parprint(name)
    try:
        mol = Molecule(name)
        datf, outf = filenames(mol.directory, xc, h, box, spinpol,
                               charge=options.q, sz=options.sz)
        outf.parent.mkdir(exist_ok=True)
        txt = paropen(outf, 'a')
        world.barrier()

        parprint('  calculating', datf)
        parprint('  output in', outf)
        atoms = mol.atoms
        mm = [0] * len(atoms)
        if options.sz is not None:
            # magnetic moment for the ground state
            mm = [(float(options.sz) + charge)
                  / len(atoms)] * len(atoms)
        atoms.set_initial_magnetic_moments(mm)
        adjust_cell(atoms, box, h=h)
        atoms.calc = GPAW(
            mode='fd', xc=xcop, h=h, spinpol=spinpol,
            occupations=occupations,
            setups=setups(atoms),
            # eigensolver='rmm-diis',
            # this might be needed for Iron
            # mixer=Mixer(beta=0.05, nmaxold=5, weight=50.0),
            txt=txt)

        if not options.nsc:
            dxc = None
        calculate_and_write(atoms, datf, charge=charge,
                            nonselfconsistent_xc=dxc, sz=options.sz)

    except RuntimeError as e:
        parprint(' ', e)
