from __future__ import print_function
from glob import glob
import os
import sys
import yaml
import copy

from experiment import Experiment
from utilities import lc_dict
from xps import state_Jolly1984

nicename = {
    'coreelectronbindingenergy' : 'CoreElectronBindingEnergy',
    'reference' : 'Reference',
    'unit' : 'Unit',
    'index' : 'Atom',
    'atom' : 'Atom',
}

def collect(dirname, fname='experimental.yml'):
    fc = []
    for root, dirs, files in os.walk(dirname):
        for fn in files:
            if fn == fname:
                fc.append(os.path.join(root, fn))
        for direc in dirs:
            fc += collect(os.path.join(root, direc))
    return fc

if len(sys.argv) > 1:
    filelist = sys.argv[1:]
else:
    filelist = collect('molecules')
    filelist = collect('solids')


for fn in filelist:
    print('checking', fn)
    changed = False

    alldata = yaml.load(open(fn, 'r'))
    lc = lc_dict(alldata)
    try:
        xpsdata = lc['coreelectronbindingenergy']
        if type(xpsdata) != type([]):
            xpsdata = [xpsdata]

        for entry in xpsdata:
            # each entry may define several atom types
            for element in state_Jolly1984:
                vstate = '{0}({1})'.format(element, state_Jolly1984[element])
                if vstate in entry:
                    edata = entry[vstate]
                    # atom indicees might be specified
                    ## print(' ', element, edata)
                    if type(edata) == type([]):
                        # multiple entries
                        for eentry in edata:
                            if 'atoms' in eentry:
                                # lazy notation -> transform
                                changed = True
                                eentry['Atom'] = [
                                    int(i) for i in 
                                    eentry.pop('atoms').split(',')]
                            if 'index' in eentry:
                                changed = True
                                eentry['Atom'] = eentry.pop('index')
                    else:
                        ## print(' ', element, edata)
                        pass

                    ##print(' ', element, edata)
        if changed:
#            print(yaml.dump(alldata, default_flow_style=False))
            with open(fn, 'w') as yaml_file:
                yaml_file.write(yaml.dump(alldata, default_flow_style=False))
            print('  corrected')

    except ValueError:
        print('  no XPS data.')
