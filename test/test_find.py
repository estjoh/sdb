import numpy as np
import pytest
from ase.build import molecule
from sampling.translate import from_smiles, to_smiles

from sdb.molecules import Molecule, find, find_element


def test_from_name(capsys):
    find(name='water')

    try:
        find(name='wuter')
        assert 0
    except RuntimeError:
        assert 'water' in capsys.readouterr().out


def test_from_composition():
    mol = find(composition='OH2')
    assert isinstance(mol, Molecule)

    try:
        find(composition='O2H2')
    except RuntimeError:
        pass


def test_from_file(test_data):
    mol = find(file=test_data / 'molecules' / 'H2O' / 'GPAW_PBE' / 'ini.xyz')
    assert isinstance(mol, Molecule)


@pytest.mark.xfail  # XXX
def test_find_element():
    assert len(find_element('O')) == 1


def test_find_emt():
    molecule = find(composition='OH2', code='EMT')
    assert molecule.relaxed()


def test_find_isomers():
    mols = find('F2LiO2P')
    assert len(mols) == 2

    mol = find(name='F2LiO2P/symmetric')
    assert hasattr(mol, 'atoms')


def test_find_spin_projection():
    mol = find('CH2')
    assert mol.atoms.get_initial_magnetic_moments().sum() \
           == pytest.approx(0)

    def calc_Sz(calc):
        print('kpts', calc.kpts)
        if calc.get_number_of_spins() == 1:
            return 0
        else:
            return np.sum(calc.kpts[0].f_n - calc.kpts[1].f_n)

    for Sz in [0, 2]:
        mol = find('CH2', Sz=Sz)
        assert calc_Sz(mol.atoms.calc) == pytest.approx(Sz)


def test_find_atoms_H2O():
    atoms = molecule('H2O')
    assert isinstance(find(atoms=atoms), Molecule)


def test_find_atoms_C3H6():
    atoms = from_smiles('CC=C')
    mol = find(atoms=atoms)
    assert isinstance(mol, Molecule)
    assert to_smiles(mol.atoms) == to_smiles(atoms)


def test_find_Sz(test_data):
    E0 = find(name='H2O', charge=1).atoms.get_potential_energy()
    E1 = find(name='H2O', charge=1, Sz=1).atoms.get_potential_energy()
    assert E0 > E1  # Sz=1 lowers energy


def test_conformers(test_data):
    mols = find(name='TCAQ')
    assert len(mols) == 2


def test_charge(test_data):
    mol = find(name='C8H14O')
    assert hasattr(mol, 'atoms')

    molq1 = find(name='C8H14O', charge=1)
    assert hasattr(molq1, 'atoms')
    assert (mol.directory.parent / 'q1'
            == molq1.directory.parent)


def test_find_always_as_list(test_data):
    mols = find(name='F4S', aslist=True)
    assert isinstance(mols, list)
    assert len(mols) == 1

    mols = find(name='nonexisting', aslist=True)
    assert isinstance(mols, list)
    assert len(mols) == 0
