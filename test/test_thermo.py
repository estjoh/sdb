import pytest
from pathlib import Path

from ase.build import molecule
from ase.calculators.emt import EMT
from ase.optimize import FIRE
from sampling.thermochemistry import thermochemistry

from sdb.molecules import Molecule
from sdb.molecules import find
from sdb.thermochemistry import molecule_thermo


def test_H2O():
    molecule = find(composition='OH2', code='EMT')

    # previous calculation
    expected = {
        None: 1.8791735093002786,
        'zpe': 2.236755604630236,
        'enthalpy': 2.3533654566572837,
        'gibbs': 1.741931927811761,
    }

    for correction in expected.keys():
        assert expected[correction] == pytest.approx(
            molecule_thermo(molecule, correction))


def test_interface():
    molecule = find(composition='OH2', code='EMT')

    correction = 'gibbs'
    assert (molecule_thermo(molecule, correction)
            == thermochemistry(
                molecule.atoms, molecule.vibrations(), correction))


def test_zero_vibration():
    """Numerically we my have zero real vibrations as here for flat C2H4"""
    name = 'C2H4'
    code = 'EMT'
    atoms = molecule(name)
    atoms.calc = EMT()

    opt = FIRE(atoms)
    opt.run(fmax=0.05)

    direc = Path('molecules') / name / code
    direc.mkdir(parents=True)
    atoms.write(direc / 'relaxed.traj')

    def initialize(atoms):
        atoms.calc = EMT()
        return atoms

    mol = Molecule(name, 'molecules', code=code)
    mol.vibrations(initialize)

    # XXX problems with Gibbs values
    # assert molecule_thermo(mol, 'gibbs') == pytest.approx(0.7134395923086339)
