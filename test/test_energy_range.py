from sdb.data import EnergyRange


def test_basics():
    values = [1, 1, 4]
    energy_range = EnergyRange(values)
    assert energy_range.mmp() == (2, 1, 2)
    assert energy_range.mean == 2
