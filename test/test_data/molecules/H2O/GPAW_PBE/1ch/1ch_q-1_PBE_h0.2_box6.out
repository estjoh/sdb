
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.3.1b1
 |___|_|             

User:   mw@mmos1
Date:   Mon Apr 23 15:08:27 2018
Arch:   x86_64
Pid:    11010
Python: 3.5.2
gpaw:   /home/mw/source/gpaw/trunk/gpaw (171fe00f53)
_gpaw:  /home/mw/source/gpaw/trunk/build/bin.linux-x86_64-3.5/gpaw-python
ase:    /home/mw/source/ase/trunk/ase (version 3.15.1b1-0be2e38134)
numpy:  /usr/lib/python3/dist-packages/numpy (version 1.11.0)
scipy:  /usr/local/lib/python3.5/dist-packages/scipy (version 0.18.1)
units:  Angstrom and eV
cores:  2

Input parameters:
  eigensolver: rmm-diis
  h: 0.2
  occupations: {name: fermi-dirac,
                width: 0.1}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: c7d727ddbf81696289a2bba6bb064aec
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /home/mw/phys/dft/gridpaw/setups/gpaw-setups-0.6.6300/O.PBE.gz
  cutoffs: 0.74(comp), 1.30(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.741
    2p(4.00)    -9.029   0.741
    *s           3.251   0.741
    *p          18.182   0.741
    *d           0.000   0.741

  Using partial waves for O as LCAO basis

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /home/mw/phys/dft/gridpaw/setups/gpaw-setups-0.6.6300/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  Using partial waves for H as LCAO basis

Reference energy: -2065.832616

Spin-paired calculation

Occupation numbers:
  Fermi-Dirac: width=0.1000 eV

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 0.0001 electrons
  Maximum integral of absolute eigenstate change: 4e-08 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: Uniform real-space grid
  Kinetic energy operator: 6*3+1=19 point O(h^6) finite-difference Laplacian
  ScaLapack parameters: grid=1x1, blocksize=None 

Eigensolver
   RMM-DIIS eigensolver
       keep_htpsit: True
       Block size: 10
       DIIS iterations: 3
       Threshold for DIIS: 1.0e-16
       Limit lambda: False
       use_rayleigh: False
       trial_step: 0.1 

Densities:
  Coarse grid: 64*68*64 grid
  Fine grid: 128*136*128 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 128*136*128 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: Jacobi solver with 4 multi-grid levels
    Coarsest grid: 16 x 17 x 16 points
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    Max iterations: 1000
    Tolerance: 2.000000e-10 

Memory estimate:
  Process memory now: 70.21 MiB
  Calculator: 88.01 MiB
    Density: 33.45 MiB
      Arrays: 26.73 MiB
      Localized functions: 0.72 MiB
      Mixer: 6.00 MiB
    Hamiltonian: 37.51 MiB
      Arrays: 17.49 MiB
      XC: 0.00 MiB
      Poisson: 19.97 MiB
      vbar: 0.05 MiB
    Wavefunctions: 17.06 MiB
      Arrays psit_nG: 6.00 MiB
      Eigensolver: 10.99 MiB
      Projections: 0.00 MiB
      Projectors: 0.07 MiB

Total number of cores used: 2
Domain decomposition: 1 x 2 x 1

Number of atoms: 3
Number of atomic orbitals: 6
Number of bands in calculation: 6
Bands to converge: occupied states only
Number of valence electrons: 8

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
Creating initial wave functions:
  6 bands from LCAO basis set

         .------------------------------.  
        /|                              |  
       / |                              |  
      /  |                              |  
     /   |                              |  
    /    |                              |  
   /     |                              |  
  /      |                              |  
 *       |                              |  
 |       |                              |  
 |       |                              |  
 |       |                              |  
 |       |          OH                  |  
 |       |          H                   |  
 |       |                              |  
 |       |                              |  
 |       .------------------------------.  
 |      /                              /   
 |     /                              /    
 |    /                              /     
 |   /                              /      
 |  /                              /       
 | /                              /        
 |/                              /         
 *------------------------------*          

Positions:
   0 O      6.400000    6.800000    6.702322    ( 0.0000,  0.0000,  0.0000)
   1 H      6.400000    7.566928    6.097678    ( 0.0000,  0.0000,  0.0000)
   2 H      6.400000    6.033072    6.097678    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    12.800000    0.000000    0.000000    64     0.2000
  2. axis:    no     0.000000   13.600000    0.000000    68     0.2000
  3. axis:    no     0.000000    0.000000   12.800000    64     0.2000

  Lengths:  12.800000  13.600000  12.800000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.2000

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  15:08:34  +1.85   +inf   -12.329324    0      28     
iter:   2  15:08:34  +0.35   +inf   -16.119685    0             
iter:   3  15:08:34  -0.98   +inf   -16.192010    0             
iter:   4  15:08:39  +0.57  -0.74   -14.202200    0      17     
iter:   5  15:08:43  -0.11  -0.99   -14.292268    3      15     
iter:   6  15:08:46  -0.13  -1.19   -14.343104    0      14     
iter:   7  15:08:49  -1.07  -1.78   -14.338017    4      10     
iter:   8  15:08:52  -1.47  -2.28   -14.339597    2      6      
iter:   9  15:08:54  -2.24  -2.37   -14.343471    0      5      
iter:  10  15:08:56  -2.68  -2.54   -14.343396    0      5      
iter:  11  15:08:59  -2.42  -2.77   -14.343359    0      5      
iter:  12  15:09:01  -3.26  -2.76   -14.343534    0      4      
iter:  13  15:09:03  -3.59  -3.27   -14.343556    0      4      
iter:  14  15:09:05  -4.48  -3.49   -14.343476    0      3      
iter:  15  15:09:06  -4.88  -3.82   -14.343496    0      3      
iter:  16  15:09:08  -4.94  -3.99   -14.343575    0      3      
iter:  17  15:09:10  -6.27  -4.13   -14.343527    0      2      
iter:  18  15:09:12  -5.84  -4.27   -14.343455    0      2      
iter:  19  15:09:13  -7.05  -4.83   -14.343596    0      1      
iter:  20  15:09:15  -8.44  -5.13   -14.343542    0      1      

Converged after 20 iterations.

Dipole moment: (0.000000, -0.000000, -0.377974) |e|*Ang

Energy contributions relative to reference atoms: (reference = -2065.832616)

Kinetic:        +15.207591
Potential:      -17.331034
External:        +0.000000
XC:             -12.409088
Entropy (-ST):   -0.000000
Local:           +0.188989
--------------------------
Free energy:    -14.343542
Extrapolated:   -14.343542

Fermi level: -4.65965

 Band  Eigenvalues  Occupancy
    0    -25.21465    2.00000
    1    -12.98987    2.00000
    2     -9.36853    2.00000
    3     -7.23048    2.00000
    4     -0.98252    0.00000
    5      0.46908    0.00000

Input parameters:
  charge: -1
  setups: {0: 1s1ch}

Initialize ...

O-setup (1.0 core hole):
  name: Oxygen
  id: f6381a37458101a991b2db4d0ae8d064
  Z: 8
  valence: 6
  core: 1.0
  charge: 1.0
  file: /home/mw/phys/dft/gridpaw/setups/generate/O.1s1ch.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.77(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -43.769   0.688
    2p(4.00)   -28.908   0.598
    *s         -16.558   0.688
    *p          -1.697   0.598
    *d           0.000   0.619

  Using partial waves for O as LCAO basis

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /home/mw/phys/dft/gridpaw/setups/gpaw-setups-0.6.6300/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  Using partial waves for H as LCAO basis

Reference energy: -1511.033609

Spin-paired calculation

Occupation numbers:
  Fermi-Dirac: width=0.1000 eV

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 0.0001 electrons
  Maximum integral of absolute eigenstate change: 4e-08 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: Uniform real-space grid
  Kinetic energy operator: 6*3+1=19 point O(h^6) finite-difference Laplacian
  ScaLapack parameters: grid=1x1, blocksize=None 

Eigensolver
   RMM-DIIS eigensolver
       keep_htpsit: True
       Block size: 10
       DIIS iterations: 3
       Threshold for DIIS: 1.0e-16
       Limit lambda: False
       use_rayleigh: False
       trial_step: 0.1 

Densities:
  Coarse grid: 64*68*64 grid
  Fine grid: 128*136*128 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 128*136*128 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: Jacobi solver with 4 multi-grid levels
    Coarsest grid: 16 x 17 x 16 points
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    Max iterations: 1000
    Tolerance: 2.000000e-10 

Memory estimate:
  Process memory now: 268.54 MiB
  Calculator: 87.90 MiB
    Density: 33.35 MiB
      Arrays: 26.73 MiB
      Localized functions: 0.62 MiB
      Mixer: 6.00 MiB
    Hamiltonian: 37.50 MiB
      Arrays: 17.49 MiB
      XC: 0.00 MiB
      Poisson: 19.97 MiB
      vbar: 0.04 MiB
    Wavefunctions: 17.04 MiB
      Arrays psit_nG: 6.00 MiB
      Eigensolver: 10.99 MiB
      Projections: 0.00 MiB
      Projectors: 0.05 MiB

Total number of cores used: 2
Domain decomposition: 1 x 2 x 1

Number of atoms: 3
Number of atomic orbitals: 6
Number of bands in calculation: 6
Bands to converge: occupied states only
Number of valence electrons: 9

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
Creating initial wave functions:
  6 bands from LCAO basis set

         .------------------------------.  
        /|                              |  
       / |                              |  
      /  |                              |  
     /   |                              |  
    /    |                              |  
   /     |                              |  
  /      |                              |  
 *       |                              |  
 |       |                              |  
 |       |                              |  
 |       |                              |  
 |       |          OH                  |  
 |       |          H                   |  
 |       |                              |  
 |       |                              |  
 |       .------------------------------.  
 |      /                              /   
 |     /                              /    
 |    /                              /     
 |   /                              /      
 |  /                              /       
 | /                              /        
 |/                              /         
 *------------------------------*          

Positions:
   0 O      6.400000    6.800000    6.702322    ( 0.0000,  0.0000,  0.0000)
   1 H      6.400000    7.566928    6.097678    ( 0.0000,  0.0000,  0.0000)
   2 H      6.400000    6.033072    6.097678    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    12.800000    0.000000    0.000000    64     0.2000
  2. axis:    no     0.000000   13.600000    0.000000    68     0.2000
  3. axis:    no     0.000000    0.000000   12.800000    64     0.2000

  Lengths:  12.800000  13.600000  12.800000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.2000

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  15:09:22  +1.93   +inf   -20.799404    35     28     
iter:   2  15:09:22  +0.58   +inf   -27.174898    36            
iter:   3  15:09:22  -0.42   +inf   -27.812361    32            
iter:   4  15:09:26  -0.29  -0.85   -27.710190    33     17     
iter:   5  15:09:31  -0.19  -0.99   -27.724539    33     21     
iter:   6  15:09:36  -0.84  -1.55   -27.787987    4      15     
iter:   7  15:09:38  -1.79  -2.10   -27.804119    2      7      
iter:   8  15:09:40  -2.79  -2.08   -27.806023    2      5      
iter:   9  15:09:44  -2.25  -2.09   -27.803490    3      11     
iter:  10  15:09:46  -3.19  -2.66   -27.804602    2      5      
iter:  11  15:09:48  -3.31  -2.67   -27.804603    3      6      
iter:  12  15:09:50  -4.26  -2.97   -27.804588    2      3      
iter:  13  15:09:53  -4.68  -3.10   -27.804579    2      5      
iter:  14  15:09:55  -5.71  -3.60   -27.804607    2      2      
iter:  15  15:09:56  -5.16  -3.60   -27.804639    2      3      
iter:  16  15:09:58  -5.78  -3.74   -27.804595    2      3      
iter:  17  15:10:00  -6.32  -3.92   -27.804619    2      3      
iter:  18  15:10:02  -6.48  -4.40   -27.804583    2      2      
iter:  19  15:10:04  -7.43  -4.99   -27.804555    1      1      

Converged after 19 iterations.

Dipole moment: (-0.000000, 0.000000, 0.208593) |e|*Ang

Energy contributions relative to reference atoms: (reference = -1511.033609)

Kinetic:        +37.662892
Potential:      -45.299354
External:        +0.000000
XC:             -21.080856
Entropy (-ST):   -0.138630
Local:           +0.982078
--------------------------
Free energy:    -27.873870
Extrapolated:   -27.804555

Fermi level: -2.97482

 Band  Eigenvalues  Occupancy
    0    -31.92012    2.00000
    1    -18.90164    2.00000
    2    -16.04735    2.00000
    3    -14.23918    2.00000
    4     -2.97482    1.00000
    5     -1.35573    0.00000

Timing:                              incl.     excl.
-----------------------------------------------------------
Hamiltonian:                        11.857     0.000   0.0% |
 Atomic:                             0.092     0.008   0.0% |
  XC Correction:                     0.084     0.084   0.1% |
 Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
 Communicate:                        0.000     0.000   0.0% |
 Hartree integrate/restrict:         0.035     0.035   0.0% |
 Initialize Hamiltonian:             0.002     0.002   0.0% |
 Poisson:                            9.677     9.677  10.0% |---|
 XC 3D grid:                         2.038     2.038   2.1% ||
 vbar:                               0.012     0.012   0.0% |
LCAO initialization:                 0.092     0.071   0.1% |
 LCAO eigensolver:                   0.004     0.000   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.000     0.000   0.0% |
  Orbital Layouts:                   0.000     0.000   0.0% |
  Potential matrix:                  0.003     0.003   0.0% |
 LCAO to grid:                       0.006     0.006   0.0% |
 Set positions (LCAO WFS):           0.012     0.009   0.0% |
  Basic WFS set positions:           0.000     0.000   0.0% |
  Basis functions set positions:     0.000     0.000   0.0% |
  TCI: Calculate S, T, P:            0.002     0.002   0.0% |
SCF-cycle:                          83.632     0.066   0.1% |
 Density:                            0.724     0.001   0.0% |
  Atomic density matrices:           0.019     0.019   0.0% |
  Mix:                               0.630     0.630   0.7% |
  Multipole moments:                 0.004     0.004   0.0% |
  Pseudo density:                    0.070     0.069   0.1% |
   Symmetrize density:               0.001     0.001   0.0% |
 Hamiltonian:                       73.994     0.003   0.0% |
  Atomic:                            1.526     0.137   0.1% |
   XC Correction:                    1.389     1.389   1.4% ||
  Calculate atomic Hamiltonians:     0.023     0.023   0.0% |
  Communicate:                       0.001     0.001   0.0% |
  Hartree integrate/restrict:        0.568     0.568   0.6% |
  Poisson:                          36.677    36.677  37.9% |--------------|
  XC 3D grid:                       35.021    35.021  36.2% |-------------|
  vbar:                              0.175     0.175   0.2% |
 Orthonormalize:                     0.318     0.004   0.0% |
  calc_s_matrix:                     0.113     0.113   0.1% |
  inverse-cholesky:                  0.013     0.013   0.0% |
  projections:                       0.014     0.014   0.0% |
  rotate_psi_s:                      0.173     0.173   0.2% |
 RMM-DIIS:                           7.396     0.280   0.3% |
  Apply hamiltonian:                 0.731     0.731   0.8% |
  Calculate residuals:               0.183     0.183   0.2% |
  DIIS step:                         3.232     0.374   0.4% |
   Calculate residuals:              0.834     0.111   0.1% |
    Apply hamiltonian:               0.723     0.723   0.7% |
   Construct matrix:                 0.309     0.309   0.3% |
   Linear solve:                     0.013     0.013   0.0% |
   Update trial vectors:             0.438     0.438   0.5% |
   precondition:                     1.264     1.264   1.3% ||
  Find lambda:                       0.182     0.182   0.2% |
  Update psi:                        0.235     0.235   0.2% |
  precondition:                      2.537     2.537   2.6% ||
  projections:                       0.016     0.016   0.0% |
 Subspace diag:                      1.135     0.004   0.0% |
  calc_h_matrix:                     0.846     0.136   0.1% |
   Apply hamiltonian:                0.710     0.710   0.7% |
  diagonalize:                       0.011     0.011   0.0% |
  rotate_psi:                        0.274     0.274   0.3% |
Set symmetry:                        0.001     0.001   0.0% |
Other:                               1.082     1.082   1.1% |
-----------------------------------------------------------
Total:                                        96.664 100.0%

Memory usage: 269.36 MiB
Date: Mon Apr 23 15:10:04 2018
