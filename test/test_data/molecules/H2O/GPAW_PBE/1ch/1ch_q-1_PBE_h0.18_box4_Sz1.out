
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.3.1b1
 |___|_|             

User:   mw@mmos1
Date:   Sat Apr 21 00:18:26 2018
Arch:   x86_64
Pid:    20434
Python: 3.5.2
gpaw:   /home/mw/source/gpaw/trunk/gpaw (b17f77bb26)
_gpaw:  /home/mw/source/gpaw/trunk/build/bin.linux-x86_64-3.5/gpaw-python
ase:    /home/mw/source/ase/trunk/ase (version 3.15.1b1-0be2e38134)
numpy:  /usr/lib/python3/dist-packages/numpy (version 1.11.0)
scipy:  /usr/local/lib/python3.5/dist-packages/scipy (version 0.18.1)
units:  Angstrom and eV
cores:  4

Input parameters:
  eigensolver: rmm-diis
  h: 0.18
  occupations: {name: fermi-dirac,
                width: 0.1}
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: c7d727ddbf81696289a2bba6bb064aec
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /home/mw/phys/dft/gridpaw/setups/gpaw-setups-0.6.6300/O.PBE.gz
  cutoffs: 0.74(comp), 1.30(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.741
    2p(4.00)    -9.029   0.741
    *s           3.251   0.741
    *p          18.182   0.741
    *d           0.000   0.741

  Using partial waves for O as LCAO basis

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /home/mw/phys/dft/gridpaw/setups/gpaw-setups-0.6.6300/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  Using partial waves for H as LCAO basis

Reference energy: -2065.832616

Spin-paired calculation

Occupation numbers:
  Fermi-Dirac: width=0.1000 eV

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 0.0001 electrons
  Maximum integral of absolute eigenstate change: 4e-08 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: Uniform real-space grid
  Kinetic energy operator: 6*3+1=19 point O(h^6) finite-difference Laplacian
  ScaLapack parameters: grid=1x1, blocksize=None 

Eigensolver
   RMM-DIIS eigensolver
       keep_htpsit: True
       Block size: 10
       DIIS iterations: 3
       Threshold for DIIS: 1.0e-16
       Limit lambda: False
       use_rayleigh: False
       trial_step: 0.1 

Densities:
  Coarse grid: 48*56*48 grid
  Fine grid: 96*112*96 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 96*112*96 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: Jacobi solver with 5 multi-grid levels
    Coarsest grid: 6 x 7 x 6 points
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    Max iterations: 1000
    Tolerance: 2.000000e-10 

Memory estimate:
  Process memory now: 71.06 MiB
  Calculator: 20.28 MiB
    Density: 7.89 MiB
      Arrays: 6.07 MiB
      Localized functions: 0.49 MiB
      Mixer: 1.34 MiB
    Hamiltonian: 8.56 MiB
      Arrays: 3.97 MiB
      XC: 0.00 MiB
      Poisson: 4.55 MiB
      vbar: 0.04 MiB
    Wavefunctions: 3.83 MiB
      Arrays psit_nG: 1.34 MiB
      Eigensolver: 2.45 MiB
      Projections: 0.00 MiB
      Projectors: 0.05 MiB

Total number of cores used: 4
Domain decomposition: 2 x 2 x 1

Number of atoms: 3
Number of atomic orbitals: 6
Number of bands in calculation: 6
Bands to converge: occupied states only
Number of valence electrons: 8

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
Creating initial wave functions:
  6 bands from LCAO basis set

       .--------------------.  
      /|                    |  
     / |                    |  
    /  |                    |  
   /   |                    |  
  /    |                    |  
 *     |                    |  
 |     |                    |  
 |     |      OH            |  
 |     |      H             |  
 |     |                    |  
 |     .--------------------.  
 |    /                    /   
 |   /                    /    
 |  /                    /     
 | /                    /      
 |/                    /       
 *--------------------*        

Positions:
   0 O      4.320000    5.040000    4.622322    ( 0.0000,  0.0000,  0.0000)
   1 H      4.320000    5.806928    4.017678    ( 0.0000,  0.0000,  0.0000)
   2 H      4.320000    4.273072    4.017678    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no     8.640000    0.000000    0.000000    48     0.1800
  2. axis:    no     0.000000   10.080000    0.000000    56     0.1800
  3. axis:    no     0.000000    0.000000    8.640000    48     0.1800

  Lengths:   8.640000  10.080000   8.640000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1800

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  00:18:28  +1.86   +inf   -12.313003    0      15     
iter:   2  00:18:28  +0.26   +inf   -16.126200    0             
iter:   3  00:18:28  -1.04   +inf   -16.174447    0             
iter:   4  00:18:29  +0.55  -0.75   -14.217086    0      9      
iter:   5  00:18:30  -0.10  -0.98   -14.277079    3      8      
iter:   6  00:18:31  -0.10  -1.19   -14.343192    0      9      
iter:   7  00:18:32  -1.05  -1.77   -14.335615    4      7      
iter:   8  00:18:32  -1.46  -2.29   -14.337147    2      6      
iter:   9  00:18:33  -2.22  -2.36   -14.341550    0      5      
iter:  10  00:18:34  -3.70  -2.51   -14.341699    0      3      
iter:  11  00:18:34  -2.50  -2.51   -14.341363    0      5      
iter:  12  00:18:35  -3.15  -2.69   -14.341406    0      4      
iter:  13  00:18:36  -3.13  -2.80   -14.341467    0      5      
iter:  14  00:18:36  -3.19  -2.97   -14.341568    0      4      
iter:  15  00:18:37  -4.07  -3.17   -14.341425    0      3      
iter:  16  00:18:37  -3.93  -3.36   -14.341510    0      4      
iter:  17  00:18:38  -3.34  -3.50   -14.341604    0      4      
iter:  18  00:18:39  -3.78  -3.15   -14.341506    0      4      
iter:  19  00:18:39  -4.58  -3.74   -14.341526    0      3      
iter:  20  00:18:40  -4.50  -3.96   -14.341624    0      3      
iter:  21  00:18:40  -6.01  -3.98   -14.341551    0      2      
iter:  22  00:18:41  -5.34  -4.08   -14.341433    0      2      
iter:  23  00:18:42  -6.36  -4.37   -14.341785    0      1      
iter:  24  00:18:42  -7.38  -4.75   -14.341464    0      1      
iter:  25  00:18:43  -6.10  -4.59   -14.341609    0      2      
iter:  26  00:18:43  -7.30  -4.74   -14.341484    0      1      
iter:  27  00:18:44  -6.76  -4.90   -14.341365    0      1      
iter:  28  00:18:44  -7.77  -5.03   -14.341521    0      1      

Converged after 28 iterations.

Dipole moment: (0.000000, -0.000000, -0.381280) |e|*Ang

Energy contributions relative to reference atoms: (reference = -2065.832616)

Kinetic:        +15.056739
Potential:      -17.143196
External:        +0.000000
XC:             -12.401973
Entropy (-ST):   -0.000000
Local:           +0.146908
--------------------------
Free energy:    -14.341521
Extrapolated:   -14.341521

Fermi level: -4.65263

 Band  Eigenvalues  Occupancy
    0    -25.22529    2.00000
    1    -12.99690    2.00000
    2     -9.37951    2.00000
    3     -7.23795    2.00000
    4     -0.81904    0.00000
    5      0.88935    0.00000

Input parameters:
  charge: -1
  mixer: {backend: pulay,
          beta: 0.05,
          method: sum,
          nmaxold: 5,
          weight: 50.0}
  occupations: {fixmagmom: True,
                name: fermi-dirac,
                width: 0.1}
  spinpol: True

Input parameters:
  setups: {0: 1s1ch}

System changes: initial_magmoms 

Initialize ...

O-setup (1.0 core hole):
  name: Oxygen
  id: f6381a37458101a991b2db4d0ae8d064
  Z: 8
  valence: 6
  core: 1.0
  charge: 1.0
  file: /home/mw/phys/dft/gridpaw/setups/generate/O.1s1ch.PBE.gz
  cutoffs: 0.67(comp), 1.17(filt), 0.77(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -43.769   0.688
    2p(4.00)   -28.908   0.598
    *s         -16.558   0.688
    *p          -1.697   0.598
    *d           0.000   0.619

  Using partial waves for O as LCAO basis

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /home/mw/phys/dft/gridpaw/setups/gpaw-setups-0.6.6300/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  Using partial waves for H as LCAO basis

Reference energy: -1511.033609

Spin-polarized calculation.
Magnetic moment: 1.000000

Occupation numbers:
  Fixed magnetic moment
  Fermi-Dirac: width=0.1000 eV

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 0.0001 electrons
  Maximum integral of absolute eigenstate change: 4e-08 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: Uniform real-space grid
  Kinetic energy operator: 6*3+1=19 point O(h^6) finite-difference Laplacian
  ScaLapack parameters: grid=1x1, blocksize=None 

Eigensolver
   RMM-DIIS eigensolver
       keep_htpsit: True
       Block size: 10
       DIIS iterations: 3
       Threshold for DIIS: 1.0e-16
       Limit lambda: False
       use_rayleigh: False
       trial_step: 0.1 

Densities:
  Coarse grid: 48*56*48 grid
  Fine grid: 96*112*96 grid
  Total Charge: 0.000000 

Density mixing:
  Method: sum
  Backend: pulay
  Linear mixing parameter: 0.05
  Mixing with 5 old densities
  Damping of long wave oscillations: 50 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 96*112*96 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: Jacobi solver with 5 multi-grid levels
    Coarsest grid: 6 x 7 x 6 points
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    Max iterations: 1000
    Tolerance: 2.000000e-10 

Memory estimate:
  Process memory now: 111.83 MiB
  Calculator: 51.26 MiB
    Density: 21.92 MiB
      Arrays: 16.51 MiB
      Localized functions: 0.86 MiB
      Mixer: 4.55 MiB
    Hamiltonian: 21.53 MiB
      Arrays: 12.27 MiB
      XC: 0.00 MiB
      Poisson: 9.19 MiB
      vbar: 0.06 MiB
    Wavefunctions: 7.81 MiB
      Arrays psit_nG: 2.73 MiB
      Eigensolver: 5.01 MiB
      Projections: 0.00 MiB
      Projectors: 0.07 MiB

Total number of cores used: 4
Parallelization over spin
Domain decomposition: 1 x 2 x 1

Number of atoms: 3
Number of atomic orbitals: 6
Number of bands in calculation: 6
Bands to converge: occupied states only
Number of valence electrons: 9

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
Creating initial wave functions:
  6 bands from LCAO basis set

       .--------------------.  
      /|                    |  
     / |                    |  
    /  |                    |  
   /   |                    |  
  /    |                    |  
 *     |                    |  
 |     |                    |  
 |     |      OH            |  
 |     |      H             |  
 |     |                    |  
 |     .--------------------.  
 |    /                    /   
 |   /                    /    
 |  /                    /     
 | /                    /      
 |/                    /       
 *--------------------*        

Positions:
   0 O      4.320000    5.040000    4.622322    ( 0.0000,  0.0000,  0.3333)
   1 H      4.320000    5.806928    4.017678    ( 0.0000,  0.0000,  0.3333)
   2 H      4.320000    4.273072    4.017678    ( 0.0000,  0.0000,  0.3333)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no     8.640000    0.000000    0.000000    48     0.1800
  2. axis:    no     0.000000   10.080000    0.000000    56     0.1800
  3. axis:    no     0.000000    0.000000    8.640000    48     0.1800

  Lengths:   8.640000  10.080000   8.640000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1800

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson  magmom
iter:   1  00:18:50  +1.96   +inf   -29.364130    11     15       +1.0000
iter:   2  00:18:50  +0.54   +inf   -36.101562    2               +1.0000
iter:   3  00:18:51  -0.41   +inf   -36.661876    13              +1.0000
iter:   4  00:18:54  -0.47  -0.86   -36.160251    0      7        +1.0000
iter:   5  00:18:58  +0.36  -0.87   -36.154200    0      9        +1.0000
iter:   6  00:19:01  -0.89  -1.33   -36.531725    1      7        +1.0000
iter:   7  00:19:05  -0.87  -1.45   -36.561931    1      8        +1.0000
iter:   8  00:19:08  -0.83  -1.64   -36.766948    8      9        +1.0000
iter:   9  00:19:12  -0.88  -1.91   -36.679372    2      7        +1.0000
iter:  10  00:19:15  -0.78  -1.91   -36.819314    6      6        +1.0000
iter:  11  00:19:18  -1.43  -2.16   -36.805344    6      6        +1.0000
iter:  12  00:19:21  -1.67  -2.50   -36.782218    4      4        +1.0000
iter:  13  00:19:24  -2.15  -2.45   -36.765800    4      3        +1.0000
iter:  14  00:19:27  -2.44  -2.44   -36.753302    4      3        +1.0000
iter:  15  00:19:30  -3.23  -2.43   -36.752148    4      3        +1.0000
iter:  16  00:19:33  -3.08  -2.45   -36.757857    4      4        +1.0000
iter:  17  00:19:36  -1.07  -2.46   -36.835708    7      6        +1.0000
iter:  18  00:19:39  -1.32  -2.61   -36.857640    9      5        +1.0000
iter:  19  00:19:42  -1.70  -2.82   -36.855451    6      5        +1.0000
iter:  20  00:19:45  -2.59  -2.78   -36.860609    9      4        +1.0000
iter:  21  00:19:48  -2.20  -2.80   -36.863406    9      5        +1.0000
iter:  22  00:19:51  -3.36  -3.25   -36.864452    10     3        +1.0000
iter:  23  00:19:54  -2.91  -3.28   -36.863111    8      3        +1.0000
iter:  24  00:19:56  -2.85  -3.20   -36.864880    10     3        +1.0000
iter:  25  00:19:59  -4.44  -3.69   -36.865050    10     3        +1.0000
iter:  26  00:20:02  -5.50  -3.89   -36.865009    10     2        +1.0000
iter:  27  00:20:04  -6.36  -3.93   -36.865061    10     2        +1.0000
iter:  28  00:20:07  -7.05  -3.99   -36.864988    10     1        +1.0000
iter:  29  00:20:09  -6.21  -4.01   -36.865087    10     2        +1.0000
iter:  30  00:20:12  -7.31  -4.14   -36.865088    10     1        +1.0000
iter:  31  00:20:14  -7.26  -4.14   -36.865234    10     1        +1.0000
iter:  32  00:20:17  -7.53  -4.13   -36.865212    10     1        +1.0000

Converged after 32 iterations.

Dipole moment: (-0.000000, 0.000001, 0.212896) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.999907)
Local magnetic moments:
   0 O  ( 0.000000,  0.000000, -0.031485)
   1 H  ( 0.000000,  0.000000,  0.047710)
   2 H  ( 0.000000,  0.000000,  0.047710)

Energy contributions relative to reference atoms: (reference = -1511.033609)

Kinetic:        +35.253855
Potential:      -42.294498
External:        +0.000000
XC:             -30.548456
Entropy (-ST):   -0.000403
Local:           +0.724088
--------------------------
Free energy:    -36.865413
Extrapolated:   -36.865212

Spin contamination: 0.085338 electrons
Fermi levels: -2.35419, -4.35369

                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -31.08370    1.00000    -32.15446    1.00000
    1    -18.39661    1.00000    -18.99226    1.00000
    2    -15.48830    1.00000    -16.24710    1.00000
    3    -13.62996    1.00000    -14.52072    1.00000
    4     -3.19964    0.99979     -1.95629    0.00000
    5     -1.50875    0.00021     -0.18792    0.00000

Input parameters:
  setups: {0: paw}

Timing:                              incl.     excl.
-----------------------------------------------------------
Hamiltonian:                         5.560     0.004   0.0% |
 Atomic:                             0.073     0.073   0.1% |
  XC Correction:                     0.000     0.000   0.0% |
 Calculate atomic Hamiltonians:      0.002     0.002   0.0% |
 Communicate:                        0.115     0.115   0.1% |
 Hartree integrate/restrict:         0.034     0.034   0.0% |
 Initialize Hamiltonian:             0.000     0.000   0.0% |
 Poisson:                            3.311     3.311   3.0% ||
 XC 3D grid:                         2.013     2.013   1.8% ||
 vbar:                               0.007     0.007   0.0% |
LCAO initialization:                 0.145     0.108   0.1% |
 LCAO eigensolver:                   0.006     0.000   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.000     0.000   0.0% |
  Orbital Layouts:                   0.000     0.000   0.0% |
  Potential matrix:                  0.004     0.004   0.0% |
 LCAO to grid:                       0.008     0.008   0.0% |
 Set positions (LCAO WFS):           0.024     0.019   0.0% |
  Basic WFS set positions:           0.001     0.001   0.0% |
  Basis functions set positions:     0.000     0.000   0.0% |
  TCI: Calculate S, T, P:            0.003     0.003   0.0% |
SCF-cycle:                         103.403     0.130   0.1% |
 Density:                            1.917     0.002   0.0% |
  Atomic density matrices:           0.045     0.045   0.0% |
  Mix:                               1.679     1.679   1.5% ||
  Multipole moments:                 0.010     0.010   0.0% |
  Pseudo density:                    0.182     0.179   0.2% |
   Symmetrize density:               0.003     0.003   0.0% |
 Hamiltonian:                       90.697     0.096   0.1% |
  Atomic:                            2.269     2.269   2.0% ||
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.041     0.041   0.0% |
  Communicate:                       2.992     2.992   2.7% ||
  Hartree integrate/restrict:        0.983     0.983   0.9% |
  Poisson:                          25.487    25.487  23.0% |--------|
  XC 3D grid:                       58.592    58.592  52.9% |--------------------|
  vbar:                              0.237     0.237   0.2% |
 Orthonormalize:                     0.349     0.010   0.0% |
  calc_s_matrix:                     0.111     0.111   0.1% |
  inverse-cholesky:                  0.048     0.048   0.0% |
  projections:                       0.022     0.022   0.0% |
  rotate_psi_s:                      0.159     0.159   0.1% |
 RMM-DIIS:                           8.976     0.550   0.5% |
  Apply hamiltonian:                 0.827     0.827   0.7% |
  Calculate residuals:               0.258     0.258   0.2% |
  DIIS step:                         3.949     0.527   0.5% |
   Calculate residuals:              1.001     0.173   0.2% |
    Apply hamiltonian:               0.827     0.827   0.7% |
   Construct matrix:                 0.372     0.372   0.3% |
   Linear solve:                     0.032     0.032   0.0% |
   Update trial vectors:             0.602     0.602   0.5% |
   precondition:                     1.415     1.415   1.3% ||
  Find lambda:                       0.237     0.237   0.2% |
  Update psi:                        0.298     0.298   0.3% |
  precondition:                      2.834     2.834   2.6% ||
  projections:                       0.022     0.022   0.0% |
 Subspace diag:                      1.333     0.009   0.0% |
  calc_h_matrix:                     0.962     0.165   0.1% |
   Apply hamiltonian:                0.797     0.797   0.7% |
  diagonalize:                       0.022     0.022   0.0% |
  rotate_psi:                        0.339     0.339   0.3% |
Set symmetry:                        0.002     0.002   0.0% |
Other:                               1.648     1.648   1.5% ||
-----------------------------------------------------------
Total:                                       110.758 100.0%

Memory usage: 192.84 MiB
Date: Sat Apr 21 00:20:17 2018
