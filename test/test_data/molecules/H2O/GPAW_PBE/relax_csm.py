
from ase import io, optimize
from ase.units import Pascal, m
from ase.constraints import FixedPlane

from gpaw import FermiDirac
from gpaw.solvation.poisson import WeightedFDPoissonSolver
from ase.data.vdw import vdw_radii

from gpaw.solvation import (
    SolvationGPAW,
    EffectivePotentialCavity,
    Power12Potential,
    LinearDielectric,
    GradientSurface,
    SurfaceInteraction,
)

epsinf = 78.36
u0 = 0.180
gamma = 18.4 * 1e-3 * Pascal * m
T = 298.15
vdw_radii = vdw_radii[:]
vdw_radii[1] = 1.09
atomic_radii = lambda atoms: [vdw_radii[n] for n in atoms.numbers]

Oup = [6, 7, 11]
Odn = [28, 30, 32]

s = io.read('ini.xyz')

h = 0.2

constr = []
for ia in (Oup + Odn):
    constr.append(FixedPlane(ia, [0,1,0]))
#s.set_constraint(constr)

c = SolvationGPAW(
    poissonsolver=WeightedFDPoissonSolver(eps=1.e-5),
    xc='PBE', h=h, spinpol=False,
    #             mixer=Mixer(beta=0.1, nmaxold=5, weight=100),
    occupations=FermiDirac(width=0.1),
    eigensolver='rmm-diis',
    maxiter=300,
    cavity=EffectivePotentialCavity(
    effective_potential=Power12Potential(atomic_radii, u0),
    temperature=T,
    surface_calculator=GradientSurface()
    ),
    dielectric=LinearDielectric(epsinf=epsinf),
    interactions=[SurfaceInteraction(surface_tension=gamma)],
    )
s.set_calculator(c)

dyn = optimize.FIRE(s)
dyn.run(fmax=0.05)
