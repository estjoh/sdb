
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  1.4.1b1
 |___|_|             

User:   mw@mmos1
Date:   Fri Nov  9 13:59:01 2018
Arch:   x86_64
Pid:    14734
Python: 3.6.6
gpaw:   /home/mw/source/gpaw/trunk/gpaw (76821075c4)
_gpaw:  /home/mw/source/gpaw/trunk/build/bin.linux-x86_64-3.6/
        gpaw-python (c77868648e)
ase:    /home/mw/source/ase/trunk/ase (version 3.16.3b1-daf0a3e1fa)
numpy:  /usr/lib/python3/dist-packages/numpy (version 1.13.3)
scipy:  /usr/lib/python3/dist-packages/scipy (version 0.19.1)
units:  Angstrom and eV
cores:  4

Input parameters:
  h: 0.15
  occupations: {name: fermi-dirac,
                width: 0.1}
  spinpol: False
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

O-setup:
  name: Oxygen
  id: c7d727ddbf81696289a2bba6bb064aec
  Z: 8
  valence: 6
  core: 2
  charge: 0.0
  file: /home/mw/phys/dft/gridpaw/setups/gpaw-setups-0.6.6300/O.PBE.gz
  cutoffs: 0.74(comp), 1.30(filt), 0.83(core), lmax=2
  valence states:
                energy  radius
    2s(2.00)   -23.961   0.741
    2p(4.00)    -9.029   0.741
    *s           3.251   0.741
    *p          18.182   0.741
    *d           0.000   0.741

  Using partial waves for O as LCAO basis

H-setup:
  name: Hydrogen
  id: d65de229564ff8ea4db303e23b6d1ecf
  Z: 1
  valence: 1
  core: 0
  charge: 0.0
  file: /home/mw/phys/dft/gridpaw/setups/gpaw-setups-0.6.6300/H.PBE.gz
  cutoffs: 0.48(comp), 0.85(filt), 0.53(core), lmax=2
  valence states:
                energy  radius
    1s(1.00)    -6.494   0.476
    *s          20.717   0.476
    *p           0.000   0.476

  Using partial waves for H as LCAO basis

Reference energy: -2065.832616

Spin-paired calculation

Occupation numbers:
  Fermi-Dirac: width=0.1000 eV

Convergence criteria:
  Maximum total energy change: 0.0005 eV / electron
  Maximum integral of absolute density change: 0.0001 electrons
  Maximum integral of absolute eigenstate change: 4e-08 eV^2
  Maximum number of iterations: 333

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: Uniform real-space grid
  Kinetic energy operator: 6*3+1=19 point O(h^6) finite-difference Laplacian
  ScaLapack parameters: grid=1x1, blocksize=None 

Eigensolver
   Davidson(niter=1, smin=None, normalize=True) 

Densities:
  Coarse grid: 56*64*60 grid
  Fine grid: 112*128*120 grid
  Total Charge: 0.000000 

Density mixing:
  Method: separate
  Backend: pulay
  Linear mixing parameter: 0.25
  Mixing with 3 old densities
  No damping of long wave oscillations 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 112*128*120 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    6*3+1=19 point O(h^6) finite-difference Laplacian stencil;
    FFT axes: [];
    FST axes: [0, 2, 1].
 

Memory estimate:
  Process memory now: 108.62 MiB
  Calculator: 26.55 MiB
    Density: 13.31 MiB
      Arrays: 10.20 MiB
      Localized functions: 0.85 MiB
      Mixer: 2.26 MiB
    Hamiltonian: 6.74 MiB
      Arrays: 6.67 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.06 MiB
    Wavefunctions: 6.50 MiB
      Arrays psit_nG: 2.26 MiB
      Eigensolver: 4.15 MiB
      Projections: 0.00 MiB
      Projectors: 0.08 MiB

Total number of cores used: 4
Domain decomposition: 1 x 2 x 2

Number of atoms: 3
Number of atomic orbitals: 6
Number of bands in calculation: 6
Bands to converge: occupied states only
Number of valence electrons: 8

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
Creating initial wave functions:
  6 bands from LCAO basis set

       .--------------------.  
      /|                    |  
     / |                    |  
    /  |                    |  
   /   |                    |  
  /    |                    |  
 *     |                    |  
 |     |                    |  
 |     |       O            |  
 |     |      H             |  
 |     |                    |  
 |     .--------------------.  
 |    /                    /   
 |   /                    /    
 |  /                    /     
 | /                    /      
 |/                    /       
 *--------------------*        

Positions:
   0 O      4.200000    4.800000    4.801900    ( 0.0000,  0.0000,  0.0000)
   1 H      4.200000    5.566800    4.198100    ( 0.0000,  0.0000,  0.0000)
   2 H      4.200000    4.033200    4.198100    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no     8.400000    0.000000    0.000000    56     0.1500
  2. axis:    no     0.000000    9.600000    0.000000    64     0.1500
  3. axis:    no     0.000000    0.000000    9.000000    60     0.1500

  Lengths:   8.400000   9.600000   9.000000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.1500

                     log10-error:    total        iterations:
           time      wfs    density  energy       fermi  poisson
iter:   1  13:59:04  +1.87   +inf   -15.796043    0      1      
iter:   2  13:59:04  +1.33   +inf   -16.099233    0             
iter:   3  13:59:04  +0.24   +inf   -16.151275    0             
iter:   4  13:59:04  +0.69  -0.75   -14.487688    0      1      
iter:   5  13:59:05  +0.54  -0.96   -14.301507    10     1      
iter:   6  13:59:06  +0.94  -1.21   -14.322746    0      1      
iter:   7  13:59:07  -0.15  -1.45   -14.321503    0      1      
iter:   8  13:59:07  +0.19  -2.23   -14.329091    0      1      
iter:   9  13:59:08  -0.50  -2.46   -14.333006    0      1      
iter:  10  13:59:09  -0.41  -2.42   -14.335793    0      1      
iter:  11  13:59:09  -0.92  -2.31   -14.336933    0      1      
iter:  12  13:59:10  -0.90  -2.29   -14.336452    0      1      
iter:  13  13:59:11  -1.62  -2.39   -14.335926    0      1      
iter:  14  13:59:11  -1.55  -2.47   -14.335566    0      1      
iter:  15  13:59:12  -1.71  -2.74   -14.335636    0      1      
iter:  16  13:59:13  -2.44  -2.85   -14.335647    0      1      
iter:  17  13:59:14  -2.67  -3.02   -14.335657    0      1      
iter:  18  13:59:14  -2.71  -3.30   -14.335660    0      1      
iter:  19  13:59:15  -3.31  -3.37   -14.335664    0      1      
iter:  20  13:59:16  -3.24  -3.64   -14.335666    0      1      
iter:  21  13:59:16  -3.76  -3.66   -14.335666    0      1      
iter:  22  13:59:17  -3.79  -4.39   -14.335667    0      1      
iter:  23  13:59:18  -4.35  -4.65   -14.335667    0      1      
iter:  24  13:59:18  -4.28  -4.61   -14.335668    0      1      
iter:  25  13:59:19  -4.77  -4.46   -14.335668    0      1      
iter:  26  13:59:20  -4.72  -4.45   -14.335668    0      1      
iter:  27  13:59:20  -5.28  -4.63   -14.335668    0      1      
iter:  28  13:59:21  -5.17  -4.90   -14.335668    0      1      
iter:  29  13:59:22  -5.72  -5.00   -14.335668    0      1      
iter:  30  13:59:23  -5.63  -5.51   -14.335668    0      1      
iter:  31  13:59:23  -6.19  -5.58   -14.335668    0      1      
iter:  32  13:59:24  -6.08  -5.69   -14.335668    0      1      
iter:  33  13:59:25  -6.62  -5.81   -14.335668    0      1      
iter:  34  13:59:25  -6.48  -5.87   -14.335668    0      1      
iter:  35  13:59:26  -7.07  -6.11   -14.335668    0      1      
iter:  36  13:59:27  -6.91  -6.44   -14.335668    0      1      
iter:  37  13:59:27  -7.50  -6.64   -14.335668    0      1      

Converged after 37 iterations.

Dipole moment: (0.000000, 0.000000, -0.380699) |e|*Ang

Energy contributions relative to reference atoms: (reference = -2065.832616)

Kinetic:        +14.989476
Potential:      -17.026307
External:        +0.000000
XC:             -12.407667
Entropy (-ST):   -0.000000
Local:           +0.108831
--------------------------
Free energy:    -14.335668
Extrapolated:   -14.335668

Fermi level: -4.41228

 Band  Eigenvalues  Occupancy
    0    -25.23188    2.00000
    1    -13.00445    2.00000
    2     -9.37480    2.00000
    3     -7.23537    2.00000
    4     -0.80941    0.00000
    5      0.95588    0.00000


Forces in eV/Ang:
  0 O     0.00000    0.00000   -0.00239
  1 H     0.00000    0.00752   -0.00020
  2 H     0.00000   -0.00752   -0.00020

      Step     Time          Energy         fmax
*Force-consistent energies used in optimization.
FIRE:    0 13:59:27      -14.335668*       0.0075
PBE -14.3356680903
TPSS -16.655234689
M06L -15.9887282555
Timing:                              incl.     excl.
-----------------------------------------------------------
Forces:                              0.009     0.009   0.0% |
Hamiltonian:                         0.615     0.000   0.0% |
 Atomic:                             0.023     0.023   0.1% |
  XC Correction:                     0.000     0.000   0.0% |
 Calculate atomic Hamiltonians:      0.001     0.001   0.0% |
 Communicate:                        0.018     0.018   0.1% |
 Hartree integrate/restrict:         0.015     0.015   0.0% |
 Initialize Hamiltonian:             0.000     0.000   0.0% |
 Poisson:                            0.164     0.007   0.0% |
  Communicate bwd 0:                 0.030     0.030   0.1% |
  Communicate bwd 1:                 0.029     0.029   0.1% |
  Communicate fwd 0:                 0.038     0.038   0.1% |
  Communicate fwd 1:                 0.029     0.029   0.1% |
  fft:                               0.010     0.010   0.0% |
  fft2:                              0.022     0.022   0.1% |
 XC 3D grid:                         0.391     0.391   1.2% |
 vbar:                               0.003     0.003   0.0% |
LCAO initialization:                 0.047     0.039   0.1% |
 LCAO eigensolver:                   0.002     0.000   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.000     0.000   0.0% |
  Orbital Layouts:                   0.000     0.000   0.0% |
  Potential matrix:                  0.002     0.002   0.0% |
 LCAO to grid:                       0.003     0.003   0.0% |
 Set positions (LCAO WFS):           0.003     0.000   0.0% |
  Basic WFS set positions:           0.000     0.000   0.0% |
  Basis functions set positions:     0.000     0.000   0.0% |
  P tci:                             0.001     0.001   0.0% |
  ST tci:                            0.000     0.000   0.0% |
  mktci:                             0.001     0.001   0.0% |
SCF-cycle:                          23.833     0.019   0.1% |
 Davidson:                           2.367     0.698   2.2% ||
  Apply hamiltonian:                 0.324     0.324   1.0% |
  Subspace diag:                     0.586     0.004   0.0% |
   calc_h_matrix:                    0.392     0.069   0.2% |
    Apply hamiltonian:               0.323     0.323   1.0% |
   diagonalize:                      0.009     0.009   0.0% |
   rotate_psi:                       0.182     0.182   0.6% |
  calc. matrices:                    0.569     0.242   0.7% |
   Apply hamiltonian:                0.327     0.327   1.0% |
  diagonalize:                       0.008     0.008   0.0% |
  rotate_psi:                        0.182     0.182   0.6% |
 Density:                            0.592     0.001   0.0% |
  Atomic density matrices:           0.019     0.019   0.1% |
  Mix:                               0.524     0.524   1.6% ||
  Multipole moments:                 0.004     0.004   0.0% |
  Pseudo density:                    0.044     0.044   0.1% |
   Symmetrize density:               0.001     0.001   0.0% |
 Hamiltonian:                       20.851     0.006   0.0% |
/home/mw/source/gpaw/trunk/gpaw/xc/__init__.py:104: UserWarning: Please use M06-L instead of M06L
  warnings.warn('Please use M06-L instead of M06L')
/home/mw/source/gpaw/trunk/gpaw/xc/__init__.py:104: UserWarning: Please use M06-L instead of M06L
  warnings.warn('Please use M06-L instead of M06L')
  Atomic:                            0.825     0.825   2.5% ||
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.033     0.033   0.1% |
  Communicate:                       0.585     0.585   1.8% ||
  Hartree integrate/restrict:        0.496     0.496   1.5% ||
  Poisson:                           5.226     0.237   0.7% |
   Communicate bwd 0:                1.025     1.025   3.2% ||
   Communicate bwd 1:                0.972     0.972   3.0% ||
   Communicate fwd 0:                0.945     0.945   2.9% ||
   Communicate fwd 1:                0.980     0.980   3.0% ||
   fft:                              0.327     0.327   1.0% |
   fft2:                             0.742     0.742   2.3% ||
  XC 3D grid:                       13.559    13.559  41.9% |----------------|
  vbar:                              0.120     0.120   0.4% |
 Orthonormalize:                     0.005     0.000   0.0% |
  calc_s_matrix:                     0.001     0.001   0.0% |
  inverse-cholesky:                  0.000     0.000   0.0% |
  projections:                       0.000     0.000   0.0% |
  rotate_psi_s:                      0.003     0.003   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
Other:                               7.880     7.880  24.3% |---------|
-----------------------------------------------------------
Total:                                        32.386 100.0%

/home/mw/source/gpaw/trunk/gpaw/xc/__init__.py:104: UserWarning: Please use M06-L instead of M06L
  warnings.warn('Please use M06-L instead of M06L')
Memory usage: 137.71 MiB
Date: Fri Nov  9 13:59:34 2018
/home/mw/source/gpaw/trunk/gpaw/xc/__init__.py:104: UserWarning: Please use M06-L instead of M06L
  warnings.warn('Please use M06-L instead of M06L')
