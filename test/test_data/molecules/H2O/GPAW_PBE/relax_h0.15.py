import os

from ase import optimize
from ase.parallel import parprint

from gpaw import *
from gpaw.cluster import Cluster

fname='ini.xyz'
h=.15
q=0
#spin=True
spin=False
box=4.

ftraj = 'relax.traj'
if os.path.exists(ftraj):
    s = Cluster(ftraj)
else:
    s = Cluster(fname)
    s.minimal_box(box, h=h)
    if spin:
        mm = [0] * len(s)
        for i in [0]:
            mm[i] = 1.
        s.set_initial_magnetic_moments(mm)
    
c = GPAW(xc='PBE', 
         charge=q, spinpol=spin, h=h,
#         mixer=MixerDif(beta=0.05, nmaxold=5, weight=50.0),
         occupations=FermiDirac(width=0.1)
         )
s.set_calculator(c)

# Find optimal height.  The stopping criteria is: the force on the
# H atom should be less than 0.05 eV/Ang
dyn = optimize.FIRE(s)
dyn.run(fmax=0.05)

#c.write('PBE.gpw')
E_PBE=c.get_potential_energy()

for xc in ['PBE', 'TPSS', 'M06L']:
    dE = c.get_xc_difference(xc)
    parprint(xc, E_PBE + dE)


