from ase.parallel import parprint

from gpaw import GPAW, mpi
from gpaw.lrtddft import LrTDDFT
from gpaw.pes.dos import DOSPES
from gpaw.pes.tddft import TDDFTPES
from gpaw.cluster import Cluster

xc='PBE'
h=0.2

H2O=Cluster('gpaw.out')
calc = GPAW(xc=xc, h=h, 
            parallel={'domain': mpi.world.size},
            )
H2O.set_calculator(calc)
e_H2O = H2O.get_potential_energy()

calc_plus = GPAW(nbands=100, h=h, 
                 parallel={'domain': mpi.world.size},
                 convergence={'bands':90, 'eigenstates':1e-3, 'energy':1e-3})
calc_plus.set(charge=+1)
H2O.set_calculator(calc_plus)
e_H2_plus = H2O.get_potential_energy()

out = 'dospes_Gauss.dat'
pes = DOSPES(calc, calc_plus, shift=True)
pes.save_folded_pes(filename=out, folding=None)
parprint('DOS:')
pes.save_folded_pes(filename='dospes.dat', folding=None)

lr = LrTDDFT(calc_plus, xc=xc)

out = 'lrpes_Gauss.dat'
pes = TDDFTPES(calc, lr)
pes.save_folded_pes(filename=out, folding='Gauss')
parprint('Linear response:')
pes.save_folded_pes(filename='lrdos.dat', folding=None)
