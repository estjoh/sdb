#! /usr/bin/env python


import numpy as np

import ase
import gpaw


from gpaw.mixer import BroydenMixer
from gpaw.cluster import Cluster
from gpaw.occupations import FermiDirac

###

h    = 0.16
vac  = 6.0

###

mol = Cluster(ase.data.molecules.molecule('H2O'))
mol.minimal_box(border=vac, h=h)

# Calculator
calc  = gpaw.GPAW(
    h            = h,
    nbands       = -10,
    occupations  = FermiDirac(0.01),
    spinpol      = True,
    xc           = 'PBE',
    mixer        = BroydenMixer(beta=0.3),
    txt          = 'gpaw.out'
    )
mol.set_calculator(calc)

ase.optimize.FIRE(mol, trajectory='gpaw.traj').run(fmax=0.1)

emol  = mol.get_potential_energy()
dmol  = mol.get_dipole_moment()
calc.write('final.gpw')

np.savetxt('e.out', [ emol ])
np.savetxt('d.out', [ dmol ])

