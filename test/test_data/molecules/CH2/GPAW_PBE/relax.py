from ase import optimize
from ase.parallel import parprint

from gpaw import GPAW, FermiDirac
from gpaw.cluster import Cluster

fname = 'ini.xyz'
h = 0.2
q = 0
# spin=True
spin = False
box = 4.

s = Cluster(fname)
s.minimal_box(box, h=h)
if spin:
    mm = [0] * len(s)
    for i in [0]:
        mm[i] = 1.
    s.set_initial_magnetic_moments(mm)

s.calc = GPAW(xc='PBE',
              charge=q, spinpol=spin, h=h,
              #         mixer=MixerDif(beta=0.05, nmaxold=5, weight=50.0),
              occupations=FermiDirac(width=0.1))

dyn = optimize.FIRE(s)
dyn.run(fmax=0.05)

E_PBE = s.calc.get_potential_energy()

for xc in ['PBE', 'TPSS', 'M06-L']:
    dE = s.calc.get_xc_difference(xc)
    parprint(xc, E_PBE + dE)
