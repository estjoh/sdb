##########################################################################
# exden.tcl
# plot the density files using gOpenMol
#

cd "/home/mw/phys/structure_database/molecules/F4S/GPAW_PBE/1ch/PBE_h0.2_3_F1s_wfs"

#NOTE:  comment lines start with a # character
# simple demo tcl script 
#
    global env
#

#  I like the orthographic, rather than perspective view
#define projection orthographic

#white backgroud
define bgcolour white
#improve display quality for publication
#define cylinderquality 100
#define spherequality 100

#make atoms black
#atom  color * * * Black

# CPK display
atom cpk
atom scale cpk 0.2 * * *

#make matte finish
#define material specular red 0.0 
#define material specular blue 0.0 
#define material specular green 0.0

# load the coordinates
import coord xyz struct.xyz
rotate display 0.0 0.0 0.0

#load up a contour file, give the name mo5
#contour file exden_1.plt mo5
#plot contours -- -0.2 is red, +0.2 is blue
#contour plot mo5 -0.005 red 0.0005 blue
#display

set min 0.1
puts "**** hi"

for {set i 0} { $i < 20 } {incr i} {

    puts "**** ho"
    set basename "wf$i"
    puts "****  $basename"
    append basename "_s0"
    set infn "$basename.plt"
    set outfn "$basename.jpg"
    puts "****  $infn -> $outfn"

    plot text black 0.1 0.9 "$basename"

    contour file $infn $basename
    contour plot $basename -$min blue $min green
    contour alpha $basename .6
    display
    eval "hardcopy 1 jpg $outfn"
    contour display $basename off
    plot -text
#    pause 1

}
