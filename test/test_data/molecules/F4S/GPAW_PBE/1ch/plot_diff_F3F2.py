import pylab as plt
import matplotlib.lines as lines
from ase import io

color = 'br'

def plot(ax, iF=2):
    for i, xc in enumerate(['PBE', '1ch_LDA']):
        t = io.read('{0}_h0.18_box4.out@:'.format(xc))
        sF3 = t[3]
        sF2 = t[4]

        cF2 = sF2.get_calculator()
        cF3 = sF3.get_calculator()
        eps2_n = cF2.kpts[0].eps_n
        eps3_n = cF3.kpts[0].eps_n
        deps_n = eps3_n - eps2_n
        f_n = cF3.kpts[0].f_n

        def plotsticks(X, Y):
            for x, y in zip(X, Y):
                 l = lines.Line2D([x, x], [0, y], color=color[i], lw=1)
                 ax.add_line(l)

        if iF == 2:
            plt.plot(eps2_n, deps_n, label=xc, color=color[i], lw=0)
            plotsticks(eps2_n, deps_n)
        else:
            plt.plot(eps3_n, deps_n, label=xc, color=color[i], lw=0)
            plotsticks(eps3_n, deps_n)


ax = plt.subplot(211)
plot(ax)
plt.legend()

ax = plt.subplot(212)
plot(ax, 3)

plt.show()
