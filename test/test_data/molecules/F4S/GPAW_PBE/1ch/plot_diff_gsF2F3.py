import pylab as plt
import matplotlib.lines as lines
from ase import io

color = 'br'

def plot(ax, iF=2):
    for i, xc in enumerate(['PBE']):
        t = io.read('{0}_h0.18_box4.out@:'.format(xc))
        gs = t[0]
        sF = t[iF + 1]

        cgs = gs.get_calculator()
        cF = sF.get_calculator()
        epsgs_n = cgs.kpts[0].eps_n
        epsF_n = cF.kpts[0].eps_n

        def plotsticks(X, Y):
            for x, y in zip(X, Y):
                 l = lines.Line2D([x, x], [0, y], color=color[i], lw=1)
                 ax.add_line(l)

        plt.plot(epsgs_n, epsF_n - epsgs_n, label=xc, color=color[i], lw=0)
        plotsticks(epsgs_n, epsF_n - epsgs_n)


ax = plt.subplot(211)
plot(ax)
plt.legend()

ax = plt.subplot(212)
plot(ax, 3)

plt.show()
