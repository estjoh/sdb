import os
import sys

from ase import *
from ase.io import write
from gpaw import *
from gpaw.cluster import Cluster

h=0.2
q=0
ia = int(sys.argv[1])

# core hole mapping
corehole = {
    'C' : '1s',
    'F' : '1s',
    'N' : '1s',
    'O' : '1s',
    'Al' : '2p',
    'Si' : '2p',
    'P' : '2p',
    'S' : '2p',
    'Fe' : '2p',
    'Mo' : '3d',
}    

s = Cluster('../relax.py.out')
gpwname = 'PBE_h{0}_{1}_{2}{3}.gpw'.format(h, ia, s[ia].symbol, 
                                           corehole[s[ia].symbol])
try:
    c = GPAW(gpwname)
    c.converge_wave_functions()
except IOError:
    c = GPAW(xc='PBE', 
             charge=q, h=h,
             #         mixer=MixerDif(beta=0.05, nmaxold=5, weight=50.0),
             occupations=FermiDirac(width=0.1),
             setups={ia:'{0}1ch'.format(corehole[s[ia].symbol])},
         )
    c.calculate(s)
    c.write(gpwname)

# write wf files out to plot them
dirname = 'PBE_h{0}_{1}_{2}{3}_wfs'.format(h, ia, s[ia].symbol, 
                                           corehole[s[ia].symbol])
if not os.path.exists(dirname):
    os.makedirs(dirname)
for spin in [0]:
    for band in range(20):
        wf = c.get_pseudo_wave_function(band=band, spin=spin)
        fname = '%s/wf%d_s%d' % (dirname, band, spin) + '.plt'
        write(fname, s, data=wf)
    
