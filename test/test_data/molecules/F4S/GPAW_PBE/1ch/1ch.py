from __future__ import print_function
import sys
import os
from optparse import OptionParser

sys.path.insert(1, '/home/mw/phys/structure_database')
from molecules import *
from xps import state

from ase.parallel import paropen
from ase.units import Hartree

from gpaw.cluster import Cluster
from gpaw import GPAW, FermiDirac

parser = OptionParser()
parser.add_option("-H", "--h", dest="h",
                  help="Grid spacing", default=0.18)
parser.add_option("-f", "--xcfunctional", dest="XC", default='PBE',
                  help="Exchange-Correlation functional (default value PBE)")

(options, args) = parser.parse_args()

xc = options.XC
h = float(options.h)
box = 4


datf = os.path.join('1ch_by_atom_{0}_h{1}_box{2}_check.dat'.format(
    xc, h, box))
outf = os.path.join('1ch_{0}_h{1}_box{2}.out'.format(
    xc, h, box))
s = Cluster('../relax.py.out')
s.set_initial_magnetic_moments([0] * len(s))
s.minimal_box(box, h=h)
c = GPAW(xc=xc, h=h, charge=0,
         occupations=FermiDirac(width=0.1),
         txt=outf)
s.set_calculator(c)
E0 = s.get_potential_energy()
E0 += c.wfs.setups.Eref * Hartree

f = paropen(datf, 'a')
print('# energy', E0, file=f)
print('# i  E(1s1ch)  Eref   element', file=f)
for ia, a in enumerate(s[2:4]):
    if a.symbol in state:
        c.set(setups={ia:'{}1ch'.format(state[a.symbol])})
        s.set_calculator(c)
        E = s.get_potential_energy()
        E += c.wfs.setups.Eref * Hartree
        print(ia, E - E0, E, a.symbol, file=f)
f.close()
