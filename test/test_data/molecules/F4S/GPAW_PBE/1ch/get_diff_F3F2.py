import sys
from ase import io

t = io.read(sys.argv[1] + '@:')
sF3 = t[3]
sF2 = t[4]

cF2 = sF2.get_calculator()
cF3 = sF3.get_calculator()
deps_n = cF3.kpts[0].eps_n - cF2.kpts[0].eps_n
f_n = cF3.kpts[0].f_n
i = 0
for e2, e, f in zip(cF2.kpts[0].eps_n, deps_n, f_n):
    print '{2:2d} {0:5.2f} {1:7.2f} {3:7.2f}'.format(f, e, i, e2)
    i += 1
