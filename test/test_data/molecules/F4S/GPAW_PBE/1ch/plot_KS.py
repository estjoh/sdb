import sys
import pylab as plt
from mpl_toolkits.axes_grid.axislines import Subplot

from ase import io

t = io.read('{0}_h0.18_box4.out@:'.format('PBE'))

cgs = t[0].get_calculator()
cF2 = t[3].get_calculator()
cF3 = t[4].get_calculator()

fig = plt.figure()
ax = Subplot(fig, 111)
fig.add_subplot(ax)
ax.axis["right"].set_visible(False)
ax.axis["top"].set_visible(False)
ax.axis["bottom"].set_visible(False)

height = 0.2

def show(c, left):
    eF = c.get_fermi_level()
    plt.plot((left, left + 1), (eF, eF), linewidth=2, color='r', ls=':')
#    plt.axhline(y=eF, xmin=left, xmax=float(left + 1), 
#                linewidth=2, color='r', ls='--')

    x = c.kpts[0].eps_n
    plt.barh(x - 0.5 * height, 
             width=[1] * len(x),
             left=left, height=height, linewidth=0, color='k')

show(cgs, 5)
show(cF2, 2)
show(cF3, 8)
plt.xlim([-0.5, 11.5])

plt.savefig(sys.argv[0].replace('.', '_') + '.svg')
plt.show()
