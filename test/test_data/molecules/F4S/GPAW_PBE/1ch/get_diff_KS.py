from __future__ import print_function
import sys
from ase import io

t = io.read(sys.argv[1] + '@:')
sF3 = t[4]
sF2 = t[3]

print('total energy diff F3 - F2', 
      sF3.get_potential_energy() - sF2.get_potential_energy()) 
cF2 = sF2.get_calculator()
cF3 = sF3.get_calculator()
#print(dir(cF2))

deps_n = cF3.kpts[0].eps_n - cF2.kpts[0].eps_n
f_n = cF3.kpts[0].f_n
print('Valence KS energy F2', (cF2.kpts[0].eps_n * f_n).sum()) 
print('Valence KS energy F3', (cF3.kpts[0].eps_n * f_n).sum()) 
print('Valence KS energy diff F3 - F2', 
      (deps_n  * f_n).sum()) 
