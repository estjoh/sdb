import os

from ase import *
from ase.io import write
from gpaw import *
from gpaw.cluster import Cluster

h=0.2
q=0

s = Cluster('relax.py.out')
gpwname = 'PBE_h{0}.gpw'.format(h)
try:
    c = GPAW(gpwname)
    c.converge_wave_functions()
except IOError:
    c = GPAW(xc='PBE', 
             charge=q, h=h,
             #         mixer=MixerDif(beta=0.05, nmaxold=5, weight=50.0),
             occupations=FermiDirac(width=0.1)
         )
    c.calculate(s)
    c.write(gpwname)

# write wf files out to plot them
dirname = 'PBE_h{0}_wfs'.format(h)
if not os.path.exists(dirname):
    os.makedirs(dirname)
for spin in [0]:
    for band in range(20):
        wf = c.get_pseudo_wave_function(band=band, spin=spin)
        fname = '%s/wf%d_s%d' % (dirname, band, spin) + '.plt'
        write(fname, s, data=wf)
    
