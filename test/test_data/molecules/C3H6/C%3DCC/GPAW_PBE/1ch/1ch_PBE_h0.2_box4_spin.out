
  ___ ___ ___ _ _ _  
 |   |   |_  | | | | 
 | | | | | . | | | | 
 |__ |  _|___|_____|  24.7.0b1
 |___|_|             

User:   fr_mw767@n1611
Date:   Mon Oct  7 15:24:27 2024
Arch:   x86_64
Pid:    2808004
CWD:    /lustre/home/fr/fr_fr/fr_mw767/S2p_fit
Python: 3.11.1
gpaw:   /home/fr/fr_fr/fr_mw767/AG_Walter/source/gpaw/gpaw (0d99f9e070)
_gpaw:  /home/fr/fr_fr/fr_mw767/AG_Walter/source/gpaw/build/lib.linux-x86_64-cpython-311/
        _gpaw.cpython-311-x86_64-linux-gnu.so (0d99f9e070)
ase:    /home/fr/fr_fr/fr_mw767/AG_Walter/source/ase/ase (version 3.23.1b1-fd2283d28c)
numpy:  /opt/bwhpc/common/numlib/python_numpy/1.24.2_python-3.11.1/lib/python3.11/site-packages/numpy (version 1.24.2)
scipy:  /home/fr/fr_fr/fr_mw767/.site-packages/lib64/numpy-1.24.2_python-3.11.1/site-packages/scipy (version 1.14.1)
libxc:  6.2.2
units:  Angstrom and eV
cores: 48
OpenMP: False
OMP_NUM_THREADS: 1

Input parameters:
  convergence: {eigenstates: 0.001}
  h: 0.2
  mode: fd
  occupations: {fixmagmom: False,
                name: fermi-dirac,
                width: 0.1}
  setups: {2: 1s1ch,
           Cr: 14_1.55}
  spinpol: True
  xc: PBE

System changes: positions, numbers, cell, pbc, initial_charges, initial_magmoms 

Initialize ...

species:
  C:
    name: Carbon
    id: 4aa54d4b901d75f77cc0ea3eec22967b
    Z: 6.0
    valence: 4
    core: 2
    charge: 0.0
    file: /home/fr/fr_fr/fr_mw767/AG_Walter/source/gpaw-setups/gpaw-setups-0.6.6300/C.PBE.gz
    compensation charges: {type: gauss,
                           rc: 0.20,
                           lmax: 2}
    cutoffs: {filter: 1.14,
              core: 1.14}
    projectors:
      #              energy  rcut
      - 2s(2.00)   -13.751   0.635
      - 2p(2.00)    -5.284   0.635
      -  s          13.461   0.635
      -  p          21.927   0.635
      -  d           0.000   0.635
  
    # Using partial waves for C as LCAO basis

  C:  # (1.0 core hole)
    name: Carbon
    id: e16a8533f519786af9e640cb14921825
    Z: 6.0
    valence: 4
    core: 1.0
    charge: 1.0
    file: /home/fr/fr_fr/fr_mw767/AG_Walter/source/gpaw-setups/generate/C.1s1ch.PBE.gz
    compensation charges: {type: gauss,
                           rc: 0.20,
                           lmax: 2}
    cutoffs: {filter: 1.14,
              core: 1.01}
    projectors:
      #              energy  rcut
      - 2s(2.00)   -29.911   0.635
      - 2p(2.00)   -21.336   0.635
      -  s          -2.700   0.635
      -  p           5.875   0.635
      -  d           0.000   0.635
  
    # Using partial waves for C as LCAO basis

  H:
    name: Hydrogen
    id: d65de229564ff8ea4db303e23b6d1ecf
    Z: 1.0
    valence: 1
    core: 0
    charge: 0.0
    file: /home/fr/fr_fr/fr_mw767/AG_Walter/source/gpaw-setups/gpaw-setups-0.6.6300/H.PBE.gz
    compensation charges: {type: gauss,
                           rc: 0.15,
                           lmax: 2}
    cutoffs: {filter: 0.85,
              core: 0.53}
    projectors:
      #              energy  rcut
      - 1s(1.00)    -6.494   0.476
      -  s          20.717   0.476
      -  p           0.000   0.476
  
    # Using partial waves for H as LCAO basis

Reference energy: -2853.453386  # eV

Spin-polarized calculation.
Magnetic moment: 0.000000

Convergence criteria:
 Maximum [total energy] change in last 3 cyles: 0.0005 eV / valence electron
 Maximum integral of absolute [dens]ity change: 0.0001 electrons / valence electron
 Maximum integral of absolute [eigenst]ate change: 0.001 eV^2 / valence electron
 Maximum number of scf [iter]ations: 333
 (Square brackets indicate name in SCF output, whereas a 'c' in
 the SCF output indicates the quantity has converged.)

Symmetries present (total): 1

  ( 1  0  0)
  ( 0  1  0)
  ( 0  0  1)

1 k-point (Gamma)
1 k-point in the irreducible part of the Brillouin zone
       k-points in crystal coordinates                weights
   0:     0.00000000    0.00000000    0.00000000          1/1

Wave functions: Uniform real-space grid
  Kinetic energy operator: 6*3+1=19 point O(h^6) finite-difference Laplacian
  ScaLapack parameters: grid=1x1, blocksize=None
  Wavefunction extrapolation:
    Improved wavefunction reuse through dual PAW basis 

Occupation numbers: Fermi-Dirac:
  width: 0.1000  # eV
 

Eigensolver
   Davidson(niter=2) 

Densities:
  Coarse grid: 60*56*56 grid
  Fine grid: 120*112*112 grid
  Total Charge: 1.000000 

Density mixing:
  Method: difference
  Backend: pulay
  Linear mixing parameter: 0.25
  old densities: 3
  Damping of long wavelength oscillations: 1  # (no daming) 

Hamiltonian:
  XC and Coulomb potentials evaluated on a 120*112*112 grid
  Using the PBE Exchange-Correlation functional
  Interpolation: tri-quintic (5. degree polynomial)
  Poisson solver: FastPoissonSolver using
    Stencil: 6*3+1=19 point O(h^6) finite-difference Laplacian
    FFT axes: []
    FST axes: [1, 2, 0]
 

XC parameters: PBE with 2 nearest neighbor stencil

Memory estimate:
  Process memory now: 319.18 MiB
  Calculator: 3.27 MiB
    Density: 1.28 MiB
      Arrays: 0.96 MiB
      Localized functions: 0.07 MiB
      Mixer: 0.25 MiB
    Hamiltonian: 0.72 MiB
      Arrays: 0.71 MiB
      XC: 0.00 MiB
      Poisson: 0.00 MiB
      vbar: 0.01 MiB
    Wavefunctions: 1.27 MiB
      Arrays psit_nG: 0.75 MiB
      Eigensolver: 0.52 MiB
      Projections: 0.00 MiB
      Projectors: 0.01 MiB

Total number of cores used: 48
Domain decomposition: 4 x 4 x 3

Number of atoms: 9
Number of atomic orbitals: 18
Number of bands in calculation: 15
Number of valence electrons: 18
Bands to converge: occupied

... initialized

Initializing position-dependent things.

Density initialized from atomic densities
Creating initial wave functions:
  15 bands from LCAO basis set

        .----------------------------.  
       /|                            |  
      / |                            |  
     /  |                            |  
    /   |                            |  
   /    |                            |  
  /     |                            |  
 *      |                            |  
 |      |                            |  
 |      |           H                |  
 |      |            C H             |  
 |      |     H C  HC H              |  
 |      |                            |  
 |      |                            |  
 |      .----------------------------.  
 |     /                            /   
 |    /                            /    
 |   /                            /     
 |  /                            /      
 | /                            /       
 |/                            /        
 *----------------------------*         

Positions:
   0 C      4.779450    6.299750    4.898750    ( 0.0000,  0.0000,  0.0000)
   1 C      6.105150    6.189750    5.020950    ( 0.0000,  0.0000,  0.0000)
   2 C      6.811950    5.283850    5.996550    ( 0.0000,  0.0000,  0.0000)
   3 H      6.544150    5.525150    7.035850    ( 0.0000,  0.0000,  0.0000)
   4 H      7.903150    5.359050    5.911650    ( 0.0000,  0.0000,  0.0000)
   5 H      6.539950    4.231650    5.829950    ( 0.0000,  0.0000,  0.0000)
   6 H      4.327750    6.968350    4.164150    ( 0.0000,  0.0000,  0.0000)
   7 H      4.096850    5.720950    5.527550    ( 0.0000,  0.0000,  0.0000)
   8 H      6.736850    6.797950    4.365650    ( 0.0000,  0.0000,  0.0000)

Unit cell:
           periodic     x           y           z      points  spacing
  1. axis:    no    12.000000    0.000000    0.000000    60     0.2000
  2. axis:    no     0.000000   11.200000    0.000000    56     0.2000
  3. axis:    no     0.000000    0.000000   11.200000    56     0.2000

  Lengths:  12.000000  11.200000  11.200000
  Angles:   90.000000  90.000000  90.000000

Effective grid spacing dv^(1/3) = 0.2000

     iter     time        total  log10-change:  magmom
                         energy   eigst   dens
iter:   1 15:24:27   -70.646026                   +0.0000
iter:   2 15:24:27   -64.719194   +0.38  -0.55    -0.0000
iter:   3 15:24:27   -63.293515   +0.36  -0.76    -0.0000
iter:   4 15:24:28   -62.900978   +0.04  -0.96    +0.0000
iter:   5 15:24:28   -62.872260   -0.58  -1.40    +0.0000
iter:   6 15:24:28   -62.868430   -1.43  -1.93    +0.0000
iter:   7 15:24:28   -62.868613c  -1.85  -2.26    +0.0000
iter:   8 15:24:28   -62.868339c  -2.67  -2.64    +0.0000
iter:   9 15:24:28   -62.868402c  -3.02c -2.86    +0.0000
iter:  10 15:24:29   -62.868423c  -3.58c -3.09    +0.0000
iter:  11 15:24:29   -62.868418c  -4.03c -3.40    +0.0000
iter:  12 15:24:29   -62.868416c  -4.25c -3.59    +0.0000
iter:  13 15:24:29   -62.868416c  -4.77c -4.07c   +0.0000

Converged after 13 iterations.

Dipole moment: (6.500302, 5.434114, 5.832333) |e|*Ang

Total magnetic moment: (0.000000, 0.000000, 0.000000)
Local magnetic moments:
   0 C  ( 0.000000,  0.000000, -0.001793)
   1 C  ( 0.000000,  0.000000,  0.017454)
   2 C  ( 0.000000,  0.000000, -0.138569)
   3 H  ( 0.000000,  0.000000,  0.016207)
   4 H  ( 0.000000,  0.000000,  0.016079)
   5 H  ( 0.000000,  0.000000,  0.016151)
   6 H  ( 0.000000,  0.000000,  0.001137)
   7 H  ( 0.000000,  0.000000,  0.000560)
   8 H  ( 0.000000,  0.000000,  0.000618)

Energy contributions relative to reference atoms: (reference = -2853.453386)

Kinetic:        +43.608747
Potential:      -56.326505
External:        +0.000000
XC:             -50.650522
Entropy (-ST):   -0.000000
Local:           +0.499864
SIC:             +0.000000
--------------------------
Free energy:    -62.868416
Extrapolated:   -62.868416

Spin contamination: 1.479580 electrons
                   Up                     Down
 Band  Eigenvalues  Occupancy  Eigenvalues  Occupancy
    0    -28.23996    1.00000    -29.03126    1.00000
    1    -24.29054    1.00000    -24.42300    1.00000
    2    -20.69122    1.00000    -20.98609    1.00000
    3    -19.74803    1.00000    -20.21756    1.00000
    4    -19.69874    1.00000    -20.20800    1.00000
    5    -17.77401    1.00000    -17.94555    1.00000
    6    -16.00398    1.00000    -16.04837 Timing:                              incl.     excl.
-----------------------------------------------------------
Hamiltonian:                         0.125     0.000   0.0% |
 Atomic:                             0.000     0.000   0.0% |
  XC Correction:                     0.000     0.000   0.0% |
 Calculate atomic Hamiltonians:      0.000     0.000   0.0% |
 Communicate:                        0.060     0.060   0.6% |
 Hartree integrate/restrict:         0.001     0.001   0.0% |
 Initialize Hamiltonian:             0.000     0.000   0.0% |
 Poisson:                            0.037     0.000   0.0% |
  Communicate from 1D:               0.007     0.007   0.1% |
  Communicate from 2D:               0.009     0.009   0.1% |
  Communicate to 1D:                 0.008     0.008   0.1% |
  Communicate to 2D:                 0.010     0.010   0.1% |
  FFT 1D:                            0.001     0.001   0.0% |
  FFT 2D:                            0.002     0.002   0.0% |
 XC 3D grid:                         0.026     0.026   0.3% |
 vbar:                               0.000     0.000   0.0% |
LCAO initialization:                 0.039     0.018   0.2% |
 LCAO eigensolver:                   0.009     0.000   0.0% |
  Calculate projections:             0.000     0.000   0.0% |
  DenseAtomicCorrection:             0.000     0.000   0.0% |
  Distribute overlap matrix:         0.007     0.007   0.1% |
  Orbital Layouts:                   0.001     0.001   0.0% |
  Potential matrix:                  0.000     0.000   0.0% |
 LCAO to grid:                       0.000     0.000   0.0% |
 Set positions (LCAO WFS):           0.012     0.007   0.1% |
  Basic WFS set positions:           0.000     0.000   0.0% |
  Basis functions set positions:     0.000     0.000   0.0% |
  P tci:                             0.000     0.000   0.0% |
  ST tci:                            0.001     0.001   0.0% |
  mktci:                             0.004     0.004   0.0% |
SCF-cycle:                           2.240     0.018   0.2% |
 Davidson:                           0.622     0.267   2.6% ||
  Apply hamiltonian:                 0.050     0.050   0.5% |
  Subspace diag:                     0.074     0.002   0.0% |
   calc_h_matrix:                    0.054     0.008   0.1% |
    Apply hamiltonian:               0.046     0.046   0.4% |
   diagonalize:                      0.012     0.012   0.1% |
   rotate_psi:                       0.006     0.006   0.1% |
  calc. matrices:                    0.203     0.073   0.7% |
   Apply hamiltonian:                0.130     0.130   1.3% ||
  diagonalize:                       0.019     0.019   0.2% |
  rotate_psi:                        0.010     0.010   0.1% |
 Density:                            0.045     0.000   0.0% |
  Atomic density matrices:           0.016     0.016   0.2% |
  Mix:                               0.023     0.023   0.2% |
  Multipole moments:                 0.002     0.002   0.0% |
  Pseudo density:                    0.004     0.004   0.0% |
   Symmetrize density:               0.000     0.000   0.0% |
 Hamiltonian:                        1.542     0.002   0.0% |
  Atomic:                            0.002     0.002   0.0% |
   XC Correction:                    0.000     0.000   0.0% |
  Calculate atomic Hamiltonians:     0.003     0.003   0.0% |
  Communicate:                       0.777     0.777   7.6% |--|
  Hartree integrate/restrict:        0.015     0.015   0.1% |
  Poisson:                           0.422     0.004   0.0% |
   Communicate from 1D:              0.094     0.094   0.9% |
   Communicate from 2D:              0.103     0.103   1.0% |
   Communicate to 1D:                0.095     0.095   0.9% |
   Communicate to 2D:                0.101     0.101   1.0% |
   FFT 1D:                           0.007     0.007   0.1% |
   FFT 2D:                           0.017     0.017   0.2% |
  XC 3D grid:                        0.319     0.319   3.1% ||
  vbar:                              0.002     0.002   0.0% |
 Orthonormalize:                     0.014     0.000   0.0% |
  calc_s_matrix:                     0.001     0.001   0.0% |
  inverse-cholesky:                  0.012     0.012   0.1% |
  projections:                       0.000     0.000   0.0% |
  rotate_psi_s:                      0.000     0.000   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
Other:                               7.833     7.833  76.5% |------------------------------|
-----------------------------------------------------------
Total:                                        10.239 100.0%

Memory usage: 321.20 MiB
Date: Mon Oct  7 15:24:29 2024
0.002   0.1% |
 Orthonormalize:                     0.013     0.000   0.0% |
  calc_s_matrix:                     0.001     0.001   0.0% |
  inverse-cholesky:                  0.012     0.012   0.5% |
  projections:                       0.000     0.000   0.0% |
  rotate_psi_s:                      0.000     0.000   0.0% |
Set symmetry:                        0.001     0.001   0.0% |
Other:                               0.205     0.205   8.4% |--|
-----------------------------------------------------------
Total:                                         2.445 100.0%

Memory usage: 321.14 MiB
Date: Mon Oct  7 15:24:29 2024
