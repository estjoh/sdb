from ase import io, optimize
from ase.parallel import parprint

from gpaw import GPAW, FermiDirac
from gpaw.utilities.adjust_cell import adjust_cell

from sdb.gpaw import setups

fname = 'ini.xyz'
h = 0.2
q = 0
box = 4.

atoms = io.read(fname)
adjust_cell(atoms, box, h=h)

mm = [1 / len(atoms)] * len(atoms)
atoms.set_initial_magnetic_moments(mm)

atoms.calc = GPAW(
    mode='fd', xc='PBE', setups=setups(atoms),
    charge=q, h=h,
    #  mixer=MixerDif(beta=0.05, nmaxold=5, weight=50.0),
    occupations=FermiDirac(width=0.1, fixmagmom=True))

dyn = optimize.FIRE(atoms)
dyn.run(fmax=0.05)

E_PBE = atoms.calc.get_potential_energy()

for xc in ['PBE', 'TPSS', 'M06-L']:
    dE = atoms.calc.get_xc_difference(xc)
    parprint(xc, E_PBE + dE)
