from pathlib import Path

from sdb.xray.calculated import from_lines, data_filename
from sdb.molecules import find


def test_line_variants():
    """Test reading of all line variants"""
    lines_all_hash = [
        '# E(1s1ch)  Eref   E(gs) HOMO lumo',
        '48 284.1 -24858219.406 -24858503.571 -4.335 -4.335 # C(1s)'
    ]
    lines_all = [
        '# E(1s1ch)  Eref   E(gs) HOMO lumo',
        '48 284.1 -24858219.406 -24858503.571 -4.335 -4.335 C(1s)'
    ]
    lines_hash = [
        '# E(1s1ch)  Eref   E(gs)',
        '48 284.1 -24858219.406 # C(1s)'
    ]
    lines_minimal = [
        '# E(1s1ch)  Eref   E(gs)',
        '48 284.1 -24858219.406 C'
    ]

    data0 = from_lines(lines_minimal)

    for lines in [lines_all_hash, lines_all, lines_hash]:
        data = from_lines(lines)
        assert 'header' in data
        assert data0['C(1s)'][0][:3] == data['C(1s)'][0][:3]


def test_filename():
    mol = find(name='H2O')

    # default
    assert data_filename(mol.directory) == (
        mol.directory / '1ch' / '1ch_by_atom_PBE_h0.2_box4.dat')

    # all options
    assert data_filename(
        mol.directory, xc='LDA', h=0.5, box=2, spinpol=True,
        charge=-4, ext='_something'
    ) == (mol.directory / '1ch'
          / '1ch_q-4_by_atom_LDA_h0.5_box2_spin_something.dat')

    # filename for solids
    assert data_filename(
        '.', xc='PBE', box=None, ext='_k444', charge=-1,
        ) == Path('.') / '1ch' / '1ch_q-1_by_atom_PBE_h0.2_k444.dat'

    # non-self-consistent xc
    assert data_filename(mol.directory, xc='SCAN', nsc=True) == (
        mol.directory / '1ch' / '1ch_by_atom_SCAN_nsc_h0.2_box4.dat')
