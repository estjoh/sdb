import numpy as np
import pytest
from gpaw.utilities.folder import Folder

from sdb.xray.compare import compare_spectra, select_data, splined
from sdb.xray.compare import write_params, r_squared
from sdb.xray.folder import varing_fold


def test_two_peaks_exact():
    """Make sure folded and shifted spectra are recovered"""
    width = 0.4
    shift_exp = -0.3
    scale_exp = 10

    energies = np.array([285, 288])
    intensities = np.array([2, 1])

    for folding in ['Gauss', 'Lorentz']:
        e_exp, i_exp = Folder(width, folding).fold(
            energies + shift_exp, intensities, dx=0.1, xmin=280, xmax=293)
        i_exp /= i_exp.max() * scale_exp

        # Test minimize residu
        R2, params = compare_spectra(
            e_exp, i_exp, energies, intensities, folding=folding,
            init_params={'width': 3,
                         'scale': 1,
                         'shift': 0})

        assert R2 == pytest.approx(1)
        assert params['shift'] == pytest.approx(shift_exp)
        assert params['width'] == pytest.approx(width)
        assert params['scale'] == pytest.approx(scale_exp)

        # Test Maximize R squared
        R2, params = compare_spectra(
            e_exp, i_exp, energies, intensities,
            folding=folding,
            maximize_r_squerd=True,
            init_params={'width': 3,
                         'scale': 1,
                         'shift': 0})
        assert R2 == pytest.approx(1)
        assert params['shift'] == pytest.approx(shift_exp, rel=1e-5)
        assert params['width'] == pytest.approx(width, rel=1e-5)
        assert params['scale'] == pytest.approx(scale_exp, rel=1e-5)

        # Test with an alrady folded spectra e.i. comparing experiment
        # to experiment
        e_fold, i_fold = Folder(width, folding).fold(
            energies, intensities, dx=0.1, xmin=280, xmax=293)

        R2, params = compare_spectra(
            e_exp, i_exp, e_fold, i_fold, folded=True,
            init_params={'width': 3,
                         'scale': 1,
                         'shift': 0})

        assert R2 == pytest.approx(1)
        assert params['shift'] == pytest.approx(shift_exp)
        assert params['scale'] == pytest.approx(scale_exp)

        NRMSE, params = compare_spectra(
            e_exp, i_exp, energies, intensities,
            folding=folding, methode='NRMSE',
            init_params={'width': 3,
                         'scale': 1,
                         'shift': 0})

        assert NRMSE == pytest.approx(0, abs=1e-06)


def test_two_peaks_gauss_lorenz():
    width = 0.4
    shift_exp = -0.3
    scale_exp = 10

    energies = np.array([285, 288])
    intensities = np.array([2, 1])

    e_exp, i_exp = Folder(width, 'Lorentz').fold(
        energies + shift_exp, intensities, dx=0.1, xmin=280, xmax=295)
    i_exp *= scale_exp

    R2, params = compare_spectra(
        e_exp, i_exp, energies, intensities,
        init_params={'width': 3,
                     'scale': 1,
                     'shift': 0})

    R2_adj, params = compare_spectra(
        e_exp, i_exp, energies, intensities, methode='Adj. R2',
        init_params={'width': 3,
                     'scale': 1,
                     'shift': 0})

    assert R2 < 1 and R2 > 0

    assert R2_adj < R2 and R2_adj > 0


def test_select_data():
    width = 0.4
    shift_exp = -0.3
    scale_exp = 10

    emax = 290
    emin = 285
    energies = np.array([285, 288, 295])
    intensities = np.array([2, 1, 3])

    e_exp, i_exp = Folder(width, 'Lorentz').fold(
        energies + shift_exp, intensities, dx=0.1, xmin=280, xmax=300)
    i_exp *= scale_exp

    exp = select_data(e_exp, i_exp, e_min=emin, e_max=emax)

    assert len(exp[0]) == len(exp[1])
    assert exp[0].max() < emax
    assert exp[0].min() > emin

    i_max = np.where(e_exp == exp[0].max())[0]
    i_min = np.where(e_exp == exp[0].min())[0]

    assert e_exp[i_max+1] > emax
    assert e_exp[i_min-1] < emin


def test_splined():
    width = 0.4

    energies = np.array([285, 288])
    intensities = np.array([2, 1])

    e_exp, i_exp = Folder(width, 'Lorentz').fold(
        energies, intensities, dx=0.05, xmin=280, xmax=300)
    i_exp /= i_exp.max()

    e_fold, i_fold = Folder(width, 'Lorentz').fold(
        energies, intensities, dx=0.1, xmin=280, xmax=300)
    i_fold /= i_fold.max()
    fold = np.array([e_fold, i_fold])

    i_fold_new = splined(e_exp, fold)

    assert len(e_exp) == len(i_fold_new)
    assert (i_fold_new[::2] == i_fold).all()
    assert i_fold_new == pytest.approx(i_exp, 3e-4)

    # XXX remove later
    import matplotlib.pyplot as plt
    plt.plot(e_fold, i_fold)
    plt.plot(e_exp, i_fold_new, ':')
    # plt.show()


def test_varing_fold():
    width = 0.4
    shift_exp = -0.3
    scale_exp = 10
    linbroad = [0.8, 280, 300]
    energies = np.array([254, 285, 288, 293])
    intensities = np.array([0.3, 2, 1, 3])

    e_exp, i_exp = varing_fold(
        width, 'Lorentz', energies + shift_exp,
        intensities, linbroad=linbroad
        )

    i_exp /= i_exp.max() * scale_exp

    R2, params = compare_spectra(e_exp, i_exp, energies, intensities,
                                 folding='Lorentz',
                                 linbroad_params={'width2': 1.5,
                                                  'l1': 260,
                                                  'l2': 310},
                                 maximize_r_squerd=True,
                                 init_params={'width': 0.3,
                                              'scale': 1,
                                              'shift': 0})

    # XXX remove later
    import matplotlib.pyplot as plt
    x, y = varing_fold(
        params['width'], 'Lorentz', energies,
        intensities, linbroad=[params['width2'],
                               params['l1'],
                               params['l2']]
        )
    plt.plot(e_exp - params['shift'], i_exp*params['scale']*y.max())
    plt.plot(x, y, ':')
    # plt.show()

    assert R2 == pytest.approx(1)
    assert params['shift'] == pytest.approx(shift_exp)
    assert params['scale'] == pytest.approx(scale_exp, 5e-6)

    NRMSE, params = compare_spectra(e_exp, i_exp, energies, intensities,
                                    folding='Lorentz',
                                    linbroad_params={'width2': 1.5,
                                                     'l1': 260,
                                                     'l2': 310},
                                    methode='NRMSE', maximize_r_squerd=True,
                                    init_params={'width': 0.3,
                                                 'scale': 1,
                                                 'shift': 0})

    assert NRMSE == pytest.approx(0, abs=2.5e-06)
    assert params['shift'] == pytest.approx(shift_exp)
    assert params['scale'] == pytest.approx(scale_exp, 5e-6)


def test_update_defult():
    width = 0.4
    shift_exp = -0.3
    scale_exp = 10
    linbroad = [0.8, 280, 300]
    energies = np.array([254, 285, 288, 293])
    intensities = np.array([0.3, 2, 1, 3])

    e_exp, i_exp = varing_fold(
        width, 'Lorentz', energies + shift_exp,
        intensities, linbroad=linbroad
        )

    i_exp /= i_exp.max() * scale_exp

    _, params = compare_spectra(e_exp, i_exp, energies, intensities,
                                folding='Lorentz',
                                linbroad_params={'width2': 1.5,
                                                 'l1': 260,
                                                 'l2': 310},
                                maximize_r_squerd=True,
                                init_params={'width': 0.3,
                                             'scale': 1,
                                             'shift': 0},
                                linbroad_params_lim={'l1': {'max': 288}},
                                init_params_lim={'scale': {'min': 2}}
                                )

    assert params['l1'].max == 288
    assert params['l1'].min == 0  # defult

    assert params['l2'].max == np.inf  # defult
    assert params['l2'].min == (260 + 310) / 2  # defult

    assert params['scale'].min == 2
    assert params['scale'].max == np.inf  # defult


def test_save_params():

    width = 0.4
    shift_exp = -0.3
    scale_exp = 10

    energies = np.array([285, 288])
    intensities = np.array([2, 1])

    e_exp, i_exp = Folder(width, 'Lorentz').fold(
        energies + shift_exp, intensities, dx=0.1, xmin=280, xmax=295)
    i_exp *= scale_exp

    R2, params = compare_spectra(
        e_exp, i_exp, energies, intensities, folding='Lorentz',
        init_params={'width': 3,
                     'scale': 1,
                     'shift': 0})
    write_params('test', params)

    params_1 = np.load('test.npz')

    assert params_1['width'] == params['width'].value
    assert params_1['scale'] == params['scale'].value
    assert params_1['shift'] == params['shift'].value

    exp = (e_exp, i_exp)
    calc = (energies, intensities)

    R2_1 = r_squared(params_1, exp, calc, 'Lorentz')

    assert R2_1 == R2
