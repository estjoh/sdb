import pytest
from sdb import xray


def test_Si():
    Si2p32 = - xray.get_empirical_shift('Si', '2p3/2')
    Si2p = - xray.get_empirical_shift('Si', '2p')
    Si2p12 = - xray.get_empirical_shift('Si', '2p1/2')
    # shift of 2p1/2 < 2p < 2p3/2
    assert Si2p12 - Si2p == pytest.approx(
        4 / 6 * xray.data.so_splitting['Si']['2p'])
    assert Si2p - Si2p32 == pytest.approx(
        2 / 6 * xray.data.so_splitting['Si']['2p'])


def test_spin_polarized():
    C1s = xray.get_empirical_shift('C', '1s')
    C1s_spin = xray.get_empirical_shift('C', '1s', spinpol=True)
    assert C1s > C1s_spin
