import pytest

from sdb.molecules import find


@pytest.fixture
def s2():
    return find('S2')
