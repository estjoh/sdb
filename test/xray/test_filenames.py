from sdb.xray.gpaw import filenames


def test_filenames(s2):
    mol = s2

    datfname, outfname = filenames(
        mol.directory, 'LB94', 0.1, 3)
    assert (str(datfname)
            == str(mol.directory / '1ch' / '1ch_by_atom_LB94_h0.1_box3.dat'))
    assert (str(outfname) ==
            str(mol.directory / '1ch' / '1ch_LB94_h0.1_box3.out'))

    datfname, outfname = filenames(
        mol.directory, 'LB94', 0.1, 3, True)
    assert (str(datfname)
            == str(mol.directory / '1ch'
                   / '1ch_by_atom_LB94_h0.1_box3_spin.dat'))
    assert (str(outfname) ==
            str(mol.directory / '1ch' / '1ch_LB94_h0.1_box3_spin.out'))

    ext = 'freetext'
    datfname, outfname = filenames(
        mol.directory, 'LB94', 0.1, box=None, ext=ext)
    assert (str(datfname)
            == str(mol.directory / '1ch'
                   / f'1ch_by_atom_LB94_h0.1_{ext}.dat'))
    assert (str(outfname) ==
            str(mol.directory / '1ch' / f'1ch_LB94_h0.1_{ext}.out'))


def test_anion_filenames(s2):
    mol = s2

    datfname, outfname = filenames(
        mol.directory, xc='LB94', h=0.1, box=3, charge=-1)
    assert (str(datfname)
            == str(mol.directory / '1ch'
                   / '1ch_q-1_by_atom_LB94_h0.1_box3.dat'))
    assert (str(outfname) ==
            str(mol.directory / '1ch' / '1ch_q-1_LB94_h0.1_box3.out'))


def test_with_sz(s2):
    mol = s2

    datfname, outfname = filenames(
        mol.directory, 'PBE', 0.2, 4,
        charge=-1, spinpol=True, sz=1)
    assert datfname == (
        mol.directory / '1ch' / '1ch_q-1_by_atom_PBE_h0.2_box4_spin_Sz1.dat'
    )
    assert outfname == (
        mol.directory / '1ch' / '1ch_q-1_PBE_h0.2_box4_spin_Sz1.out'
    )
