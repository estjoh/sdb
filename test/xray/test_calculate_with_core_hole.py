from pathlib import Path
import numpy as np

from sdb.xray import state
from sdb.xray.gpaw import setups, calculate_and_write


def test_S2(s2):
    atoms = s2.atoms

    nS = len(state['S'])
    setup_list = list(setups(atoms))

    assert len(setup_list) == nS * len(atoms)


class FakeObj():
    pass


class Setups:
    def __init__(self, atoms, setups):
        self.id_a = [(None, 'paw', None)] * len(atoms)
        self.update(setups)

    def update(self, setups):
        if setups is None:
            setups = {}
        for ia, val in setups.items():
            self.id_a[ia] = (None, val, None)


class FakeGPAW():
    def __init__(self, charge=0, setups=None, txt='-'):
        self.energy = 0
        self.charge = charge
        self.wfs = FakeObj()
        self.wfs.setups = FakeObj()
        self.wfs.setups.Eref = 0
        self.parameters = FakeObj()
        if setups is None:
            setups = {}
        self.parameters.setups = setups

        # fake a gpaw-logger
        self.log = FakeObj()
        self.log.oldfd = txt

    def set(self, charge, setups):
        raise ValueError(
            'Please use new(...) instead of set(...)')

    def new(self, **kwargs):
        return FakeGPAW(**kwargs)

    def get_potential_energy(self, atoms):
        self.atoms = atoms
        self.setups = Setups(atoms, self.parameters.setups)

        energy = {'1s1ch': 100, '2s1ch': 10, '2p1ch': 1,
                  'paw': 0, 'special': -20}
        energy_list = np.array([energy.get(key, 0)
                                for key in self.parameters.setups])
        self.energy = energy_list.sum() + self.charge

        return self.energy

    def get_xc_difference(self, xc):
        return 0


def test_write_files(s2):
    atoms = s2.atoms

    datfname = Path('1ch') / 'xps.dat'
    atoms.calc = FakeGPAW()

    n = calculate_and_write(atoms, datfname, charge=-5,
                            nonselfconsistent_xc=FakeObj())
    assert Path(datfname).exists()
    assert n == 6  # 6 values processed

    # call again and nothing should be done
    n = calculate_and_write(atoms, datfname, charge=-5,
                            nonselfconsistent_xc=FakeObj())
    assert n == 0


def test_special_setup(s2):
    # make sure a special setup is kept
    atoms = s2.atoms

    setups = {0: 'other', 1: 'special'}
    atoms.calc = FakeGPAW(setups=setups)

    datfname = Path('1ch') / 'xps.dat'
    calculate_and_write(atoms, datfname)

    for ia, val in setups.items():
        assert atoms.calc.setups.id_a[ia][1] == val
