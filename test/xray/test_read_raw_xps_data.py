from sdb.molecules import Molecule
from sdb.xray.calculated import (
    data_filename, read_data, from_lines, to_lines, write_data)


def test_read_raw_data():
    molecule = Molecule('F4S')
    data = read_data(data_filename(molecule.directory))
    assert len(data['S(2p)']) == 1
    assert len(data['header']) == 2

    data2 = from_lines(to_lines(data))
    assert data == data2


def test_read_write_read():
    molecule = Molecule('F4S')
    data = read_data(data_filename(molecule.directory))

    filename = 'raw_data.dat'
    write_data(filename, data)
    data2 = read_data(filename)
    assert data == data2
