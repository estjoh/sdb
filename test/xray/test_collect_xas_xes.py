import pytest

from sdb.xray.xray import collect


def test_xas_S1s():
    entries = collect('xas', 'S(1s)')

    assert 'S2' == entries[0].name

    for entry in entries:
        assert len(entry.Eexp.values) == len(entry.references)

    entries_Sz1 = collect('xas', 'S(1s)', spinpol=True)
    assert entries[0].name == entries_Sz1[0].name
    # spin-unpol and spinpol should differ
    assert not (
        entries[0].Ecalc.mmp()[0] == pytest.approx(
            entries_Sz1[0].Ecalc.mmp()[0]))


def test_xes_H2O():
    entries = collect('xes', 'O(1s)')

    assert len(entries)
    assert entries[0].name == 'H2O'
