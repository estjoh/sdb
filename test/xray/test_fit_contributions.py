import numpy as np
import pytest
from gpaw.utilities.folder import Folder

from sdb.xray.contributions import contributions, folded_values


def test_triple_peak():
    width = 0.2

    es0 = [286, 287]
    fs0 = np.array([1, 0.5])
    w0 = 1

    es1 = [290]
    fs1 = np.array([0.3])
    w1 = 6

    es = np.concatenate((es0, es1)).flat
    fs = np.concatenate((w0 * fs0, w1 * fs1)).flat

    # the "experiment"
    folding = 'Lorentz'
    e_exp, i_exp = Folder(width, folding).fold(
        es, fs, dx=0.2, xmin=280, xmax=293)

    # the "theoretical" spectra
    energies = (es0, es1)
    oscillator_strengths = (fs0, fs1)

    R2, params = contributions(
        e_exp, i_exp, energies, oscillator_strengths,
        # use "wrong" initial parameters
        initial_params={'width': 2, 'w0': 4, 'w1': 1},
        folding=folding)

    # XXX remove later
    import matplotlib.pyplot as plt
    plt.plot(e_exp, i_exp, 'o-', label='experiment')
    e = np.linspace(e_exp.min(), e_exp.max(), 512)
    resid = e * 0
    for i, es in enumerate(energies):
        calc = [es, oscillator_strengths[i]]
        resid += params[f'w{i}'] * folded_values(e, calc, params, folding)
        plt.plot(e, params[f'w{i}'] * folded_values(
            e, calc, params, folding), label=f'contr. {i}')
    plt.plot(e, resid, '--', label='sum', lw=3, alpha=0.2)
    plt.legend()
    # plt.show()

    assert params['width'] == pytest.approx(width)
    assert params['w0'] == pytest.approx(w0)
    assert params['w1'] == pytest.approx(w1)
