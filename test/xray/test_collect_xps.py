import pytest

from sdb.xray.collect import collect, fit


def test_O():
    entries = collect('O(1s)')

    assert 'H2O' == entries[0].name
    assert [0] == entries[0].indices

    for entry in entries:
        assert len(entry.Eexp.values) == len(entry.references)

    entries = collect('O(1s)', xc='SCAN', nsc=True)
    assert len(entries) == 1


def test_F():
    entries = collect('F(1s)')
    assert len(entries) == 2
    assert entries[0].Eexp != entries[1].Eexp

    # the two entries should have distinct indices
    assert not (set(entries[0].indices) & set(entries[1].indices))

    for entry in entries:
        assert len(entry.Eexp.values) == len(entry.references)

    shift, error = fit(entries)
    assert shift == pytest.approx(8.387592094880528)
