import pytest

from sdb.xray.predict import intensities
from sdb.xray import data, get_empirical_shift


def test_C1s():
    energies = [284, 290]
    ene_corrected, intens = intensities('C(1s)', energies)
    assert len(ene_corrected) == len(energies)
    assert intens.sum() == pytest.approx(len(energies))
    for e0, e1 in zip(energies, ene_corrected):
        assert e0 - e1 == pytest.approx(
            get_empirical_shift('C', '1s'))


def test_S2p():
    """Test that spin-orbit splitting is applied"""
    ene, intens = intensities('S(2p)', [162])
    assert len(ene) == 2
    assert len(ene) == len(intens)
    assert intens.sum() == 1
    # 2p1/2 intensity is half of 2p3/2, but at higher energy
    assert 2 * intens[0] == pytest.approx(intens[1])
    assert ene[0] - ene[1] == pytest.approx(data.so_splitting['S']['2p'])


def test_xas_Si2p():
    energies_in = [104.37717154, 105.89243456, 105.8951057,
                   105.90090565, 107.68869963]
    intensities_in = [0.03043161, 0.00682793, 0.00682237,
                      0.00683013, 0.00082325]
    spinpol = True

    ene, intens = intensities('Si(2p)', energies_in, intensities_in,
                              spinpol=spinpol)
    assert len(ene) == 2 * len(energies_in)
    assert len(ene) == len(intens)

    assert 2 * intens[0] == pytest.approx(intens[1])
    assert ene[0] - ene[1] == pytest.approx(data.so_splitting['Si']['2p'])

    e0 = energies_in[0] - get_empirical_shift('Si', '2p', spinpol=spinpol)
    assert (ene[0] + 2 * ene[1]) / 3 == pytest.approx(e0)
    assert intens[0] + intens[1] == pytest.approx(intensities_in[0])


def test_xas_Sn3d():
    energies_in = [489.5]
    intensities_in = [1]

    ene, intens = intensities('Sn(3d)', energies_in, intensities_in)

    assert len(ene) == 2 * len(energies_in)
    assert len(ene) == len(intens)

    assert 3 * intens[0] == pytest.approx(2 * intens[1])
    assert ene[0] - ene[1] == pytest.approx(data.so_splitting['Sn']['3d'])

    e0 = energies_in[0] - get_empirical_shift('Sn', '3d')
    assert (2 * ene[0] + 3 * ene[1]) / 5 == pytest.approx(e0)
    assert intens[0] + intens[1] == pytest.approx(intensities_in[0])

    SO_calc = 5.85
    ene, intens = intensities('Sn(3d)', energies_in, intensities_in,
                              so_calc=SO_calc)
    assert len(ene) == 2 * len(energies_in)
    assert len(ene) == len(intens)
    assert ene[0] - ene[1] == pytest.approx(SO_calc)

    e0_so = energies_in[0] - get_empirical_shift('Sn', '3d', so_calc=SO_calc)
    assert (2 * ene[0] + 3 * ene[1]) / 5 == pytest.approx(e0_so)

    assert e0 != e0_so
