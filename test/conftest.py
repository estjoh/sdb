import os
from pathlib import Path
import pytest

from ase.utils import workdir

testdata = Path(__file__).parent.parent / 'test' / 'test_data'


@pytest.fixture
def test_data():
    return testdata


def pytest_configure(config):
    os.environ['STRUCTURE_DATABASE_DIR'] = str(testdata)


@pytest.fixture(autouse=True)
def use_tmp_workdir(tmp_path):
    # Pytest can on some systems provide a Path from pathlib2.  Normalize:
    path = Path(str(tmp_path))
    with workdir(path, mkdir=True):
        yield tmp_path
    # We print the path so user can see where test failed, if it failed.
    print(f'Testpath: {path}')
