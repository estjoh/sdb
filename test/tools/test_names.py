from sdb.naming import encode, decode, correct_hill


def test_smiles_encodings():
    """test that all problematic smiles characters are encoded"""
    smiles = r'=#:[]()/\@'
    encoded = encode(smiles)
    for letter in smiles:
        assert letter not in encoded

    assert decode(encoded) == smiles


def test_hill(tmp_path):
    """Test incorrect hill names get corrected"""
    orgname = tmp_path / 'C16H21N4CuI'
    correctname = tmp_path / 'C16H21CuIN4'
    orgname.mkdir()

    # do nothing
    assert not correct_hill(orgname)
    assert orgname.is_dir()

    # rename
    assert correct_hill(orgname, pretend=False)
    assert correctname.is_dir()
    assert not orgname.is_dir()
