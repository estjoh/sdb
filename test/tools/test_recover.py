import pytest
import shutil
from ase import io

from sdb.molecules.recover import recover_ini, renew_script
from sdb.molecules import find


@pytest.fixture
def script(tmp_path):
    name = tmp_path / 'q2' / 'GPAW_PBE' / 'relax.py'
    name.parent.mkdir(parents=True)
    return name


def test_only_newer(script):
    # this has length 0
    script.touch()

    renew_script(script)
    # should not be overwritten
    assert script.stat().st_size == 0


def test_charge_is_set(script):
    renew_script(script)
    assert script.exists()

    with open(script) as f:
        text = f.read()

    # charge should be set
    assert 'q = 2' in text


def test_recover_ini(tmp_path):
    mol = find(name='water')
    atoms = mol.atoms  # force to read atoms

    # prepare an environment without ini, but with output
    tmp_sdb = tmp_path / 'molecules' / 'H2O' / 'GPAW_PBE'
    tmp_sdb.mkdir(parents=True)
    shutil.copy(str(mol.filename), str(tmp_sdb))
    # create an empty (unreadable) ini.xyz
    ini = tmp_sdb / 'ini.xyz'
    ini.touch()

    # it has recovered
    assert recover_ini(tmp_sdb)
    assert (io.read(ini).get_positions()
            == pytest.approx(atoms.get_positions()))

    # it did not recover as ini.xyz exists
    assert not recover_ini(tmp_sdb)
