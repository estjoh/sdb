import yaml
from ase import Atoms, io
from sdb.molecules import find
from sdb.molecules.pubchem import from_cid, update, compound_names_from_cid
from sdb.molecules import prepare


class MockPubchemData:
    def __init__(self, atoms=None):
        self.atoms = atoms


class MockRequestsResponse:
    def __init__(self, json_dct=None):
        self.status_code = 200
        self.json_dct = json_dct

    def json(self):
        return self.json_dct


def patch(mocker, data=None, atoms=None):
    mpd = MockPubchemData(atoms)
    mpd.data = data
    mocker.patch(
        'sdb.molecules.pubchem.pubchem_search',
        return_value=mpd)
    patch_requests(mocker)


def patch_requests(mocker):
    dct = {
        'InformationList': {
            'Information':
                [{'Synonym': list('abcdefghijklmnop')}]}}
    mocker.patch(
        'sdb.molecules.pubchem.requests.get',
        return_value=MockRequestsResponse(dct))


def test_pubchem(test_data, mocker):
    """Check that PubchemCID is updated"""
    mol = find('F4S')

    pcid = 42
    patch(mocker, {'PUBCHEM_COMPOUND_CID': pcid})

    defname = 'new_definition.yml'
    assert update(mol, defname=defname)

    assert 'pubchemcid' in mol.definition
    with open(mol.base_directory / defname) as f:
        definition = yaml.load(f, Loader=yaml.SafeLoader)

    assert definition['pubchemcid'] == pcid
    (mol.base_directory / defname).unlink()


def test_names(mocker):
    """Retrieve the list of compound names for aspirin"""
    patch_requests(mocker)

    cid = 2244  # aspirin has many names
    assert len(compound_names_from_cid(cid)) > 10


def test_from_cid(test_data, mocker):
    patch(mocker, atoms=Atoms('OH6C2'))

    cid = 702
    atoms, definition = from_cid(cid)

    assert atoms.get_chemical_formula('hill') == 'C2H6O'


def test_update_no_smiles(test_data, mocker):
    """Test update when smiles is missing"""
    mol = find(name='water')

    patch(mocker, {'PUBCHEM_COMPOUND_CID': 0})

    defname = 'new_definition.yml'
    assert update(mol, defname=defname)
    for key in ['smiles', 'pubchemcid']:
        assert key in mol.definition

    (mol.base_directory / defname).unlink()


def test_update_tcnq_from_name(test_data, mocker):
    mol = find('C12H4N4')

    patch(mocker, {'PUBCHEM_COMPOUND_CID': 0})

    defname = 'new_definition.yml'
    assert update(mol, defname=defname)

    fname = mol.base_directory / defname
    assert fname.exists()
    fname.unlink()


def test_prepare_from_cid(tmpdir, mocker):
    patch(mocker, atoms=Atoms('OH6C2'))

    cid = 702  # ethanol
    atoms, definition = from_cid(cid)
    hill = atoms.get_chemical_formula(mode='hill')

    basedir = tmpdir / 'molecules'
    prepare(atoms, definition=definition, basedir=basedir)
    directory = basedir / hill / 'GPAW_PBE'
    assert (directory / 'relax.py').exists()
    assert io.read(str(directory / 'ini.xyz')) == atoms
    assert (basedir / hill / 'definition.yml').exists()
