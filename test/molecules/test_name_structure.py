from sdb.molecules.subdirs import charges, isomers


def test_isomers():
    assert len(isomers('H2O')) == 0
    assert len(isomers('F2LiO2P')) == 2
    assert len(isomers('notexisting')) == 0


def test_charges():
    assert len(charges('H2O')) == 2
    assert len(charges('C3H6')) == 0
    assert len(charges('C3H6/C1CC1')) == 1
