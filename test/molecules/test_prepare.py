import pytest
from pathlib import Path
import shutil

from ase import io
from ase.build import molecule
from sampling.translate import from_smiles, to_smiles

from sdb.molecules import prepare, find


@pytest.fixture
def basedir(tmpdir):
    return tmpdir / 'molecules'


def test_fresh(basedir):
    """Prepare a noexisting hill"""

    atoms = molecule('cyclobutene')

    prepare(atoms, basedir=basedir)
    directory = basedir / 'C4H6' / 'GPAW_PBE'
    assert (directory / 'relax.py').exists()
    assert io.read(str(directory / 'ini.xyz')) == atoms


def test_subdir(basedir):
    name = 'cyclobutene'
    atoms = molecule(name)
    smiles = to_smiles(atoms)

    prepare(atoms, subdir=smiles, basedir=basedir,
            definition={'name': name})
    directory = basedir / 'C4H6' / smiles / 'GPAW_PBE'
    assert (directory / 'relax.py').exists()
    assert io.read(str(directory / 'ini.xyz')) == atoms
    assert (basedir / 'C4H6' / smiles / 'definition.yml').exists()


def test_charged_sz(basedir):
    """Test charged species"""
    name = 'cyclobutene'
    atoms = molecule(name)

    charge = 1
    sz = 1
    prepare(atoms, charge=charge, sz=sz, basedir=basedir)

    hill = atoms.get_chemical_formula('hill')
    directory = basedir / hill / f'q{charge}' / 'GPAW_PBE'

    assert (directory / f'relax_Sz{sz}.py').exists()
    assert f'q = {charge}' in open(directory / f'relax_Sz{sz}.py').read()


def test_new_isomer(basedir):
    # prepare the environment of an existing isomer
    name = 'C8H14O'
    mol = find(name)
    shutil.copytree(mol.directory.parent, basedir / name)

    atoms = from_smiles('C[C@@H]1[C@H](C)[C@H]2COC[C@@H]12')
    prepare(atoms, basedir=basedir)
    # ensure files are moved
    assert not (basedir / name / 'GPAW_PBE').exists()
    assert not (basedir / name / 'q1').exists()
    assert not list(Path(basedir / name).glob('*.yml'))

    atoms = from_smiles('CCC1CCC(=O)CC1')
    assert atoms.get_chemical_formula('hill') == name
    prepare(atoms, basedir=basedir)
    # ensure not added at baselevel
    assert not (basedir / name / 'GPAW_PBE').exists()
