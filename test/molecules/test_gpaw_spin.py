import pytest
from ase import Atoms, io
from gpaw.utilities.adjust_cell import adjust_cell
from gpaw import FermiDirac, Mixer

from sdb.molecules.gpaw_spin import Ovchinnikov


def test_r2():
    """single and triplet difference at R=2"""
    atoms = Atoms('H2', positions=((0, 0, 0), (2, 0, 0)))
    h = 0.25
    adjust_cell(atoms, 2, h=h)

    mm_singlet = [-1, 1]
    mm_triplet = [1, 1]
    gpaw_kwargs = {'mode': 'fd', 'xc': 'PBE',
                   'occupations': FermiDirac(0.2, fixmagmom=True),
                   'mixer': Mixer(0.02, 5, 100),
                   'h': h,
                   'convergence': {'density': 1, 'eigenstates': 1}
                   }

    atoms.calc = Ovchinnikov(mm_singlet, mm_triplet, **gpaw_kwargs)
    atoms.get_potential_energy()

    assert atoms.calc.get_potential_energy() \
           < atoms.calc.Sz0.get_potential_energy()
    assert atoms.calc.Sz0.get_potential_energy() \
           < atoms.calc.Sz2.get_potential_energy()

    # check io
    atoms.get_forces()
    fname = 'out.traj'
    with io.Trajectory(fname, 'w') as traj:
        traj.write(atoms)
    btoms = io.read(fname)
    assert btoms.get_potential_energy() == \
        pytest.approx(atoms.get_potential_energy())
    assert btoms.get_forces() == \
        pytest.approx(atoms.get_forces())
