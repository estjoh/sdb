import json
from sdb.search.build import update


def test_water(test_data, tmp_path):
    # build tables of names
    update(datadir=test_data, tabledir=tmp_path)

    with open(tmp_path / 'names.json') as f:
        names = json.loads(f.read())
    # ensure lower case water is there
    assert 'water' in names
    # ensure no lower case h2O or h2o is added
    assert 'h2O' not in names
    assert 'h2o' not in names
    # ensure subdirs are there
    assert 'H2O/q1' not in names


def test_conformers(test_data, tmp_path):
    """Check TCAQ conformers"""
    # build tables of names
    update(select='N', datadir=test_data, tabledir=tmp_path)

    with open(tmp_path / 'names.json') as f:
        names = json.loads(f.read())

    # XXX is this sufficient?
    assert 'C20H8N4/conformer_butterfly' in names
    assert 'C20H8N4/conformer_flat' in names
    assert 'TCAQ conformer_butterfly' in names
    assert 'TCAQ conformer_flat' in names


def test_conformer_decoding(test_data, tmp_path):
    # build tables of names
    update(datadir=test_data, tabledir=tmp_path)
    with open(tmp_path / 'names.json') as f:
        names = json.loads(f.read())
    for name in names:
        assert '%' not in name
