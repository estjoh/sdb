import pytest
import requests

from sdb.utilities.references import Reference


def test_doi(tmp_path, monkeypatch):
    doi = '10.1103/PhysRevB.92.115311'
    ref_exists = Reference(doi=doi)
    assert doi.lower() == ref_exists.get_id()

    def mock_get(url, **kwargs):
        class CorrectRequest:
            text = ('@article{Remeika_2015,\n\tdoi = '
                    '{10.1103/physrevb.92.115311},\n\turl = '
                    '{https://doi.org/10.1103%2Fphysrevb.92.115311},'
                    '\n\tyear = 2015,\n\tmonth = {sep},\n\tpublisher = '
                    '{American Physical Society ({APS})},\n\tvolume = '
                    '{92},\n\tnumber = {11},\n\tauthor = '
                    '{M. Remeika and J. R. Leonard and C. J. Dorow and '
                    'M. M. Fogler and L. V. Butov and M. Hanson and '
                    'A. C. Gossard},\n\ttitle = '
                    '{Measurement of exciton correlations using '
                    'electrostatic lattices},\n\tjournal = '
                    '{Physical Review B}\n}')
        return CorrectRequest
    monkeypatch.setattr(requests, 'get', mock_get)

    (tmp_path / 'tables').mkdir()
    ref_unknown = Reference(doi=doi, datadir=tmp_path)
    assert ref_exists.data['author'] == ref_unknown.data['author']


def test_text():
    ref = Reference('Taylor J.A., Rabalais J.W.; J. Chem. Phys. (1981) 1735')
    print('My ID:', ref.get_id())
    print(ref.bibtex())


def test_invalid(monkeypatch):
    def mock_get(url, **kwargs):
        class NotHaveRequest:
            text = '<h2>DOI Not Found</h2>'
        return NotHaveRequest
    monkeypatch.setattr(requests, 'get', mock_get)

    """Test inavlid arguments"""
    for invalid in [{}, {'string': 'Taylor'}, {'doi': 'invalid'}]:
        with pytest.raises(RuntimeError):
            Reference(**invalid)
