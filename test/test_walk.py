from sdb.molecules import walk


def test_len():
    assert len(walk()) >= 2
    assert 'C3H6/C%5BC%5DC/q1' in walk('C', include_charges=True)
    assert len(walk('O')) == 4
    assert len(walk('O', include_charges=True)) == 7
