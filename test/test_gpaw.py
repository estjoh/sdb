from pathlib import Path
from ase import Atoms

from sdb.gpaw import setups, output_ok
from sdb.molecules import find


def test_no_special():
    atoms = Atoms('H')
    assert len(setups(atoms)) == 0
    assert len(setups()) == 0


def test_Cr():
    """Cr needs special setup"""
    atoms = Atoms('Cr')
    assert setups(atoms) == {'Cr': '14_1.55'}


def test_Fe_U():
    """Iron with U"""
    atoms = Atoms('Fe')
    assert setups(atoms, apply_U=True) == {'Fe': ':d,4.0'}


def test_output_not_readable():
    assert not output_ok(Path('nonexisting'))


def test_output_too_old():
    mol = find(name='S8')
    mol.atoms
    assert output_ok(mol.filename, min_gpaw_version=0)
    assert not output_ok(mol.filename)
