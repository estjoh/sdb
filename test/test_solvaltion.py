import numpy as np
from ase import Atoms
import gpaw.solvation as solv

from sdb.solvation import kwargs, get_data, permittivity_U


def test_water():
    HW14_params = solv.get_HW14_water_kwargs()
    params = kwargs('water', alvarez=False)
    assert params['dielectric'].epsinf == HW14_params['dielectric'].epsinf

    # check that standard radii still are the same
    atoms = Atoms('CHNOF')
    assert (params['cavity'].effective_potential.atomic_radii(atoms)
            == HW14_params['cavity'].effective_potential.atomic_radii(atoms))

    atoms = Atoms('Fe')  # nan in atoms.data.vdw
    assert not np.isnan(
        params['cavity'].effective_potential.atomic_radii(atoms)[0])


def test_other_solvents():
    for solvent in ['acetonitrile', 'DMSO']:
        params = kwargs(solvent)
        permittivity, _ = permittivity_U(solvent)
        assert params['dielectric'].epsinf == permittivity


def test_solvent_data():
    for name in ['acetonitrile', 'DMSO', 'DOL']:
        data = get_data(name)
        for key in ['eps', 'density [g/cm3]']:
            assert key in data
