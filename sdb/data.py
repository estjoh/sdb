from dataclasses import dataclass
from typing import List
import numpy as np


class EnergyRange():
    def __init__(self, values):
        self.values = np.array(values)

    def mmp(self):
        """return mean, dist to min, dist to max"""
        mean = self.values.mean()
        dmn = mean - self.values.min()
        dmx = self.values.max() - mean
        return mean, dmn, dmx

    @property
    def mean(self):
        return self.mmp()[0]

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        if len(self.values) > 1:
            mean, dmn, dmx = self.mmp()
            return f'{mean:.2f}-{dmn:.2f}+{dmx:.2f}'
        else:
            return f'{self.values[0]:.2f}'

    def __eq__(self, other):
        mean, dmn, dmx = self.mmp()
        omean, odmn, odmx = other.mmp()
        return mean == omean and dmn == odmn and dmx == odmx


@dataclass
class ExpCalc:
    Eexp: EnergyRange
    Ecalc: EnergyRange
    name: str
    references: List
    indices: List
