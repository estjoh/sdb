from ase import Atoms, io, optimize

from gpaw import GPAW, FermiDirac, MixerSum
from gpaw.utilities.adjust_cell import adjust_cell

from sdb.gpaw import setups

fname = 'ini.xyz'
h = 0.2
q = 0
box = 4.

atoms: Atoms = io.read(fname)
atoms.pbc = False
adjust_cell(atoms, box, h=h)

filename = 'relax_Sz_U.traj'
try:
    with io.Trajectory(filename) as traj:
        n_finished: int = len(traj)
except FileNotFoundError:
    n_finished: int = 0

with io.Trajectory(filename, 'a') as traj:
    for Sz in [0, 2, 4, 6][n_finished:]:
        mm = [Sz / len(atoms)] * len(atoms)
        atoms.set_initial_magnetic_moments(mm)

        atoms.calc = GPAW(
            mode='fd', xc='PBE', setups=setups(atoms, apply_U=True),
            charge=q, h=h,
            mixer=MixerSum(beta=0.05, nmaxold=5, weight=50.0),
            occupations=FermiDirac(width=0.1, fixmagmom=True))

        dyn = optimize.FIRE(atoms)
        dyn.run(fmax=0.05)
        traj.write(atoms)
