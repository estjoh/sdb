from ase import io, optimize

from gpaw import FermiDirac
from gpaw.utilities.adjust_cell import adjust_cell
import gpaw.solvation as solv

from sdb.gpaw import setups
from sdb.solvation import kwargs

fname = 'ini.xyz'
h = 0.2
q = 0
box = 4.

atoms = io.read(fname)
atoms.pbc = False
adjust_cell(atoms, box, h=h)

kwargs = kwargs('DME/DOL')
atoms.calc = solv.SolvationGPAW(
    mode='fd', xc='PBE', setups=setups(atoms),
    h=h, charge=q,
    occupations=FermiDirac(width=0.1), **kwargs)

dyn = optimize.FIRE(atoms)
dyn.run(fmax=0.05)
