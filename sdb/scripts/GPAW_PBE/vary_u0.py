import numpy as np
from glob import glob
from ase import io
from ase.data.vdw import vdw_radii
from ase.parallel import parprint
from ase.units import Pascal, m, Bohr
from gpaw.solvation import (
    SolvationGPAW,  # the solvation calculator
    EffectivePotentialCavity,  # cavity using an effective potential
    Power12Potential,  # a specific effective potential
    # rule to construct permittivity function from the cavity
    LinearDielectric,
    # rule to calculate the surface area from the cavity
    GradientSurface,
    SurfaceInteraction  # rule to calculate non-electrostatic interactions
)
import gpaw.solvation as solv

atoms = io.read(glob('relax.py.o*')[0])

# solvent parameters for water from J. Chem. Phys. 141, 174108 (2014)
u0 = 0.180  # eV
epsinf = 78.36  # dimensionless
# convert from dyne / cm to eV / Angstrom ** 2
gamma = 18.4 * 1e-3 * Pascal * m
T = 298.15  # Kelvin
kappa_T = 4.53e-10 / Pascal
vdw_radii = vdw_radii.copy()
vdw_radii[1] = 1.09

atomic_radii = lambda atoms: [vdw_radii[n] for n in atoms.numbers]  # noqa

h = 0.4

parprint('# u0[eV]  V[A^3]')
for u0 in np.arange(0.1, 0.36, 0.01):
    atoms.calc = SolvationGPAW(
        mode='fd', xc='PBE', h=h,
        cavity=EffectivePotentialCavity(
            effective_potential=Power12Potential(atomic_radii, u0),
            temperature=T,
            surface_calculator=GradientSurface(),
            volume_calculator=solv.KB51Volume(
                compressibility=kappa_T,
                temperature=T)
        ),
        dielectric=LinearDielectric(epsinf=epsinf),
        interactions=[SurfaceInteraction(surface_tension=gamma)],
        convergence={'energy': 1e10, 'density': 1e10, 'eigenstates': 1e10},
        txt=None,
    )
    atoms.get_potential_energy()

    cavity = atoms.calc.stuff_for_hamiltonian[0]
    g_g = cavity.g_g
    V = cavity.V * Bohr ** 3
    parprint(u0, V)
