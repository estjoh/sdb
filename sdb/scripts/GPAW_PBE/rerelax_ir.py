from pathlib import Path

from ase import io, optimize
from ase.vibrations.infrared import Infrared

from gpaw import GPAW, FermiDirac
from gpaw.utilities.adjust_cell import adjust_cell

fname = next(Path('.').glob('relax.py.out*'))
h = 0.2
q = 0
box = 4.
fmax = 0.01

atoms = io.read(fname)
atoms.pbc = False
adjust_cell(atoms, box, h=h)
atoms.set_initial_magnetic_moments([0] * len(atoms))

atoms.calc = GPAW(
    mode='fd', xc='PBE', symmetry='off',
    charge=q, spinpol=False, h=h,
    # mixer=MixerDif(beta=0.05, nmaxold=5, weight=50.0),
    occupations=FermiDirac(width=0.1))

dyn = optimize.FIRE(atoms)
dyn.run(fmax=fmax)
atoms.write(f'rerelaxed_fmax{fmax}.traj')

ir = Infrared(atoms, name=f'ir_fmax{fmax}')
ir.run()
ir.summary()
