from ase import io
from ase.optimize import FIRE
from mace.calculators import mace_off


atoms = io.read('ini.xyz')
atoms.calc = mace_off(model="medium")

opt = FIRE(atoms)
opt.run(fmax=0.05)

atoms.write('relaxed.traj')
