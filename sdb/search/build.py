from pathlib import Path
import json
import sys
import yaml
import six
from typing import Dict, Tuple

import sdb
from ..utilities import lc_dict
from ..molecules import walk
from ..naming import decode


def find_definition(datadir: Path, name: str) -> Tuple[Dict, str]:
    """Find definition.yml by searching also in parent directories"""
    path = Path(name)
    name_extension = ''
    while (path != Path('.')):
        try:
            return yaml.load(
                open(datadir / path / 'definition.yml'),
                Loader=yaml.SafeLoader,), name_extension
        except IOError:
            name_extension += f' {path.name}'
            path = path.parent
    # nothing found
    raise IOError


def update(select=None,
           datadir=None,
           tabledir=None,
           verbose=True,) -> None:
    """Build tables for names and ids"""
    nl = {}
    idl = {}

    if datadir is None:
        datadir = sdb.datadir

    if tabledir is None:
        tabledir = Path(datadir) / 'tables'

    # add to name list
    def add2nl(k: str, name: str, lc_also: bool = False):
        """add key and value to namelist

        lower case key is also added if lc_also is True
        """
        if lc_also:
            add2nl(k.lower(), name)
        if k in nl and nl[k] != name:
            try:
                nl[k].append(name)
            except AttributeError:
                nl[k] = [nl[k], name]
        nl[k] = name

    datadir = Path(datadir) / 'molecules'
    for name in walk(select, datadir=datadir):
        add2nl(decode(name), name)
        if verbose:
            sys.stdout.write("\033[K")
            print(name + "\r", end="")
        sys.stdout.flush()

        try:
            data, extension = find_definition(datadir, name)
            definition = lc_dict(data)

            for key in ["formula", "latexname", "name", "shortname"]:
                if key in definition:
                    val = definition[key]
                    if not isinstance(val, six.string_types) and hasattr(
                        val, "__iter__"
                    ):
                        # this is a list
                        for item in val:
                            add2nl(item + extension, name, True)
                    else:
                        add2nl(val + extension, name, True)

            # add to id list
            for key in definition:
                if key.lower().endswith("id"):
                    val = definition[key]
                    if val in idl:
                        try:
                            idl[val].append(name)
                        except AttributeError:
                            idl[val] = [idl[val], name]
                    else:
                        idl[val] = name
        except IOError:
            pass

    tabledir = Path(tabledir)

    def write_json(data, name, indent=None):
        fname = tabledir / f"{name}.json"
        with open(fname, "w") as f:
            json.dump(data, f, indent=indent)
        if verbose:
            print(f"{str(fname)} updated")

    write_json(nl, 'names', indent=4)
    write_json(idl, 'ids', indent=4)


if __name__ == "__main__":
    update()
