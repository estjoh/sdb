import os
from pathlib import Path
import yaml
import numpy as np

import sdb
from sdb.utilities import lc_dict
from .xray import xray_maps


def _get_vstate(element=None, state=None, vstate=None):
    if vstate is not None:
        return vstate
    return f"{element}({state})"


class Experiment:
    """Find experimental data for a given molecule."""

    def __init__(self, name=None, dirname=None, basedir=None):
        """Loads data acoording to molecule name"""
        if basedir is None:
            basedir = sdb.datadir
        self.basedir = basedir
        self.name = name
        self._set_path(name, dirname)

        try:
            fname = self.path / 'experimental.yml'
            alldata = lc_dict(yaml.load(
                    open(fname, "r"),
                    Loader=yaml.SafeLoader,))
            self.data = alldata
        except Exception as e:
            # print(f'# broken {fname}')
            raise e

        self.xray = {}
        for shortkey, longkey in xray_maps.items():
            data = alldata.get(longkey.lower(), [])
            if len(data):
                self.xray[shortkey] = data
        self.xps = alldata.get("CoreElectronBindingEnergy".lower(), [])

        try:
            self.definition = lc_dict(
                yaml.load(
                    open(os.path.join(self.path, "definition.yml"), "r"),
                    Loader=yaml.SafeLoader,
                )
            )
        except IOError:
            self.definition = None

    def xps_by_element(self, element=None, state=None,
                       vstate=None, references=False):
        """
        Return a list of experimental values if known.

        Parameters
        ----------
        element: string
            Element name, example: 'S'
        state: string
            Example '2p3/2'
        vstate: string
            Combined element and state: '{0}({1})'.format(element, state)
        references:
            return references also
        """
        vstate = _get_vstate(element, state, vstate)
        if not isinstance(self.xps, list):
            self.xps = [self.xps]

        vals = []
        refs = []
        try:
            for ref in self.xps:
                if vstate in ref:
                    value = ref[vstate]
                    assert not isinstance(value, list)
                    vals.append(value)
                    try:
                        refs.append(ref["reference"])
                    except KeyError:
                        print(self.basedir, self.name)
                        refs.append(ref["DOI"])
            if references:
                return np.array(vals), refs
            else:
                return np.array(vals)
        except AssertionError:
            raise TypeError("Different chemical environments")

    def xps_by_index(self, index, element=None, state=None,
                     vstate=None, references=False):
        """
        index: int
        element: string
        state:
        vstate:
        """
        # collect values
        vstate = _get_vstate(element, state, vstate)
        assert isinstance(self.xps, list)

        vals = []
        refs = []
        for ref in self.xps:
            if vstate in ref:
                data = ref[vstate]
                for entry in data:
                    entry = lc_dict(entry)
                    if not isinstance(entry["atom"], list):
                        entry["atom"] = [entry["atom"]]
                    if index in entry["atom"]:
                        vals.append(entry["value"])
                        refs.append(ref["reference"])
        if not len(vals):
            raise ValueError(
                "No data for element "
                + element
                + " and index "
                + str(index)
                + " in "
                + self.name
            )
        if references:
            return np.array(vals), refs
        else:
            return np.array(vals)

    def __repr__(self):
        return "{0}: {1}".format(self.__class__.__name__, self.name)


class Molecule(Experiment):
    """Experimental info for molecules"""
    def _set_path(self, molecule=None, dirname=None):
        if molecule is None:
            if dirname is None:
                raise RuntimeError("Either the name or dirname must be given.")
            else:
                self.path = Path(self.basedir) / dirname
        else:
            self.path = Path(self.basedir) / f"molecules/{molecule}"


class Solid(Experiment):
    """Experimental information for solids"""
    def _set_path(self, name=None, dirname=None):
        if name is None:
            if dirname is None:
                raise RuntimeError("Either the name or dirname must be given.")
            else:
                self.path = Path(self.basedir) / dirname
                self.name = dirname
        else:
            self.path = Path(self.basedir) / f"solids/{name}"
