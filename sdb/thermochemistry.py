from typing import Optional

from sampling.thermochemistry import (
    default_T_K, default_pressure, thermochemistry)


def molecule_thermo(molecule, correction: Optional[str],
                    T_K: float = default_T_K,
                    pressure: float = default_pressure,
                    tiny: float = 1e-12,) -> float:
    return thermochemistry(
        molecule.atoms, molecule.vibrations(), correction,
        T_K, pressure, tiny)
