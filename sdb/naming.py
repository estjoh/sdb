from urllib.parse import quote, unquote
from pathlib import Path
import os

from ase import Atoms

from .utilities.latex import decorate_formula
from .experiment import Molecule as Experiment


def encode(name: str) -> str:
    """encode smiles string to be used as directory name"""
    return quote(name, safe=',+')


def decode(encoded: str) -> str:
    """decode directory name to recover smiles string"""
    return unquote(encoded)


def best_name(name):
    try:
        exp = Experiment(name)
        if exp.definition is not None:
            if 'shortlatexname' in exp.definition:
                return exp.definition['shortlatexname']
            if 'latexname' in exp.definition:
                return exp.definition['latexname']
            return exp.definition['name']
        return decorate_formula(name)
    except FileNotFoundError:
        return name


def correct_hill(dirname: os.PathLike, pretend: bool = True) -> bool:
    """Correct name to Hill notation"""
    dirname = Path(dirname)
    assert dirname.is_dir(), f'not a directory {dirname}'

    name = str(dirname.name)
    atoms = Atoms(name)
    hill = atoms.get_chemical_formula(mode='hill')
    if name == hill:
        return False

    new = dirname.parent / hill
    print('renaming', dirname, new)

    if pretend:
        return False

    dirname.rename(new)
    return True
