from pathlib import Path
from typing import Optional, Dict
from ase import Atoms, io

special_setups = {
    'Cr': {'Cr': '14_1.55'},
}
special_setups_U = {
    'Fe': {'Fe': ':d,4.0'},
}


def setups(atoms: Optional[Atoms] = None, apply_U: bool = False) -> Dict:
    """Return special setups"""
    if atoms is None:
        return {}

    setup_dct = {}
    for atom in atoms:
        setup_dct.update(special_setups.get(atom.symbol, {}))
        if apply_U:
            setup_dct.update(special_setups_U.get(atom.symbol, {}))

    return setup_dct


def output_ok(fname: Path, min_gpaw_version: int = 24):
    if not fname.exists():
        return False
    try:
        atoms = io.read(fname)
    except Exception:
        return False

    try:
        atoms.get_potential_energy()
    except Exception:
        return False

    # check for version
    version_ok = False
    try:
        with open(fname) as f:
            for line in f:
                res = line.split(r'|__ |  _|___|_____|  ')
                if len(res) > 1:
                    version = res[-1]
                    version.strip()
                    if (int(version.split('.')[0])
                            >= min_gpaw_version):
                        version_ok = True
                    else:
                        pass
                    break
    except UnicodeDecodeError:
        pass

    return version_ok
