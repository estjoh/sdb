import re
import os
import yaml
from pathlib import Path
from typing import Callable, Optional, List

from ase import Atoms, io
from ase.optimize import FIRE
from ase.vibrations import Vibrations

import sdb
from ..utilities import lc_dict
from ..definitions import default_code
from .definitions import basedir
from .subdirs import isomers, charges


def possible_output_names(Sz: Optional[float] = None,
                          code: str = default_code) -> List[str]:
    assert code == default_code
    if Sz:
        return [f"relax_Sz{Sz}.py.out*"]
    return ["relax.py.out*", "relax.traj", "gpaw.traj",
            "relaxed.traj", "job_*.out", ]


def elements(name):
    """Return hash with elements and number"""
    elems = {}

    def add(element):
        lst = [a for a in re.split(r"([A-Z][a-z]*)", element) if a]
        if lst[0] not in elems:
            elems[lst[0]] = 0
        try:
            elems[lst[0]] += int(lst[1])
        except IndexError:
            elems[lst[0]] += 1

    elems = {}
    for a in re.split(r"([A-Z][a-z]*[\d]*)", name):
        if a:
            add(a)

    return elems


def hill(name: str) -> str:
    return Atoms(name).get_chemical_formula('hill')


def read_definition(
        directory: Path, filename: str = 'definition.yml') -> Optional[dict]:
    """Read definition .yml"""
    fpath = Path(directory) / filename
    if not fpath.exists():
        return None

    data = yaml.load(open(fpath, "r"), Loader=yaml.SafeLoader)
    data = lc_dict(data)
    if 'name' in data:
        if type(data['name']) is not type([]):
            data['name'] = [data['name']]
    return data


class Molecule:
    """Molecule with all info."""

    def __init__(self, name, path=None, code=default_code,
                 Sz: float = 0):
        self.hill = hill(name.split(os.sep)[0])
        self.code = code
        self.name = name
        self.Sz = Sz
        if path is None:
            path = sdb.datadir / "molecules"
        else:
            path = Path(path)

        self.base_directory = path / name
        if not os.path.exists(self.base_directory):
            raise RuntimeError("No molecule " + name)

        # read definition info
        self.definition = read_definition(self.base_directory)

        self.directory = Path(self.base_directory) / code
        if not self.directory.exists():
            raise RuntimeError("No " + code + " info for " + self.hill)

        self._atoms = None

    @property
    def atoms(self):
        if self._atoms is None:
            for tname in possible_output_names(self.Sz):
                fname = list(self.directory.glob(tname))
                if len(fname) > 1:
                    msg = f"More than one relaxation file found \
                        in {self.directory}:"
                    for name in fname:
                        msg += f' {name}'
                    raise RuntimeError(msg)
                try:
                    self.filename = fname[0]
                    self._atoms = io.read(fname[0])
                    break
                except (IOError, IndexError, io.formats.UnknownFileTypeError):
                    pass
            if self._atoms is None:
                raise RuntimeError("No " + self.code + " calculation for "
                                   + self.hill)
        return self._atoms

    @atoms.setter
    def atoms(self, value):
        if isinstance(value, Atoms):
            self._atoms = value
        else:
            self.filename = value
            self._atoms = io.read(value)

    def relax(self, fmax: float = 0.05,
              initialize: Optional[Callable] = None,
              relaxed_traj: Optional[str] = 'relaxed.traj',
              optimizer=FIRE) -> None:
        if self.relaxed(fmax):
            return

        if initialize is not None:
            self.atoms = initialize(self.atoms)
        opt = optimizer(self.atoms)
        opt.run(fmax=fmax)

        if relaxed_traj is not None:
            filename = self.directory / relaxed_traj
            with io.Trajectory(filename, 'w') as traj:
                traj.write(self.atoms)
            self.filename = filename

    def relaxed(self, fmax: float = 0.05) -> bool:
        try:
            myfmax2 = (self.atoms.get_forces()**2).sum(axis=1).max()
            return myfmax2 < fmax**2
        except RuntimeError:  # probably no calculator
            return False

    def vibrations(self,
                   initialize: Optional[Callable] = None
                   ) -> Vibrations:
        if initialize is not None:
            self.atoms = initialize(self.atoms)
        else:
            self.atoms  # XXX implictely read, a bit strange

        filename = Path(self.filename)
        name = filename.parent / f'vib_{filename.name}'
        vib = Vibrations(self.atoms, name=name)
        vib.run()

        return vib

    def __repr__(self):
        return " ".join([self.__class__.__name__ + ":",
                         str(self.directory)])

    def __str__(self):
        return self.__repr__()

    @classmethod
    def create(cls, name, path=basedir, code=default_code):
        """Create Molecule even if it does not exist"""
        directory = Path(path) / hill(name) / code
        directory.mkdir(parents=True, exist_ok=True)

        return cls(name, path=path, code=code)


def walk(select: object = None,
         include_charges: bool = False,
         datadir: Path = basedir) -> List:
    """Return the list of all available molecules (including isomers).

    Examples:
    for name in walk():
    ...
    for name in walk('O'):  # select all molecules containing oxygen
    ...
    """

    def select_is_in(name, select):
        if not select:
            return True
        if select in elements(name):
            return True
        return False

    compositions = [
        d
        for d in os.listdir(datadir)
        if os.path.isdir(os.path.join(datadir, d)) and select_is_in(d, select)
    ]

    dirs = []
    for composition in compositions:
        mydirs = isomer_directories(composition, datadir)
        chargedirs = []
        if include_charges:
            for name in mydirs:
                chargedirs += [str(Path(name) / q)
                               for q in charges(name, datadir)]
        dirs += mydirs + chargedirs
    return dirs


def isomer_directories(
        hill: str, datadir: Path = Path(basedir)) -> List[str]:
    """List directories belonging to the given composition"""
    basedir = Path(hill)
    isomer_list = isomers(hill, datadir)
    if not len(isomer_list):
        return [hill]
    return [str(basedir / isomer) for isomer in isomer_list]
