from pathlib import Path
import shutil
from ase import io
from ase.io.formats import UnknownFileTypeError
from gpaw.utilities.hardware import ComputeCluster

import sdb
from sdb.molecules.molecules import possible_output_names


def script_path(code: str) -> Path:
    return Path(sdb.__path__[0]) / 'scripts' / code


def recover_ini(direc: Path, source=None, verbose=True) \
        -> bool:
    """recover ini.xyz from other files"""
    ini = direc / 'ini.xyz'

    if ini.exists():
        try:
            io.read(ini)
            return False
        except UnknownFileTypeError:
            pass

    sources = possible_output_names()
    if source is not None:
        sources.insert(0, source)

    for name in sources:
        for fname in Path(direc).glob(name):
            try:
                atoms = io.read(fname)
                print(f'# recovering ini.xyz from {fname}')
                atoms.write(ini)
                return True
            except FileNotFoundError:
                pass

    return False


def get_script_and_run(script: Path,
                       time: str = '24h',
                       mincores=40):
    renew_script(script)

    try:
        cc = ComputeCluster()
    except KeyError:
        cc = ComputeCluster('justus2')
    nnodes = int(-(mincores // - cc.data['cores_per_node']))
    ncores = int(nnodes * cc.data['cores_per_node'])

    commands = [f'pushd {script.parent}',
                f'gpaw-runscript {script.name} {ncores} -t {time}',
                f'sbatch run.{cc.arch}', 'popd']
    command = '; '.join(commands)
    print(command)


def run_if_needed(script: Path):
    """Check if script output is there and """
    if not script.exists():
        return
    if len(list(script.parent.glob(script.name + '.o*'))):
        return

    get_script_and_run(script=script)


def renew_script(script: Path):
    """Renew script if older than that within sdb"""
    st_mtime = 0
    if script.exists():
        st_mtime = script.stat().st_mtime

    direc = script.parent
    code = direc.name
    try:
        qdir = float(str(direc.parent.name).replace(
                'q', '', 1))
    except ValueError:
        qdir = 0

    src = script_path(code) / script.name

    if src.stat().st_mtime <= st_mtime:
        return

    shutil.copy(src, direc)

    if qdir:  # change charge
        with open(script) as f:
            text = f.read()
        text = text.replace('q = 0', f'q = {qdir}')
        with open(script, 'w') as f:
            f.write(text)
