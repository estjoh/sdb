import re
from pathlib import Path
from typing import List

from ..definitions import calc_names
from .definitions import basedir


def is_charge_directory(dirname: str) -> bool:
    if re.match('^q[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$', dirname):
        return True
    return False


def is_calc_directory(dirname: str) -> bool:
    for cn in calc_names:
        if dirname.startswith(cn):
            return True
    return False


def isomers(hill: str, datadir: Path = Path(basedir)) -> List[str]:
    """List isomers"""
    def is_isomer(name: str) -> bool:
        if is_calc_directory(name):
            return False
        # exclude charge subdirs
        if re.match('^q[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$', name):
            return False
        return True

    res = []
    moldir = datadir / hill
    if not moldir.exists():
        return []

    for d in moldir.iterdir():
        if d.is_dir() and is_isomer(str(d.name)):
            res.append(d.name)

    return res


def charges(hill: str, datadir: Path = Path(basedir)) -> List[str]:
    """List all charges"""
    res = []
    moldir = datadir / hill
    if not moldir.exists():
        return []

    for d in moldir.iterdir():
        if d.is_dir() and is_charge_directory(str(d.name)):
            res.append(d.name)

    return res
