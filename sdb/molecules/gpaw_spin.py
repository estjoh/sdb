import numpy as np
from ase.calculators.calculator import Calculator
from gpaw import GPAW


class Ovchinnikov(Calculator):
    """Singlet calculator after 10.1103/PhysRevA.53.3946"""
    implemented_properties = ['energy', 'free_energy', 'energies',
                              'forces', 'dipole',
                              'magmom', 'magmoms']

    def __init__(self, mm_sz0, mm_sz2, **gpaw_kwargs):
        super().__init__()
        self.mm_sz0 = mm_sz0
        self.mm_sz2 = mm_sz2
        self.Sz0 = GPAW(**gpaw_kwargs)
        self.Sz2 = GPAW(**gpaw_kwargs)
        self.parameters = self.Sz0.parameters
        self.results = {}

    def calculate(self, atoms, properties, system_changes):
        Calculator.calculate(self, atoms, properties, system_changes)

        atoms0 = self.atoms.copy()
        atoms0.set_initial_magnetic_moments(self.mm_sz0)
        self.Sz0.calculate(atoms0, properties, system_changes)
        b2 = self.Sz0.density.get_spin_contamination(atoms0) / 2
        self.results['magmom'] = 2 * b2
        denom = 1 / (1 - b2)

        atoms2 = self.atoms.copy()
        atoms2.set_initial_magnetic_moments(self.mm_sz2)
        self.Sz2.calculate(atoms2, properties, system_changes)
        self.results['magmom'] = [
            2 * b2, self.Sz2.density.get_spin_contamination(atoms0)]

        self.results['energies'] = [
            self.Sz0.get_property('energy', atoms0),
            self.Sz2.get_property('energy', atoms2)]
        self.results['magmoms'] = np.array([
            self.Sz0.get_property('magmoms', atoms0),
            self.Sz2.get_property('magmoms', atoms2)])
        for prop in self.Sz0.results:
            if prop not in ['energy', 'free_energy',
                            'forces', 'dipole']:
                continue

            self.results[prop] = \
                denom * self.Sz0.get_property(prop, atoms0)
            self.results[prop] -= \
                b2 * denom * self.Sz2.get_property(prop, atoms2)

    def todict(self):
        return self.Sz0.todict()
