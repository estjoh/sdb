from pathlib import Path
from typing import List, Optional
import os
from glob import glob
import re
import difflib
import json
from ase import Atoms, io
from sampling.translate import to_smiles

import sdb
from .molecules import default_code, Molecule, isomer_directories, basedir


def isomers(hill: str, charge: float = 0,
            Sz: float = 0,
            code: str = default_code,
            datadir: Path = basedir) -> List[Molecule]:
    mols = []
    for moldir in isomer_directories(hill, datadir):
        if charge:
            moldir = str(Path(moldir) / f'q{charge}')

        try:
            mols.append(Molecule(moldir, path=datadir, code=code, Sz=Sz))
        except RuntimeError:
            pass
    return mols


def find(composition: Optional[str] = None,
         file: Optional[str] = None,
         name: Optional[str] = None,
         atoms: Optional[Atoms] = None,
         code: str = default_code,
         Sz: float = 0,
         charge: float = 0,
         datadir: str = basedir,
         aslist: bool = False,
         verbose: int = 1
         ) -> Molecule:
    if verbose:
        print('searching', sdb.datadir)
    """Search the database for a specific molecule"""

    # most specific: atoms
    if atoms is not None:
        hill = atoms.get_chemical_formula('hill')
        smiles = to_smiles(atoms)
        for mol in isomers(hill, charge, Sz, code, datadir):
            if to_smiles(mol.atoms) == smiles:
                if aslist:
                    return [mol]
                return mol
        raise RuntimeError(
            f"No molecule like atoms with composition {hill} "
            f"and smiles {smiles}")

    s = False
    if composition is not None:
        s = Atoms(composition)

    if file is not None:
        s = io.read(file)

    if atoms is not None:
        s = Atoms(atoms)
        composition = atoms.get_chemical_formula("hill")
        smiles = to_smiles(atoms)

    if s:
        dirs = isomer_directories(s.get_chemical_formula("hill"), datadir)
        mols = []
        for d in dirs:
            try:
                mols.append(Molecule(d, code=code, Sz=Sz))
            except RuntimeError:
                pass
        if aslist:
            return mols

        if not len(mols):
            raise RuntimeError("No molecule with composition " + composition)
        if len(mols) == 1:
            return mols[0]
        return mols

    with open(Path(sdb.datadir) / "tables" / "names.json") as f:
        names = json.load(f)

    if name in names:
        location = names[name]
        # XXX put the charge into Molecule?
        if charge:
            location = str(Path(location) / f'q{charge}')

        result = Molecule(location, code=code, Sz=Sz)
        if aslist:
            return [result]
        return result

    conformers = []
    for full_name in names:
        if ' conformer_' in full_name:
            mother = full_name.split(' conformer_')[0]
            if name == mother:
                conformers.append(Molecule(names[full_name], code=code, Sz=Sz))
    if len(conformers):
        return conformers

    if aslist:
        return []

    similar_words = difflib.get_close_matches(name, names.keys(), n=5)
    if len(similar_words):
        print('did you mean', similar_words, '?')
    raise RuntimeError("No molecule with name " + name)


def find_element(element):
    result = []
    # all directories containing the element

    for d0 in glob("molecules/*"):
        level1 = os.path.split(d0)[-1]
        elements = [a for a in re.split(r"([A-Z][a-z]*)", level1) if a]
        if element in elements:
            try:  # level1 molecule
                result.append(Molecule(d0))
            except ValueError:
                # level2 molecules
                for d1 in [
                    d for d in os.listdir(d0)
                        if os.path.isdir(os.path.join(d0, d))
                ]:
                    result.append(Molecule(os.path.join(d0, d1)))
    return result
