from pathlib import Path
import yaml
import shutil
import re
from typing import Optional
from ase import Atoms
from sampling.translate import to_smiles

import sdb
from . import basedir, default_code
from .molecules import Molecule
from .subdirs import isomers
from ..naming import encode


def write_definition(directory: Path, definition: dict,
                     defname: str = 'definition.yml') -> None:
    with open(directory / defname, 'w') as f:
        f.write(yaml.dump(definition))


def prepare(atoms: Atoms, subdir: Optional[str] = None,
            definition: Optional[dict] = None,
            charge: Optional[dict] = None,
            sz: Optional[dict] = None,
            basedir: Path = basedir, code: str = default_code) -> Path:
    hill = atoms.get_chemical_formula('hill')
    directory = Path(basedir) / hill

    # XXX hack to avoid ignoring other subdirs
    my_isomers = isomers(hill, Path(basedir))
    if not subdir and len(my_isomers):
        # XXX we should chekc if this is not there already
        subdir = encode(to_smiles(atoms))

    if subdir is not None:
        directory = directory / subdir
    if charge is not None and charge != 0:
        directory = directory / f'q{charge}'

    if not (directory / code).exists():
        _add_files(directory, atoms, sz=sz, charge=charge, code=code)
        _message(directory, code)
        if definition is not None:
            write_definition(directory, definition)
        return directory
    else:
        # check new isomer?
        smiles = to_smiles(atoms)
        mol = Molecule(directory.name, directory.parent)
        mol_smiles = to_smiles(mol.atoms)
        if smiles != mol_smiles:
            if (directory / code).exists():
                # move this directory into sub-dir
                newdir = directory / encode(mol_smiles)
                newdir.mkdir()
                shutil.move(directory / code, newdir)
                # move all charged subdirs
                p = re.compile(r'q[-]?\d+$')
                for subdir in directory.glob('q*'):
                    if p.match(subdir.name):
                        shutil.move(subdir, newdir)
                for ymlfile in directory.glob('*.yml'):
                    shutil.move(ymlfile, newdir)

            _add_files(directory / encode(smiles),
                       atoms, sz=sz, charge=charge, code=code)
            return directory / encode(smiles)

        assert 0, f'{directory / code} exists'


def _add_files(
        directory: Path, atoms: Atoms,
        sz: Optional[float] = None, charge: Optional[float] = None,
        code: str = default_code):
    subdir = directory / code
    subdir.mkdir(parents=True)
    atoms.write(subdir / 'ini.xyz')

    scripts = Path(sdb.__file__).parent / 'scripts' / code
    fname = 'relax.py'
    if sz is not None:
        fname = f'relax_Sz{sz}.py'
    script = open(scripts / fname).read()
    if charge is not None:
        script = script.replace('q = 0', f'q = {charge}')
    with open(subdir / fname, 'w') as f:
        f.write(script)


def _message(directory: Path, code: str = default_code):
    subdir = directory / code
    print(f'Relax the structure in {subdir} as the next step.')
