import yaml
import requests
from typing import List, Tuple, Optional
from ase import Atoms
from ase.data.pubchem import pubchem_search

from sampling.translate import to_smiles

from . import Molecule


def update(mol: Molecule, defname: str = 'definition.yml') -> bool:
    """Update Pubchem Data

    Returns True if something was changed
    """
    if mol.definition is None:
        mol.definition = {}

    if 'smiles' in mol.definition:
        smiles = mol.definition['smiles']
    else:
        smiles = to_smiles(mol.atoms)
        mol.definition['smiles'] = smiles

    res = None
    try:
        res = pubchem_search(smiles=smiles)
        print('# <pubchem> found smiles', smiles)
    except ValueError:
        # this smiles is not known in pubchem
        for name in mol.definition['name']:
            try:
                res = pubchem_search(name=name)
                print('# <pubchem> found name', name)
                break
            except ValueError:
                pass

    # no result
    if res is None:
        raise ValueError('can not find pubchem data')

    org_definition = mol.definition.copy()

    # set and/or check pubchemcid
    pccidkey = 'PUBCHEM_COMPOUND_CID'
    cidkey = 'pubchemcid'

    cid = mol.definition.get(cidkey, None)
    if cid is None:
        cid = res.data.get(pccidkey)
        mol.definition[cidkey] = cid
    else:
        assert cid == mol.definition[cidkey]

    atoms, definition = from_cid(cid, definition=mol.definition)
    mol.definition = definition

    changed = definition != org_definition
    if changed:
        with open(mol.base_directory / defname, 'w') as f:
            f.write(yaml.dump(mol.definition))

    return changed


def compound_names_from_cid(cid: int) -> List:
    url = f"https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/{cid}/synonyms/JSON"  # noqa
    response = requests.get(url)

    if response.status_code == 200:
        data = response.json()
        return data['InformationList']['Information'][0]['Synonym']

    return []


def from_cid(cid: int, maxnames: int = 10,
             definition: Optional[dict] = None) -> Tuple[Atoms, dict]:
    if definition is None:
        definition = {}

    # sanity check
    if 'pubchemcid' in definition:
        assert definition['pubchemcid'] == cid
    else:
        definition['pubchemcid'] = cid

    pubchem_data = pubchem_search(cid=cid)
    atoms = pubchem_data.atoms

    # add name until maximum is reached
    names = definition.get('name', [])
    if type(names) is type(''):
        names = [names]
    nadd = maxnames - len(names)
    if nadd > 0:
        definition['name'] = \
            names + compound_names_from_cid(cid)[:nadd]

    return atoms, definition
