import re
import yaml
import requests
from pathlib import Path

import sdb


def check_refs():
    """Check references and include them to central refrences.yml"""
    pass


class Reference:
    """Reference object"""

    def __init__(self, string=None, doi=None, datadir=None):
        """Initialize from string

        doi:
           used DOI and ignore sring"""
        if datadir is None:
            datadir = sdb.datadir
        self.datadir = Path(datadir)
        if doi is not None:
            return self.initialize_by_doi(doi)
        if string is None:
            raise RuntimeError("Either string or doi must be given")

        string = string.strip()

        self.data = {}
        field = string.split(";")
        if len(field) == 2:
            names, other = field
        elif len(field) == 3:
            names, title, other = field
            self.data["title"] = title.strip()
        else:
            raise RuntimeError(
                "number of ;-separated fields wrong in >{0}<".format(string)
            )

        def switch_name(name):
            """Switch given and last name"""
            nm = name.strip()
            w = nm.split()
            last = w[0]
            if last.isupper():
                # does not seem to be a last name
                return nm
            else:
                return " ".join(w[1:] + w[:1])

        self.data["author"] = " and ".join(
            [switch_name(n) for n in names.split(",")])

        data = [n.strip() for n in re.split(r"\(([^[\]]*)\)", other)]
        assert len(data) == 3
        self.data["year"] = data[1]
        self.data["pages"] = data[2]

        journal, vol = data[0].rsplit(" ", 1)
        # some journals do not have a volume
        if re.search(r"\d", vol):
            self.data["journal"] = journal.strip()
            self.data["volume"] = vol
        else:
            self.data["journal"] = data[0]

    def initialize_by_doi(self, doi):
        doi = doi.lower()

        # try to get data from reference file
        doifilename = (
            self.datadir / "tables" / "references_by_doi.yml")
        if doifilename.exists():
            known = yaml.load(open(doifilename), Loader=yaml.FullLoader)
        else:
            known = {}

        try:
            self.data = known[doi]
        except KeyError:
            # try to get from http://dx.doi.org/
            url = "http://dx.doi.org/" + doi
            headers = {"accept": "application/x-bibtex"}
            req = requests.get(url, headers=headers)

            lines = [line.strip() for line in req.text.split("\n")]
            if not len(lines) or '<h2>DOI Not Found</h2>' in lines:
                raise RuntimeError("Can't get ifor for DOI " + doi)
            self.data = {}

            for line in lines:
                try:
                    key, other = [w.strip() for w in line.split("=")]
                except ValueError:
                    continue
                if "{" in other:
                    entry = other[other.index("{") + 1: other.rindex("}")]
                else:
                    entry = other.replace(",", "").strip()
                # why was this necessarry ???
                # self.data[key.encode("utf-8", "replace")] = entry.encode(
                #    "utf-8", "replace")
                self.data[key] = entry

            entry = {doi: self.data}
            with open(doifilename, "a") as outfile:
                outfile.write(yaml.dump(entry, default_flow_style=False))

    def __str__(self):
        return yaml.dump(self.data, default_flow_style=False)

    def get_id(self):
        if "doi" in self.data:
            return self.data["doi"]
        else:
            name = (self.data["author"].split(" and ")[0]
                    .replace(" ", "_")).strip()
            return name + str(self.data["year"])

    def bibtex(self, id=None):
        if id is None:
            id = self.get_id()

        st = "@Article{{{0},\n".format(id)
        for entry in ["author", "title", "journal", "year", "volume", "pages"]:
            if entry in self.data:
                st += "  {0} = {{{1}}},\n".format(entry, self.data[entry])
        st += "}\n"

        return st
