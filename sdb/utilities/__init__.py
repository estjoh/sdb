def lc_dict(dictionary):
    """All keys to lowercase"""
    res = {}
    for key in dictionary.keys():
        lck = key.lower()
        assert lck not in res
        res[lck] = dictionary[key]
    return res
