import os
from pathlib import Path


def find_datadir() -> Path:
    directory = os.environ.get('STRUCTURE_DATABASE_DIR', None)
    if directory is None:
        return Path('.')
    return Path(directory)


datadir = find_datadir()
