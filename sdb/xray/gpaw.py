from typing import Iterator, Tuple, Optional
from pathlib import Path
from ase import Atoms
from ase.units import Hartree
from ase.parallel import parprint

from . import state
from .calculated import write_data, read_data, data_filename
from ..typing import PathLike


def filenames(directory: PathLike,
              xc: str, h: float, box: float,
              spinpol: bool = False, charge: Optional[float] = None,
              sz: Optional[float] = None, ext: Optional[str] = None,
              verbose=False) -> Tuple[Path, Path]:
    """Return filenames of data- and output-files

    creates subdir if necessary
    """
    basedir = Path(directory) / '1ch'
    if not basedir.exists():
        if verbose:
            parprint('  creating', basedir)
            basedir.mkdir()

    spinext = ''
    if spinpol:
        spinext = '_spin'

    qext = ''
    if charge is not None:
        qext = f'_q{charge}'

    boxstr = ''
    if box is not None:
        boxstr = f'_box{box}'

    extstr = ''
    if sz is not None:
        extstr = f'_Sz{sz}'

    if ext is not None:
        extstr += f'_{ext}'

    dataf = data_filename(directory, xc=xc, h=h, box=box,
                          spinpol=spinpol, charge=charge,
                          ext=extstr)
    outf = basedir / f'1ch{qext}_{xc}_h{h}{boxstr}{spinext}{extstr}.out'

    return dataf, outf


def setups(atoms: Atoms) -> Iterator:
    """iterator looping over all atoms and respective core-state names"""
    for ia, atom in enumerate(atoms):
        state_list = state.get(atom.symbol, [])
        for corestate in state_list:
            yield ia, corestate


def has_atom(data: dict, vstate: str,  ia: int) -> bool:
    if vstate not in data:
        return False
    for i, _, _ in data[vstate]:
        if i == ia:
            return True
    return False


def calculate_and_write_atom(
        atoms: Atoms, datfname: str, ia: int, corestate: str,
        charge: float = 0, E0: Optional[float] = None,
        nonselfconsistent_xc: Optional[str] = None,
        sz: Optional[float] = None) -> Optional[float]:
    """Evaluate the core-hole calculation for a given atom and state

    The calculation will be performed only if the data is not already in
    the file 'datfname'
    The reference energy E0 is calculated if E0 is None.

    Returns the reference energy E0 if something was calculated or None else
    """
    if Path(datfname).exists():
        data = read_data(datfname)
    else:
        datfname = Path(datfname)
        datfname.parent.mkdir(exist_ok=True)
        data = {}

    vstate = f'{atoms[ia].symbol}({corestate})'
    if has_atom(data, vstate, ia):
        return None

    if E0 is None:  # we need the reference energy
        E0 = atoms.get_potential_energy()
        E0 += atoms.calc.wfs.setups.Eref * Hartree
        if nonselfconsistent_xc is not None:
            E0 += atoms.calc.get_xc_difference(nonselfconsistent_xc)
    if 'header' not in data:
        data = {'header': [f'# energy {E0}', '# i  E(1ch)  Eref  element']}

    if sz is not None:
        magmoms_a = [0] * len(atoms)
        magmoms_a[ia] = sz
        atoms.set_initial_magnetic_moments(magmoms_a)

    # add core hole setup to the list of original setups
    original_setups = atoms.calc.parameters.setups
    original_setup_id = atoms.calc.setups.id_a[ia]
    _, setup, _ = original_setup_id
    core_hole_setup = f'{corestate}1ch'
    if setup != 'paw':
        core_hole_setup = f'{setup}.{corestate}1ch'
    setups = original_setups.copy()
    setups.update({ia: core_hole_setup})

    original_calc = atoms.calc
    # XXX hack to keep txt
    txt = original_calc.log.oldfd
    atoms.calc = atoms.calc.new(charge=charge, setups=setups, txt=txt)

    E = atoms.get_potential_energy()
    E += atoms.calc.wfs.setups.Eref * Hartree
    if nonselfconsistent_xc is not None:
        E += atoms.calc.get_xc_difference(nonselfconsistent_xc)
    if vstate not in data:
        data[vstate] = []
    data[vstate].append([ia, E - E0, E])
    write_data(datfname, data)
    # make sure the hole is removed again,
    atoms.calc = original_calc

    return E0


def calculate_and_write(
        atoms: Atoms, datfname: str, charge: float = 0, *,
        nonselfconsistent_xc: Optional[str] = None,
        sz: Optional[float] = None) -> int:
    """Calculate all core-holes and write the corresponding file"""
    E0 = None
    n_calculated = 0
    for ia, corestate in setups(atoms):
        result = calculate_and_write_atom(
            atoms, datfname, ia, corestate,
            charge, E0, nonselfconsistent_xc, sz)
        if result is not None:
            E0 = result
            n_calculated += 1

    return n_calculated
