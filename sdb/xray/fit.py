import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

from ..naming import best_name


def collect_and_show(entries, ms=10, fmt='o'):
    Ecalc = np.array([e.Ecalc.mmp()[0] for e in entries])
    Eexp = np.array([e.Eexp.mmp()[0] for e in entries])
    dEexpm = np.array([e.Eexp.mmp()[1] for e in entries])
    dEexpp = np.array([e.Eexp.mmp()[2] for e in entries])
    names = [e.name for e in entries]
    plt.errorbar(Eexp, Ecalc, fmt=fmt, xerr=[dEexpm, dEexpp], ms=ms)
    return Ecalc, Eexp, dEexpm, dEexpp, names


def show(entries, entries_xas=[], dx=0.1, ms=10, title=None, ylabel=True):
    plt.title(title)
    ec, ee, dep, dem, nn = collect_and_show(entries, ms)
    Ecalc = ec
    Eexp = ee
    dEexpm = dep
    dEexpp = dem
    names = nn
    print(len(Eexp), len(Ecalc), len(names))

    ec, ee, dep, dem, nn = collect_and_show(entries_xas, ms, 's')
    Ecalc = np.append(Ecalc, ec)
    Eexp = np.append(Eexp, ee)
    dEexpm = np.append(dEexpm, dep)
    dEexpp = np.append(dEexpp, dem)
    names += nn
    print(len(Eexp), len(Ecalc), len(names))

    # fit
    def func(x, de):
        return x - de

    popt, pcov = curve_fit(func, Eexp, Ecalc)
    print(popt, pcov)

    x = np.linspace(Eexp.min(), Eexp.max(), 10)
    y = func(x, popt[0])
    label = r'$\delta$={0:5.2f} +- {1:4.2f} eV'.format(
        -popt[0], np.sqrt(np.diag(pcov))[0])
    plt.plot(x, y, '-', label=label)

    for i, name in enumerate(names):
        x = Eexp[i] + dx
        y = Ecalc[i]
        # if dE[i] < 0:
        #    x -= 0.4
        #    y += 0.1
        plt.text(x, y, best_name(name))

    plt.xlabel(r'$E[{\rm Cr(2p}_{3/2})]$ exp [eV]')
    if ylabel:
        plt.ylabel('E(1ch) calc [eV]')
    plt.legend()

    return label
