from typing import List
import numpy as np
from gpaw.utilities.folder import Folder


def x_y_xl(x, y, xl=None):
    X = np.array(x)
    assert len(X.shape) == 1
    Y = np.array(y)
    assert X.shape[0] == Y.shape[0]

    if xl is None:
        Xl = np.unique(X)
    else:
        Xl = np.array(xl)
        assert len(Xl.shape) == 1
    return X, Y, Xl


def varing_fold(width1, folding, x, y, linbroad: list,
                dx=None, xmin=None, xmax=None):
    '''
    linbord: list, fhe form of this list is
    [width2, lin_x1, lin_x2], where width2 is the second
    folding width and lin_x1 and lin_x2 determends where
    each width is appied. Before lin_x1 width1 is used
    and after lin_x2 is used. Inbetwen a mix is uesd,
    applied as a gradient.
    '''
    if xmin is None:
        xmin = np.min(x) - 4 * width1
    if xmax is None:
        xmax = np.max(x) + 4 * width1
    if dx is None:
        dx = width1 / 4.

    xl = np.arange(xmin, xmax + 0.5 * dx, dx)

    X, Y, Xl = x_y_xl(x, y, xl)

    width2 = float(linbroad[0])
    lin_x1 = linbroad[1]
    lin_x2 = linbroad[2]

    width1 = width1

    Yl = np.zeros((Xl.shape[0]))

    for i, x in enumerate(X):
        if x < lin_x1:
            width_line = width1
        elif x <= lin_x2 and lin_x2 != lin_x1:
            width_line = (width1 + (x - lin_x1) *
                          (width2 - width1) / (lin_x2 - lin_x1))
        elif x >= lin_x2:
            width_line = width2

        Yl += Folder(width_line, folding).func.get(Xl, x) * Y[i]

    return Xl, Yl


class VaryFolder(Folder):
    def __init__(self, width: List[float], folding: str = 'Gauss'):
        self.folding = folding
        self.width = width.pop(0)
        self.linbroad = width

    def fold_values(self, x, y, xl):
        X, Y, Xl = x_y_xl(x, y, xl)

        width1 = self.width
        width2 = float(self.linbroad[0])
        lin_x1 = self.linbroad[1]
        lin_x2 = self.linbroad[2]

        Yl = np.zeros((Xl.shape[0]))

        for i, x in enumerate(X):
            if x < lin_x1:
                width_line = width1
            elif x <= lin_x2 and lin_x2 != lin_x1:
                width_line = (width1 + (x - lin_x1) *
                              (width2 - width1) / (lin_x2 - lin_x1))
            elif x >= lin_x2:
                width_line = width2

            Yl += Folder(
                width_line, self.folding).func.get(Xl, x) * Y[i]

        return Xl, Yl
