from pathlib import Path
import io
from typing import List, Optional
from ase.parallel import paropen, world

from . import element_state
from ..molecules import Molecule


def values(
        mol: Molecule, element: str, state: Optional[str] = None,
        xc: str = "PBE", nsc: bool = False, h: float = 0.2, box: float = 4,
        spinpol: bool = False, fname: Optional[str] = None) -> List:
    if state is None:
        _, state = element_state(element)

    if fname is None:
        fname = data_filename(mol.directory, xc=xc, nsc=nsc,
                              h=h, box=box, spinpol=spinpol)
    else:
        fname = mol.directory / "1ch" / fname
    all_data = read_data(fname)

    return all_data[f'{element}({state})']


def data_filename(
        directory: str = '.',
        xc: str = "PBE", nsc: bool = False,
        h: float = 0.2, box: float = 4,
        spinpol: bool = False,
        charge: Optional[float] = None,
        ext: Optional[str] = None) -> Path:
    spinext = ''
    if spinpol:
        spinext = '_spin'

    qext = ''
    if charge is not None:
        qext = f'_q{charge}'

    nscext = ''
    if nsc:
        nscext = '_nsc'

    boxext = ''
    if box is not None:
        boxext = f'_box{box}'

    extstr = ''
    if ext is not None:
        extstr = ext

    return (
        Path(directory) / '1ch'
        / f'1ch{qext}_by_atom_{xc}{nscext}_h{h}{boxext}{spinext}{extstr}.dat')


def from_lines(lines: List[str]) -> dict:
    data = {}

    header = []
    for line in lines:
        if line.startswith('#'):
            header.append(line.strip())
            continue

        word = line.replace('#', '').split()
        if len(word) < 4:
            continue

        # index first
        values = [int(word.pop(0))]
        last = False
        while not last:
            try:
                number = float(word[0])
                values.append(number)
                word.pop(0)
            except ValueError:
                last = True
        # non-numeric -> state
        elem, state = element_state(word[0])
        elemst = f'{elem}({state})'

        if elemst not in data:
            data[elemst] = []
        data[elemst].append(tuple(values))
    data['header'] = header

    return data


def to_lines(data: dict) -> List[str]:
    # make sure header is first
    lines = data['header'].copy()

    for key, entry in data.items():
        if key == 'header':
            continue
        for ia, E1ch, Eref in entry:
            with io.StringIO() as line:
                print(ia, E1ch, Eref, key, file=line, end='')
                lines.append(line.getvalue())

    return lines


def read_data(filename: str) -> dict:
    with open(filename) as f:
        return from_lines(f.readlines())


def write_data(
        filename: str, data: dict, mode: str = 'w',
        comm=None) -> None:
    if comm is None:
        comm = world
    with paropen(filename, mode, comm=comm) as f:
        for line in to_lines(data):
            print(line, file=f)
        comm.barrier()
