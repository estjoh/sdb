from typing import Tuple
import numpy as np
from scipy.interpolate import CubicSpline
from numpy.typing import ArrayLike
from lmfit import minimize, Parameters
from gpaw.utilities.folder import Folder


def update_limits(config_defult, new_config):
    con = config_defult
    for key, lim in new_config.items():
        for m, value in lim.items():
            con[key][m] = value
    return con


def folder_varing(energies: ArrayLike,  energy_calc,
                  intensity_calc, params: Parameters,
                  folding):
    if energy_calc <= params['l1']:
        width_line = params['width']

    elif (energy_calc < params['l2'] and
          params['l2'] != params['l1']):
        width_line = (params['width'] +
                      (energy_calc - params['l1']) *
                      abs(params['width2'] - params['width']) /
                      (params['l2'] - params['l1']))

    elif energy_calc >= params['l2']:
        width_line = params['width2']

    return Folder(width_line,
                  folding).func.get(energies,
                                    energy_calc) * intensity_calc


def folder(energies, energy_calc, intensity_calc, params: Parameters,
           folding: str = 'Lorentz'):
    return Folder(params['width'],
                  folding).func.get(energies,
                                    energy_calc) * intensity_calc


def folded_values(e_exp: ArrayLike, calc, params: Parameters,
                  folding: str = 'Lortnetz') -> ArrayLike:
    """folded intensities evaluated at experimental energies"""

    energies = np.array(e_exp)  # - params['shift']
    if 'shift' in params:
        energies -= params['shift']

    i_calc = np.zeros(energies.shape)

    folder_func = folder
    if 'l1' in params and 'l2' in params:
        folder_func = folder_varing

    for energy, intensity in zip(calc[0], calc[1]):
        i_calc += folder_func(energies, energy, intensity,
                              params, folding)
    return i_calc


def splined(energies: ArrayLike, calc,
            params: Parameters = None):
    if params is not None and 'shift' in params:
        energies = energies - params['shift']
    cs = CubicSpline(*calc)
    return cs(energies)


def residual(params, exp: ArrayLike,
             calc: ArrayLike, folding_params) -> ArrayLike:

    e_exp, i_exp = exp
    if 'scale' in params:
        i_exp = i_exp * params['scale']

    if folding_params == 'folded':
        intensities = splined(e_exp, calc, params)
    else:
        intensities = folded_values(e_exp, calc, params, folding_params)

    resid = (i_exp) - intensities / intensities.max()

    return resid


def ss_reg_ss_tot(params, exp: ArrayLike,
                  calc: ArrayLike, folding_params):

    _, i_exp = exp
    if 'scale' in params:
        i_exp = i_exp * params['scale']

    res = residual(params, exp, calc, folding_params)
    ss_tot = np.sum((i_exp-np.mean(i_exp))**2)

    return res**2 / ss_tot


def r_squared(params, exp: ArrayLike,
              calc: ArrayLike, folding_params):

    return 1 - (ss_reg_ss_tot(params, exp, calc,
                folding_params)).sum()


def adj_r_squared(params, exp: ArrayLike,
                  calc: ArrayLike, folding_params):

    k = len(params)
    n = len(exp[0])
    r2 = r_squared(params, exp, calc, folding_params)

    return 1 - ((1-r2)*(n-1)/(n-k-1))


def nrmse(params, exp: ArrayLike,
          calc: ArrayLike, folding_params):
    resid = residual(params, exp, calc, folding_params)
    _, i_exp = exp
    if 'scale' in params:
        i_exp = i_exp * params['scale']

    return np.sqrt(np.sum(resid**2)/np.sum(i_exp**2))


def write_params(fname, params: Parameters):
    dic = {key: params[key].value for key in params}
    np.savez(fname, **dic)


def select_data(energy: ArrayLike, intensity: ArrayLike,
                e_min: float, e_max: float):

    indexes = np.where((energy <= e_max) & ((energy >= e_min)))[0]

    return np.array([energy[indexes], intensity[indexes]])


def starting_parameters(folding: str = 'Gauss',
                        init_params: dict = None,
                        init_params_lim: dict = None,
                        maximize_r_squerd: bool = False,
                        methode: str = 'R2',
                        folded: bool = False,
                        linbroad_params: dict = None,
                        linbroad_params_lim: dict = None,
                        ):

    folding_params = folding
    if folded:
        folding_params = 'folded'

    init_params_lim_defult = {'scale': {'min': 0, 'max': np.inf},
                              'width': {'min': 0, 'max': np.inf},
                              'shift': {'min': -np.inf, 'max': np.inf}}
    params = Parameters()
    if init_params is None:
        init_params = {}
    if init_params_lim is None:
        init_params_lim = init_params_lim_defult

    else:
        init_params_lim = update_limits(init_params_lim_defult,
                                        init_params_lim)

    for key, value in init_params.items():
        params.add(key, value=value,
                   min=init_params_lim[key]['min'],
                   max=init_params_lim[key]['max'])

    if linbroad_params is not None:
        assert 'width2' and 'l1' and 'l2' in linbroad_params.keys()

        l_mid = (linbroad_params['l1'] + linbroad_params['l2']) / 2
        linbroad_params_lim_defult = {'width2': {'min': 0, 'max': np.inf},
                                      'l1': {'min': 0, 'max': l_mid},
                                      'l2': {'min': l_mid, 'max': np.inf}}

        if linbroad_params_lim is None:
            linbroad_params_lim = linbroad_params_lim_defult

        else:
            linbroad_params_lim = update_limits(linbroad_params_lim_defult,
                                                linbroad_params_lim)

        for key, value in linbroad_params.items():
            params.add(key, value=value,
                       min=linbroad_params_lim[key]['min'],
                       max=linbroad_params_lim[key]['max'])

    residual_function = residual
    if maximize_r_squerd:
        residual_function = ss_reg_ss_tot

    assert methode in ['R2', 'Adj. R2', 'NRMSE']

    if methode == 'R2':
        r2_function = r_squared
    elif methode == 'Adj. R2':
        r2_function = adj_r_squared
    elif methode == 'NRMSE':
        r2_function = nrmse

    return params, folding_params, residual_function, r2_function


def compare_spectra(
            e_exp: ArrayLike, i_exp: ArrayLike,
            e_calc: ArrayLike, f_calc: ArrayLike,
            e_min=None, e_max=None,
            **kwarg,
        ) -> Tuple[float, Parameters]:

    (params, folding_params, residual_function,
     r2_function) = starting_parameters(**kwarg)

    if e_min is None:
        e_min = min(e_exp)
    if e_max is None:
        e_max = max(e_exp)

    exp = select_data(e_exp, i_exp, e_min, e_max)
    calc = np.array([e_calc, f_calc])

    out = minimize(residual_function, params, args=(exp, calc,
                                                    folding_params))

    R2 = r2_function(out.params, exp, calc, folding_params)

    return R2, out.params
