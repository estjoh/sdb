import os
import numpy as np
from typing import Optional

from glob import glob
from ase.io.gpaw_out import read_gpaw_out

from .data import Data

data = Data()

# states to consider
# first entry is used as "default state"
state = {
    "B": ["1s"],
    "C": ["1s"],
    "N": ["1s"],
    "O": ["1s"],
    "F": ["1s"],
    "Mg": ["1s", "2s", "2p"],
    "Al": ["2p"],
    "Si": ["2p"],
    "P": ["2p", "2s", "1s"],
    "S": ["2p", "2s", "1s"],
    "Cl": ["2p", "1s"],
    "Ti": ["2p"],
    "Cr": ["2p"],
    "Fe": ["2p"],
    "Ni": ["2p"],
    "Cu": ["3p", "3s", "2p"],
    "Ge": ["3p"],
    "Se": ["3d"],
    "Mo": ["3d"],
    "Br": ["3d"],
    "Sn": ["3d"],
    "Te": ["4d"],
    "I": ["3d"],
    "Pt": ["2p", "4f"],
    "Au": ["4f"],
}

# State and spin projection given in
# Jolly, William L.; Bomben, K. D. and Eyermann, C. J.
# At. Data Nucl. Data Tables 31 (1984) 433-493
state_Jolly1984 = {
    "B": {"1s": ""},
    "C": {"1s": ""},
    "N": {"1s": ""},
    "O": {"1s": ""},
    "F": {"1s": ""},
    "Al": {"2p": "3/2"},
    "Si": {"2p": ""},
    "P": {"1s": "", "2s": "", "2p": "3/2"},
    "S": {"1s": "", "2s": "", "2p": "3/2"},
    "Cl": {"2p": "3/2"},
    "Ti": {"2p": "3/2"},
    "Cr": {"2p": "3/2"},
    "Fe": {"2p": "3/2"},
    "Ni": {"2p": "3/2"},
    "Ge": {"3p": "3/2"},
    "Se": {"3d": ""},
    "Mo": {"3d": "5/2"},
    "Br": {"3d": "5/2"},
    "Sn": {"3d": "5/2"},
    "Te": {"4d": ""},
    "I": {"3d": "5/2"},
}

# experimental spin-orbit splitting
# now in data/so_splitting.csv

so_weight = {
    "1s": {
        "": 1,
    },
    # Energy of 2p1/2 > energy  of 2p3/2
    "2p": {
        "": 0,
        "1/2": 2.0 / 6.0,
        "3/2": -4.0 / 6.0,
    },
    "3d": {
        "": 0,
        "3/2": 4.0 / 10.0,  # weight 4/10 j = 2 -1/2
        "5/2": - 6.0 / 10.0,  # weight 6/10 j = 2 +1/2
    },
    "4f": {
        "": 0,
        "5/2": 6.0 / 14.0,  # weight 6/14 j = 3 -1/2
        "7/2": - 8.0 / 14.0,  # weight 8/14 j = 3 +1/2
    },
}

xray_maps = {
    'xps': 'CoreElectronBindingEnergy',
    'xas': 'XASFirstPeak', 'xes': 'XESHighestPeak'}


def element_state(vstate):
    """Split element and state given as 'C(1s)' or as 'C'"""
    w = vstate.replace("(", " ")
    w = w.replace(")", "")
    ws = w.split()
    try:
        elem, st = ws
    except ValueError:
        elem = ws[0]
        st = state[elem][0]  # default state
    return elem, st


def shift(E, element, state, xc="PBE", h=0.2):
    delta = get_empirical_shift(element, state, xc="PBE", h=0.2)
    return E - delta


def get_empirical_shift(element, state, xc="PBE", h=0.2,
                        spinpol: bool = False, so_calc: float = None,
                        extrapolate=False) -> float:
    """Return empirical shift in [eV].

    so: correct for experimental spin-orbit splitting
    """
    h_value: dict = data.empirical_shift(element, state, xc, spinpol)

    def interp(h, h_value, extrapolate) -> float:
        # np.interp needs increasing values
        hl = sorted(h_value.keys())
        if not extrapolate:
            assert h >= min(hl)
            assert h <= max(hl)
        vl = [h_value[x] for x in hl]
        return np.interp(h, hl, vl)

    # we have at least one value, so interpolate
    if len(h_value):
        return interp(h, h_value, extrapolate)

    # we do not have it explicitly, but maybe for another spin projection

    def get_so(element, st, spin,
               so_splitting: Optional[float] = None):
        # get spin_orbit shift relative to mean
        # assert 'p' in state  # works only for p states? why??
        if so_splitting is None:
            so_splitting = data.so_splitting[element][st]

        sow = so_weight[st][spin]
        return (np.sign(sow) * (1 - np.abs(sow))
                * so_splitting)

    st = state[:2]
    e_so = -get_so(element, st, state[2:], so_calc)
    for spin in so_weight[st]:
        stspin = st + spin
        h_value: dict = data.empirical_shift(element, stspin, xc, spinpol)
        if len(h_value):
            e_so += get_so(element, st, spin, so_calc)
            return e_so + interp(h, h_value, extrapolate)

    raise RuntimeError(f'No data for element {element} and state {state}')


class XPSMolecule:
    def __init__(self, directory):
        tlst = glob(os.path.join(directory, "relax.py.out*"))
        if not len(tlst):
            raise ValueError
        elif len(tlst) > 1:
            raise RuntimeError("more than one structure in " + directory)

        self.directory = directory
        #        self.atoms = io.read(tlst[0])
        self.atoms = read_gpaw_out(tlst[0])
        self.fmax = np.sqrt(self.atoms.get_forces() ** 2).max()

    def name(self):
        return os.path.split(self.directory)[-1]

    def datadir(self):
        """Return data directory with full path"""
        dirlst = glob(os.path.join(self.directory, "*1ch"))
        assert len(dirlst) < 2
        try:
            return dirlst[0]
        except IndexError:
            return os.path.join(self.directory, "1ch")

    def datafile(self, xc="PBE", h=0.2, box=4):
        """New style data file name"""
        return os.path.join(
            self.datadir, "1ch_by_atom_{0}_h{1}_box{2}.dat".format(
                xc, h, box)
        )

    def xps(self, element, state=None, xc="PBE", h=0.2, box=4):
        datadir = self.datadir
        if not os.path.exists(datadir):
            raise ValueError

        data = []

        if xc == "PBE":
            try:
                # try to use old data
                fname = os.path.join(datadir, "CN_1s1ch_by_atom.dat")
                for line in open(fname).readlines():
                    try:
                        word = line.split()
                        i = int(word[0])
                        Ech = float(word[1])
                        elem = word[3]
                        assert element == elem
                        try:
                            dh = float(word[4])
                            assert h == dh
                            dbox = float(word[5])
                            assert box == dbox
                        except IndexError:
                            # very old data
                            assert h == 0.2 and box == 4
                        data.append((i, Ech))
                    except (ValueError, AssertionError):
                        pass
                if len(data):
                    return data
            except IOError:
                pass

        try:
            fname = os.path.join(
                datadir,
                "CNO1s1ch_by_atom_{0}_h{1}_box{2}.dat".format(xc, h, box)
            )
            for line in open(fname).readlines():
                try:
                    word = line.split()
                    i = int(word[0])
                    Ech = float(word[1])
                    elem = word[3].split("(")[0]
                    assert element == elem
                    data.append((i, Ech))
                except (ValueError, AssertionError):
                    pass
            if len(data):
                return data
        except IOError:
            pass

        fname = self.datafile(xc, h, box)
        for line in open(fname).readlines():
            try:
                word = line.split()
                i = int(word[0])
                Ech = float(word[1])
                elem = word[3]
                assert element == elem
                data.append((i, Ech))
            except (ValueError, AssertionError):
                pass
        if len(data):
            return data

        raise ValueError
