from pathlib import Path
import pandas as pd


class Data():
    @property
    def so_splitting(self):
        if not hasattr(self, '_so_splitting'):
            self._so_splitting = {}
            data = pd.read_csv(
                Path(__file__).parent / 'data'
                / 'so_splitting.csv', sep='\t')
            for element, state, shift in zip(
                    data['element'], data['state'], data['splitting [eV]']):
                if element not in self._so_splitting:
                    self._so_splitting[element] = {}
                self._so_splitting[element][state.split()[0]] = (
                    float(shift)
                )

        return self._so_splitting

    def empirical_shift(self, element: str, state: str, xc: str,
                        spinpol: bool = False) -> dict:
        if not hasattr(self, '_shift_df'):
            self._shift_df = pd.read_csv(
                Path(__file__).parent / 'data' / 'shifts.csv', sep='\t')
        spinkey = 'unpol'
        if spinpol:
            spinkey = 'spinpol'

        df = self._shift_df[
            (self._shift_df['element'] == element)
            & (self._shift_df['xc'] == xc)
            & (self._shift_df['spin'] == spinkey)
            & (self._shift_df['ref state'] == state)]
        if not len(df):
            return {}
        assert len(df) == 1  # no double entries

        res = {}
        for col in df.columns:
            try:
                val = float(df[col].iloc[0])
                if not pd.isna(val):
                    res[float(col)] = val
            except ValueError:
                pass
        return res
