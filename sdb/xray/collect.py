import numpy as np
from scipy.optimize import curve_fit
from typing import List

from sdb.data import EnergyRange, ExpCalc
from sdb.molecules import walk, Molecule
from sdb.experiment import Molecule as Experiment
from sdb.xray import element_state
from sdb.xray import state_Jolly1984
from sdb.xray.calculated import values


def state_exp(element, state):
    sd = state_Jolly1984[element]
    return '{0}{1}'.format(state, sd[state])


def collect(vstate: str,
            xc: str = 'PBE', nsc: bool = False,
            h: float = 0.2, box=4, spinpol=False,
            verbose=2) -> List[ExpCalc]:
    """
    element: string, e.g. 'Si'
    state: None or string, e.g. '2p'
      None means default state for the element from xps_state
    xc: string, default 'PBE'
    h: grid spacing
    box: grid spacing around each atom
    spinpol: spin polarized or not
    """
    element, state = element_state(vstate)

    entries = []

    for name in walk(element):
        try:
            exp = Experiment(name)
            if verbose > 1:
                print(f'{name}: experimental data available')
        except IOError:
            if verbose > 9:
                print(f'{name}: No experimental data')
            continue

        try:
            types = False
            eexp, refs = exp.xps_by_element(
                element, state_exp(element, state), references=True)
            if not len(eexp):
                if verbose > 1:
                    print('No experimental data for', name,
                          'and state {0}({1})'.format(
                              element, state_exp(element, state)))
                continue
        except TypeError:
            types = True

        try:
            calc = Molecule(name)
        except RuntimeError as e:
            print('############### error', name)
            print(e)
            continue

        try:
            ecalc = values(
                calc, element, state, xc=xc, nsc=nsc,
                h=h, box=box, spinpol=spinpol)
        except (IOError, ValueError, KeyError):
            if verbose > 1:
                print('No calculated XPS data for', name)
            continue
        except KeyError:
            if verbose > 1:
                print(f'No calculated XPS data for {name} and state'
                      + f' {element}({state_exp(element, state)})')
            continue

        if not types:
            if verbose:
                print(f'{name}: adding')
            energies_calc = []
            indices = []
            for i, e, _ in ecalc:
                energies_calc.append(e)
                indices.append(i)
            entries.append(ExpCalc(EnergyRange(eexp),
                                   EnergyRange(energies_calc),
                                   name, refs, indices))
        else:
            # we have different chemical environments
            calc_energies = {}
            calc_indices = {}
            for i, a in enumerate(calc.atoms):
                if a.symbol == element:
                    try:
                        eexp, refs = exp.xps_by_index(
                            i, element, state, references=True)
                    except ValueError as e:
                        if verbose:
                            print('###', e)
                        continue

                    for entry in ecalc:
                        if i == entry[0]:
                            calc_energies[eexp[0]] = \
                                calc_energies.get(eexp[0], []) \
                                + [entry[1]]
                            calc_indices[eexp[0]] = \
                                calc_indices.get(eexp[0], []) + [i]

            for key in calc_energies.keys():
                Eexp = EnergyRange([key])
                Ecalc = EnergyRange(calc_energies[key])
                entries.append(ExpCalc(Eexp, Ecalc, name, refs,
                                       calc_indices[key]))

    return entries


def fit(exp_calc_list):
    """Fit XPS/XAS shift"""
    # XXX consider errors
    eexp = [e.Eexp.mmp()[0] for e in exp_calc_list]
    ecalc = [e.Ecalc.mmp()[0] for e in exp_calc_list]

    def func(x, de):
        return x - de
    popt, pcov = curve_fit(func, eexp, ecalc)

    # return in convention of DOI 10.1103/PhysRevB.94.041112
    return -popt[0], np.sqrt(np.diag(pcov))[0]
