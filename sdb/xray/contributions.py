import numpy as np
from typing import Optional, Tuple
from numpy.typing import ArrayLike
from lmfit import minimize, Parameters
from sampling.compare import r2

from .compare import select_data, folded_values


def predicted(params: Parameters, exp: ArrayLike,
              calcs: ArrayLike, folding: str) -> ArrayLike:
    e_exp, i_exp = exp

    pred = np.zeros_like(i_exp)
    all_es, all_fs = calcs
    for i, es in enumerate(all_es):
        pred += params[f'w{i}'] * folded_values(
            e_exp, (es, all_fs[i]), params, folding)

    return pred


def residual(params: Parameters, exp: ArrayLike,
             calcs: ArrayLike, folding: str) -> ArrayLike:
    return exp[1] - predicted(params, exp, calcs, folding)


def contributions(
            e_exp: ArrayLike, i_exp: ArrayLike,
            es_calc: ArrayLike, fs_calc: ArrayLike,
            e_min=None, e_max=None,
            initial_params: Optional[dict] = None,
            folding: str = 'Lorentz',
        ) -> Tuple[float, Parameters]:

    """Fit contributions to an experimental spectrum"""
    assert len(es_calc) == len(fs_calc)

    params = Parameters()
    if initial_params is None:
        initial_params = {}
    for i, _ in enumerate(es_calc):
        key = f'w{i}'
        params.add(key, value=initial_params.get(key, 1), min=0)
    params.add('width', value=initial_params.get('width', 1), min=0)

    if e_min is None:
        e_min = min(e_exp)
    if e_max is None:
        e_max = max(e_exp)

    exp = select_data(e_exp, i_exp, e_min, e_max)
    calcs = (es_calc, fs_calc)

    out = minimize(residual, params,
                   args=(exp, calcs, folding))

    R2 = r2(exp[1], predicted(out.params, exp, calcs, folding))

    return R2, out.params
