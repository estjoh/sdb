from pathlib import Path
import numpy as np
from typing import List, Tuple

from sdb.molecules import walk
from sdb.experiment import Molecule as Experiment
from sdb.molecules import Molecule
from sdb.data import EnergyRange, ExpCalc
from . import element_state, xray_maps


def xray_experiment(
        property, experiment, vstate: str) -> Tuple[np.array, List]:
    """
    Return a list of experimental values if known.

    Parameters
    ----------
    property: str
      in ['CoreElectronBindingEnergy', 'XASFirstPeak', 'XESHighestPeak']

    vstate:
        Example S(1s)

    Returns:
    --------
    values, references
    """
    property = property.lower()
    print('**', property, experiment.data)
    if property not in experiment.data:
        return [], []

    xray = experiment.data[property]
    if not isinstance(xray, list):
        xray = [xray]

    vals = []
    refs = []
    for ref in xray:
        if vstate in ref:
            value = ref[vstate]
            assert not isinstance(value, list)
            vals.append(value)
            try:
                refs.append(ref["reference"])
            except KeyError:
                refs.append(ref["DOI"])

    return np.array(vals), refs


def xray_calculated(
        template: str,
        directory: Path,
        vstate: str, xc="PBE", h=0.2, box=4,
        spinpol=False):
    element, state = element_state(vstate)

    data = []
    spinext = ""
    if spinpol:
        spinext = "_Sz1"

    fname = Path(directory) / "1ch" \
        / f'{template}_{xc}_h{h}_box{box}{spinext}.dat'

    if fname.is_file():
        with open(fname) as f:
            for line in f.readlines():
                if line.startswith('#'):
                    continue
                # take care of commented out vstates
                line = line.replace('#', '')
                try:
                    word = line.split()
                    i = int(word[0])
                    Ech = float(word[1])
                    elem, st = element_state(word[3])
                    assert element == elem
                    assert state == st
                    data.append((i, Ech))
                except (ValueError, AssertionError):
                    pass

    return data


calculation_template = {
    'xas': '1ch_q-1_by_atom',
    'xes': '1ch_q1_by_atom'
}


def collect(spectroscopy: str,
            vstate: str, xc: str = 'PBE', h: float = 0.2,
            box=4, spinpol=False,
            verbose=2) -> List[ExpCalc]:
    """
    spectroscopy: 'xas' or 'xes'
    state: None or string, e.g. '2p'
      None means default state for the element from xps_state
    xc: string, default 'PBE'
    h: grid spacing
    box: grid spacing around each atom
    spinpol: spin polarized or not
    """
    element, _ = element_state(vstate)

    entries = []

    for name in walk(element):
        try:
            exp = Experiment(name)
        except IOError:
            if verbose > 1:
                print(f'{name}: No experimental data')
            continue

        prop = xray_maps[spectroscopy]
        eexp, refs = xray_experiment(prop, exp, vstate)
        if not len(eexp):
            if verbose > 1:
                print(f'{name}: No experimental data for {vstate}')
            continue
        experiment = EnergyRange(eexp)

        mol = Molecule(name)
        data = xray_calculated(
            calculation_template[spectroscopy],
            mol.directory, vstate,
            xc=xc, h=h, spinpol=spinpol)
        if not len(data):
            if verbose > 1:
                print(f'{name}: No calculated data for {vstate}')
            continue

        calculation = EnergyRange([d[1] for d in data])

        if verbose:
            print(f'{name}: adding')
        entries.append(ExpCalc(experiment, calculation, name, refs, []))

    return entries
