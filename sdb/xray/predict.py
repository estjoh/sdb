import numpy as np
from typing import Optional, List, Tuple

from . import element_state, get_empirical_shift, so_weight

from .data import Data

data = Data()


def intensities(
        vstate: str, energies_in: List,
        intensities_in: Optional[List] = None,
        so_calc: float = None,
        xc: str = 'PBE', h: float = 0.2, spinpol: bool = False
        ) -> Tuple[List, List]:
    """Return corrected and SO added energies and intensities"""
    element, state = element_state(vstate)

    # reconstruct all so contributions
    weights = []
    shifts = []
    for so, w in so_weight[state].items():
        if w:
            weights.append(abs(w))
            shifts.append(
                - get_empirical_shift(
                    element, state + so, xc=xc, h=h,
                    so_calc=so_calc, spinpol=spinpol))

    if intensities_in is None:
        intensities_i = np.ones_like(energies_in)
    else:
        intensities_i = intensities_in

    energies = []
    intensities = []

    for energy, intens in zip(energies_in, intensities_i):
        for shift, weight in zip(shifts, weights):
            energies.append(energy + shift)
            intensities.append(intens * weight)

    return np.array(energies), np.array(intensities)
