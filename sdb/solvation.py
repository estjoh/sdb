import numpy as np
import pandas as pd
from pathlib import Path
from typing import Optional, Tuple

from ase.data.vdw_alvarez import vdw_radii as alvarez_vdw_radii
from ase.data.vdw import vdw_radii as original_vdw_radii
import gpaw.solvation as solv


def get_data(solvent: str) -> dict:
    data = pd.read_csv(
        Path(__file__).parent / 'data' / 'solvation.csv', sep='\t')
    data = data.loc[data['solvent'] == solvent]

    if not len(data):
        return {}
    assert len(data) == 1

    res = {}
    for key, val in data.to_dict().items():
        for v in val.values():
            res[key] = v
    return res


def permittivity_U(solvent: str) -> Tuple[Optional[float], Optional[float]]:
    data = pd.read_csv(
        Path(__file__).parent / 'data' / 'solvation.csv', sep='\t')
    data = data.loc[data['solvent'] == solvent]

    if not len(data):
        return None, None

    def float_or_none(ser: pd.core.series.Series) -> Optional[float]:
        if ser.isnull().sum():
            return None
        return float(ser.iloc[0])

    return float_or_none(data['eps']), float_or_none(data['U [meV]'])


def kwargs(solvent: str, alvarez: bool = True) -> dict:
    """Get easy access to solvent parameters"""
    # get undefined radii
    if alvarez:
        vdw_radii = alvarez_vdw_radii.copy()
    else:
        vdw_radii = original_vdw_radii.copy()
        for i, val in enumerate(vdw_radii):
            if np.isnan(val):
                vdw_radii[i] = alvarez_vdw_radii[i]
    vdw_radii[1] = 1.09

    def atomic_radii(atoms):
        return [vdw_radii[n] for n in atoms.numbers]

    params = solv.get_HW14_water_kwargs()
    params['cavity'].effective_potential.atomic_radii = atomic_radii

    permittivity, U = permittivity_U(solvent)
    if permittivity is not None:
        params['dielectric'] = solv.LinearDielectric(
            epsinf=permittivity)

    return params
